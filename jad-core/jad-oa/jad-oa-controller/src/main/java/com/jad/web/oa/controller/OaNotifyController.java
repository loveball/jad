/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.oa.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.Page;
import com.jad.core.oa.qo.NotifyQo;
import com.jad.core.oa.service.NotifyService;
import com.jad.core.oa.vo.NotifyVo;
import com.jad.web.mvc.BaseController;

/**
 * 通知通告Controller
 * @author ThinkGem
 * @version 2014-05-16
 */
@Controller
@RequestMapping(value = "${adminPath}/oa/oaNotify")
public class OaNotifyController extends BaseController {
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private NotifyService notifyService;
	
	@ModelAttribute
	public NotifyVo get(@RequestParam(required=false) String id) {
		NotifyVo entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = notifyService.findById(id);
		}
		if (entity == null){
			entity = new NotifyVo();
		}
		return entity;
	}
	
	@RequiresPermissions("oa:oaNotify:view")
	@RequestMapping(value = {"list", ""})
	public String list(NotifyQo oaNotifyQo,boolean isSelf, HttpServletRequest request, HttpServletResponse response, Model model) {
//		Page<NotifyVo> page = notifyService.find(getPage(request, response), oaNotifyQo,isSelf);
		
//		TODO
		String userId = "1";
		Page<NotifyVo> page = notifyService.findUserNodify(getPage(request, response), oaNotifyQo, userId);
		
		model.addAttribute("page", page);
		return "modules/oa/oaNotifyList";
	}

	@RequiresPermissions("oa:oaNotify:view")
	@RequestMapping(value = "form")
	public String form(NotifyQo oaNotifyQo, Model model) {
		NotifyVo oaNotify=new NotifyVo();
		if (StringUtils.isNotBlank(oaNotifyQo.getId())){
			oaNotify.setNotifyRecordList(notifyService.getRecordList(oaNotifyQo.getId()));
			
		}
		model.addAttribute("oaNotify", oaNotify);
		return "modules/oa/oaNotifyForm";
	}

	@RequiresPermissions("oa:oaNotify:edit")
	@RequestMapping(value = "save")
	public String save(NotifyVo oaNotifyVo, Model model, RedirectAttributes redirectAttributes) {
		
		
		

		NotifyQo oaNotifyQo=new NotifyQo();
		
		if(oaNotifyVo.getId()!=null && StringUtils.isNotBlank(oaNotifyVo.getId().toString())){
			oaNotifyQo.setId(oaNotifyVo.getId().toString());
		}
		
		if (!beanValidator(model, oaNotifyVo)){
			return form(oaNotifyQo, model);
		}
		// 如果是修改，则状态为已发布，则不能再进行操作
		if (StringUtils.isNotBlank(oaNotifyQo.getId())){
			NotifyVo e = notifyService.findById(oaNotifyQo.getId());
			if ("1".equals(e.getStatus())){
				addMessage(redirectAttributes, "已发布，不能操作！");
				return "redirect:" + adminPath + "/oa/oaNotify/form?id="+oaNotifyQo.getId();
			}
			
			notifyService.update(oaNotifyVo);
		}else{
			notifyService.add(oaNotifyVo);
		}
		
		addMessage(redirectAttributes, "保存通知'" + oaNotifyVo.getTitle() + "'成功");
		return "redirect:" + adminPath + "/oa/oaNotify/?repage";
	}
	
	@RequiresPermissions("oa:oaNotify:edit")
	@RequestMapping(value = "delete")
	public String delete(NotifyVo oaNotifyVo, RedirectAttributes redirectAttributes) {
		notifyService.delete(oaNotifyVo);
		addMessage(redirectAttributes, "删除通知成功");
		return "redirect:" + adminPath + "/oa/oaNotify/?repage";
	}
	
	/**
	 * 我的通知列表
	 */
	@RequestMapping(value = "self")
	public String selfList(NotifyQo oaNotifyQo, HttpServletRequest request, HttpServletResponse response, Model model) {
//		oaNotify.setIsSelf(true);
//		Page<NotifyVo> page = notifyService.find(getPage(request, response), oaNotifyQo,true); 
//		TODO
		String userId = "1";
		Page<NotifyVo> page = notifyService.findUserNodify(getPage(request, response), oaNotifyQo, userId);
		
		model.addAttribute("page", page);
		return "modules/oa/oaNotifyList";
	}

	/**
	 * 我的通知列表-数据
	 */
	@RequiresPermissions("oa:oaNotify:view")
	@RequestMapping(value = "selfData")
	@ResponseBody
	public Page<NotifyVo> listData(NotifyQo oaNotifyQo, HttpServletRequest request, HttpServletResponse response, Model model) {
//		oaNotify.setIsSelf(true);
//		Page<NotifyVo> page = notifyService.find(getPage(request, response), oaNotifyQo,true);
//		TODO
		String userId = "1";
		Page<NotifyVo> page = notifyService.findUserNodify(getPage(request, response), oaNotifyQo, userId);
		return page;
	}
	
	/**
	 * 查看我的通知
	 */
	@RequestMapping(value = "view")
	public String view(NotifyQo oaNotifyQo, Model model) {
		
		
		if (StringUtils.isNotBlank(oaNotifyQo.getId())){
			NotifyVo oaNotifyVo=new NotifyVo();
			oaNotifyVo.setId(oaNotifyQo.getId());
			
			notifyService.updateReadFlag(oaNotifyVo.getId());
			
			oaNotifyVo.setNotifyRecordList(notifyService.getRecordList(oaNotifyQo.getId()));
//			oaNotifyVo = oaNotifyService.getRecordList(oaNotifyQo);
			
			model.addAttribute("oaNotify", oaNotifyVo);
			return "modules/oa/oaNotifyForm";
		}
		return "redirect:" + adminPath + "/oa/oaNotify/self?repage";
	}

	/**
	 * 查看我的通知-数据
	 */
	@RequestMapping(value = "viewData")
	@ResponseBody
	public NotifyVo viewData(NotifyVo oaNotifyVo, Model model) {
		if (StringUtils.isNotBlank(oaNotifyVo.getId())){
			notifyService.updateReadFlag(oaNotifyVo.getId());
			return oaNotifyVo;
		}
		return null;
	}
	
	/**
	 * 查看我的通知-发送记录
	 */
	@RequestMapping(value = "viewRecordData")
	@ResponseBody
	public NotifyVo viewRecordData(NotifyQo oaNotifyQo, Model model) {
		if (StringUtils.isNotBlank(oaNotifyQo.getId())){
			NotifyVo vo = new NotifyVo();
			vo.setNotifyRecordList(notifyService.getRecordList(oaNotifyQo.getId()));
			return vo;
		}
		return null;
	}
	
	/**
	 * 获取我的通知数目
	 */
	@RequestMapping(value = "self/count")
	@ResponseBody
	public String selfCount(NotifyVo oaNotifyVo, Model model) {
//		oaNotify.setIsSelf(true);
		oaNotifyVo.setReadFlag("0");
//		return String.valueOf(oaNotifyService.findCount(oaNotifyVo,true));
		return String.valueOf(0);
		
	}
}