package com.jad.core.oa.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.Length;

import com.jad.commons.entity.BaseEo;
import com.jad.dao.annotation.RelateColumn;

@Entity
@Table(name = "oa_notify_record")
public class NotifyRecordEo extends BaseEo<String>{
	
	private static final long serialVersionUID = 1L;
	
	private NotifyEo notify;		// 通知通告ID
	private String readFlag;		// 阅读标记（0：未读；1：已读）
	private Date readDate;		// 阅读时间
	
	private String userId;//接收人
	
	public NotifyRecordEo() {
		super();
	}

	public NotifyRecordEo(String id){
		super(id);
	}
	

	
	
//	@ManyToOne
//	@RelateColumn(name="user_id",loadMethod=RelateLoadMethod.LEFT_JOIN)
//	public UserEo getUser() {
//		return user;
//	}
//
//	public void setUser(UserEo user) {
//		this.user = user;
//	}
	
	@Column(name="user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name="read_flag")
	@Length(min=0, max=1, message="阅读标记（0：未读；1：已读）长度必须介于 0 和 1 之间")
	public String getReadFlag() {
		return readFlag;
	}

	public void setReadFlag(String readFlag) {
		this.readFlag = readFlag;
	}
	
	@ManyToOne
	@RelateColumn(name="notify_id")
	public NotifyEo getNotify() {
		return notify;
	}

	public void setNotify(NotifyEo notify) {
		this.notify = notify;
	}
	
	@OrderBy("read_flag asc,read_date desc")
	@Column(name="read_date")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getReadDate() {
		return readDate;
	}

	public void setReadDate(Date readDate) {
		this.readDate = readDate;
	}
	

	

	
}
