package com.jad.wx.mp.resp;

import java.io.Serializable;
import java.util.List;

/**
 * 跟据id获得标签
 * @author hechuan
 *
 */
public class GetUserByTagidDataResp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<String>openid;

	public List<String> getOpenid() {
		return openid;
	}

	public void setOpenid(List<String> openid) {
		this.openid = openid;
	}
	

}
