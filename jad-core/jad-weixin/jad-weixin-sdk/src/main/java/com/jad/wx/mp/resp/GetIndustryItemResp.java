package com.jad.wx.mp.resp;

import java.io.Serializable;

public class GetIndustryItemResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
//	"first_class":"运输与仓储","second_class":"快递"},
	
	private String first_class;//主行业
	private String second_class;//副行业	
	
	public String getFirst_class() {
		return first_class;
	}
	public void setFirst_class(String first_class) {
		this.first_class = first_class;
	}
	public String getSecond_class() {
		return second_class;
	}
	public void setSecond_class(String second_class) {
		this.second_class = second_class;
	}

}
