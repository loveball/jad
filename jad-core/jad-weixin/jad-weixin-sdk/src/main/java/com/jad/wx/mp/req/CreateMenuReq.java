package com.jad.wx.mp.req;

import java.io.Serializable;
import java.util.List;

/**
 * 创建自定义菜单
 * @author hechuan
 *
 */
public class CreateMenuReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<MenuButton>button;

	public List<MenuButton> getButton() {
		return button;
	}

	public void setButton(List<MenuButton> button) {
		this.button = button;
	}

}
