package com.jad.wx.mp.req;

import java.io.Serializable;


/**
 * 发送图片消息内容
 * @author hechuan
 *
 */
public class SendImageContentMsgReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String media_id;

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	

}
