package com.jad.wx.mp.resp;

import java.util.List;


/**
 * 获取公众号已创建的标签
 * @author hechuan
 *
 */
public class GetTagsResp extends CommonResp{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<UserTagItem>tags;

	public List<UserTagItem> getTags() {
		return tags;
	}

	public void setTags(List<UserTagItem> tags) {
		this.tags = tags;
	}

}
