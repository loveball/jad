package com.jad.wx.mp.req;

import java.io.Serializable;


/**
 * 发送文本消息内容
 * @author hechuan
 *
 */
public class SendTextContentMsgReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String content;//是 	文本消息内容 

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	

}
