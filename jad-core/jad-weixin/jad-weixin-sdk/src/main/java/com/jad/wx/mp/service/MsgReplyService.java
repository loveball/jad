package com.jad.wx.mp.service;

import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.req.RecSimpleMsgReq;
import com.jad.wx.mp.resp.MusicMsgResp;
import com.jad.wx.mp.resp.NewsMsgResp;
import com.jad.wx.mp.resp.PicMsgResp;
import com.jad.wx.mp.resp.TextMsgResp;
import com.jad.wx.mp.resp.VideoMsgResp;
import com.jad.wx.mp.resp.VoiceMsgResp;

/**
 * 被动回复消息
 * @author hechuan
 *
 */
public interface MsgReplyService {
	
	/**
	 * 回复文本信息
	 * @param req
	 * @return
	 */
	TextMsgResp replayTextMsg(RecSimpleMsgReq req)throws WeixinMpException;
	
	/**
	 * 回复图片信息
	 * @param req
	 * @return
	 */
	PicMsgResp replayPicMsgReq(RecSimpleMsgReq req)throws WeixinMpException;
	
	
	/**
	 * 回复语音信息
	 * @param req
	 * @return
	 */
	VoiceMsgResp replayVoiceMsgReq(RecSimpleMsgReq req)throws WeixinMpException;
	
	/**
	 * 回复视频信息
	 * @param req
	 * @return
	 */
	VideoMsgResp replayVodeoMsgReq(RecSimpleMsgReq req)throws WeixinMpException;
	
	/**
	 * 回复音乐信息
	 * @param req
	 * @return
	 */
	MusicMsgResp replayMusicMsgReq(RecSimpleMsgReq req)throws WeixinMpException;
	
	/**
	 * 回复图文信息
	 * @param req
	 * @return
	 */
	NewsMsgResp replayNewsMsgReq(RecSimpleMsgReq req)throws WeixinMpException;
	
}
