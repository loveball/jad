package com.jad.wx.mp.resp;


/**
 * 自定义菜单配置查询
 * @author hechuan
 *
 */
public class GetMenuConfResp extends CommonResp{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String is_menu_open ;// "is_menu_open": 1, 
	private GetMenuConfInfoResp selfmenu_info ;// selfmenu_info
	public String getIs_menu_open() {
		return is_menu_open;
	}
	public void setIs_menu_open(String is_menu_open) {
		this.is_menu_open = is_menu_open;
	}
	public GetMenuConfInfoResp getSelfmenu_info() {
		return selfmenu_info;
	}
	public void setSelfmenu_info(GetMenuConfInfoResp selfmenu_info) {
		this.selfmenu_info = selfmenu_info;
	}
	

}
