package com.jad.wx.mp.resp;

import java.io.Serializable;
import java.util.List;

import com.jad.wx.mp.req.MenuConfButton;

/**
 * 自定义菜单配置查询
 * @author hechuan
 *
 */
public class GetMenuConfInfoResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<MenuConfButton>button;

	public List<MenuConfButton> getButton() {
		return button;
	}

	public void setButton(List<MenuConfButton> button) {
		this.button = button;
	}
	
	

}
