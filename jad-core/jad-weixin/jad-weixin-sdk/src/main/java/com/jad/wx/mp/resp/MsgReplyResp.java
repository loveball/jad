package com.jad.wx.mp.resp;

import java.io.Serializable;

/**
 * 被动消息回复
 * @author hechuan
 *
 */
public class MsgReplyResp implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected String toUserName 	;//是 	接收方帐号（收到的OpenID）
	protected String fromUserName ;//	是 	开发者微信号
	protected String createTime;// 	是 	消息创建时间戳 （整型）
	protected String msgType 	;//是 	 
	
	public String getToUserName() {
		return toUserName;
	}
	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}
	public String getFromUserName() {
		return fromUserName;
	}
	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

}
