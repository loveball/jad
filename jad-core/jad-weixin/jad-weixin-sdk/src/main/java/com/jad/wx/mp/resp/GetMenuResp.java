package com.jad.wx.mp.resp;


/**
 * 自定义菜单查询
 * @author hechuan
 *
 */
public class GetMenuResp extends CommonResp{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private GetMenuButtonResp menu;

	public GetMenuButtonResp getMenu() {
		return menu;
	}

	public void setMenu(GetMenuButtonResp menu) {
		this.menu = menu;
	}
	
	

}
