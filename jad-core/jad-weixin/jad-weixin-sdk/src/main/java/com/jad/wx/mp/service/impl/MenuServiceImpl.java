package com.jad.wx.mp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.http.HttpResp;
import com.jad.commons.http.HttpUtil;
import com.jad.commons.http.MyHttpException;
import com.jad.commons.json.JsonMapper;
import com.jad.wx.mp.constant.MpConstant;
import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.req.CreateMenuReq;
import com.jad.wx.mp.req.MenuButton;
import com.jad.wx.mp.resp.CommonResp;
import com.jad.wx.mp.resp.GetMenuConfResp;
import com.jad.wx.mp.resp.GetMenuResp;
import com.jad.wx.mp.service.MenuService;

public class MenuServiceImpl extends AbstractWxServiceImpl implements MenuService{

	private static Logger logger=LoggerFactory.getLogger(MenuServiceImpl.class);
	
	
	public static void main(String[] args) throws Exception{
		

		CommonResp resp=null;
		MenuServiceImpl service=new MenuServiceImpl();
		
		String openid="o2zDCt7_YbDAUXBY-fINZ1W4VVkk";
		String access_token=MpConstant.TOKEN;
		
		
		service.delMenu(access_token);
		CreateMenuReq createMenuReq = newTestCreateMenuReq();
		System.out.println("生在测试菜单:"+JsonMapper.toJsonString(createMenuReq));
		service.createMenu(createMenuReq, access_token);
		System.out.println("菜单生成结果:"+JsonMapper.toJsonString(createMenuReq));
		
//		resp=service.delMenu(access_token);
//		System.out.println("测试结果:"+JsonMapper.toJsonString(resp));
		
//		resp=service.getMenuConf(access_token);
//		System.out.println("测试结果:"+JsonMapper.toJsonString(resp));
//		resp=service.getMenu(access_token);
//		System.out.println("测试结果:"+JsonMapper.toJsonString(resp));
		
		
		
		
	}
	
	private static CreateMenuReq newTestCreateMenuReq(){
		CreateMenuReq req=new CreateMenuReq();
		
		List<MenuButton>button=new ArrayList<MenuButton>();
		req.setButton(button);
		
		MenuButton btn1=new MenuButton();
		btn1.setName("投票活动1");
		
//		btn1.setType(MpConstant.VIEW_MENU);
//		btn1.setUrl("http://weixin.bolo28.com/jeesitewx/listvote2.html");
		btn1.setType(MpConstant.VIEW_MENU);
//		btn1.setKey("newhd");//最新活动
		
		String redirect_uri="http://weixin.bolo28.com/jeesitewx/";
//		redirect_uri=Encodes.urlEncode(redirect_uri);
//		btn1.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx40659e5cfc5bea25&redirect_uri="+redirect_uri+"&wx_state=0&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect");
		
		btn1.setUrl(redirect_uri);
//		
		
		MenuButton btn2=new MenuButton();
		btn2.setName("投票活动2");
		btn2.setType(MpConstant.VIEW_MENU);
//		btn2.setUrl("http://161546r6x3.51mypc.cn:11034/jeesitewx/listvote.html");
		
//		String redirect_uri="http://weixin.bolo28.com/jeesitewx/listvote2.html";
		
		redirect_uri="http://weixin.bolo28.com/jeesitewx/";
//		redirect_uri=Encodes.urlEncode(redirect_uri);
//		btn2.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx40659e5cfc5bea25&redirect_uri="+redirect_uri+"&wx_state=0&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect");
		btn2.setUrl(redirect_uri);
		
		MenuButton btn3=new MenuButton();
		btn3.setName("投票活动3");
		btn3.setType(MpConstant.VIEW_MENU);
		
		redirect_uri="http://weixin.bolo28.com/jeesitewx/";
//		redirect_uri=Encodes.urlEncode(redirect_uri);
//		btn3.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx40659e5cfc5bea25&redirect_uri="+redirect_uri+"&wx_state=0&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect");
		btn3.setUrl(redirect_uri);
		
		button.add(btn1);button.add(btn2);button.add(btn3);
		
		return req;
	}
	
	/* 
	 * 创建微信自定义菜单
	 * (non-Javadoc)
	 * @see com.jad.wx.mp.service.MenuService#createMenu(com.jad.wx.mp.req.CreateMenuReq, java.lang.String)
	 */
	@Override
	public CommonResp createMenu(CreateMenuReq req, String access_token) throws WeixinMpException {
		String url=MpConstant.API_URL+"/menu/create?access_token="+access_token; //接口地址
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url, JsonMapper.toJsonString(req)); //与微信服务器通讯
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);//解析结果
			
			checkSuccess(resp);//判断结果是否成功，如果不成功，就抛 WeixinMpException 异常
			
			return resp;
			
		} catch (MyHttpException e){ //拦截网络导常
			
			logger.error("通讯异常,"+e.getMessage(),e);//写日志文件
			
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");//抛出业务异常
		}
	}
	

	@Override
	public GetMenuResp getMenu(String access_token) throws WeixinMpException {
		
		String url=MpConstant.API_URL+"/menu/get?access_token="+access_token;
		
		try {
			HttpResp httpResp=HttpUtil.get(url);
			
			GetMenuResp resp=(GetMenuResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetMenuResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
	}

	@Override
	public CommonResp delMenu(String access_token) throws WeixinMpException {
		
		String url=MpConstant.API_URL+"/menu/delete?access_token="+access_token;
		
		try {
			HttpResp httpResp=HttpUtil.get(url);
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
	}

	@Override
	public GetMenuConfResp getMenuConf(String access_token)throws WeixinMpException {

		String url=MpConstant.API_URL+"/get_current_selfmenu_info?access_token="+access_token;
		
		try {
			HttpResp httpResp=HttpUtil.get(url);
			
			GetMenuConfResp resp=(GetMenuConfResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetMenuConfResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
		
		
	}

}
