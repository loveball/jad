package com.jad.wx.mp.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.http.HttpResp;
import com.jad.commons.http.HttpUtil;
import com.jad.commons.http.MyHttpException;
import com.jad.commons.json.JsonMapper;
import com.jad.wx.mp.constant.MpConstant;
import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.req.RecTemplatesendjobfinishEventReq;
import com.jad.wx.mp.req.TemplateMsgItemReq;
import com.jad.wx.mp.req.TemplateMsgReq;
import com.jad.wx.mp.resp.CommonResp;
import com.jad.wx.mp.resp.GetIndustryInfoResp;
import com.jad.wx.mp.resp.GetTemplateIdResp;
import com.jad.wx.mp.resp.GetTemplateListResp;
import com.jad.wx.mp.resp.TemplateMsgResp;
import com.jad.wx.mp.service.TemplateService;

public class TemplateServiceImpl extends AbstractWxServiceImpl implements TemplateService{

	private static Logger logger=LoggerFactory.getLogger(TemplateServiceImpl.class);
	
	public static void main(String[] args) throws Exception {
		
		TemplateServiceImpl service=new TemplateServiceImpl();
		
		CommonResp resp=null;
		
		String openid="o2zDCt7_YbDAUXBY-fINZ1W4VVkk";
		
		String access_token=MpConstant.TOKEN;
		
//		resp=service.setIndustry("1", "4", access_token);
//		System.out.println("测试结果:"+JsonMapper.toJsonString(resp));
		
//		resp=service.getIndustryInfo(access_token);
//		System.out.println("测试结果:"+JsonMapper.toJsonString(resp));
		
//		service.getTemplate("TM00001", access_token);
//		System.out.println("测试结果:"+JsonMapper.toJsonString(resp));
		
//		resp=service.getTemplateList( access_token);
//		System.out.println("测试结果:"+JsonMapper.toJsonString(resp));
		
//		{{first.DATA}} 投票标题：{{keyword1.DATA}} 创建时间：{{keyword2.DATA}} {{remark.DATA}} 谢谢参与 
		
//		resp=service.delTemplateList("PmaktvjCuX1_HlHC7rFh-CTUcPlij5jIKArnXGlfuvM", access_token);
//		System.out.println("测试结果:"+JsonMapper.toJsonString(resp));
		
		TemplateMsgReq req=getTemplateMsgReq(openid);
//		
		resp=service.sendTemplateMsg(req, access_token);
		System.out.println("测试结果:"+JsonMapper.toJsonString(resp));
		
	
	}
	
	
	
	
	private static TemplateMsgReq getTemplateMsgReq(String openid){
		
		TemplateMsgReq req=new TemplateMsgReq();
		
		req.setUrl("http://weixin.qq.com/download");
		
		req.setTouser(openid);
		req.setTopcolor("#000000");
		req.setTemplate_id("fI7hGy1ryr-j08HRrrqUQSQ6k6XaZ_ZHXWzlc88-K7I");
		
		Map<String, TemplateMsgItemReq> data=new HashMap<String,TemplateMsgItemReq>();
		TemplateMsgItemReq first = new TemplateMsgItemReq();
		first.setColor("#000000");
		first.setValue("商品名字！");
		data.put("first", first);
		
		TemplateMsgItemReq keyword1 = new TemplateMsgItemReq();
		keyword1.setColor("#000000");
		keyword1.setValue("规则类型" );
		data.put("name", keyword1);
//
//		TemplateMsgItemReq keyword2 = new TemplateMsgItemReq();
//		keyword2.setColor("#000000");
//		keyword2.setValue("" + DateUtils.formatDateTime(new Date()));
//		data.put("keyword2", keyword2);

		TemplateMsgItemReq remark = new TemplateMsgItemReq();
		remark.setColor("#000000");
		remark.setValue("remarkContent" );
		
		data.put("remark", remark);// 预计2小时回复处理结果，请耐心等待，感谢您对顺丰的支持！
		
		req.setData(data);
		
		return req ;
	}
	
	@Override
	public CommonResp setIndustry(String industry_id1, String industry_id2,String access_token) throws WeixinMpException {
		
		String url=MpConstant.API_URL+"/template/api_set_industry?access_token="+access_token;
		
		Map<String,String>map=new HashMap<String,String>();
		
		map.put("industry_id1", industry_id1);
		map.put("industry_id2", industry_id2);
		
		try {
			
			HttpResp httpResp=HttpUtil.postMsg(url, JsonMapper.toJsonString(map));
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e){
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
	}
	
	@Override
	public GetIndustryInfoResp getIndustryInfo(String access_token)throws WeixinMpException{
		
//		String url=
//		https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token=ACCESS_TOKEN
		
		String url=MpConstant.API_URL+"/template/get_industry?access_token="+access_token;
		
		try {
			
			HttpResp httpResp=HttpUtil.get(url);
			
			GetIndustryInfoResp resp=(GetIndustryInfoResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetIndustryInfoResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			
			logger.error("通讯异常,"+e.getMessage(),e);
			
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
			
		}
		
	}
	

	@Override
	public GetTemplateIdResp getTemplate(String template_id_short, String access_token)throws WeixinMpException {
		
		// TODO 这个接口存在问题 template_id_short 参数不知从哪里获取，待完善
		
		String url=MpConstant.API_URL+"/template/api_add_template?access_token="+access_token;
		
		try {
			Map<String,String>prams=new HashMap<String,String>();
			prams.put("template_id_short", template_id_short);
			
			HttpResp httpResp=HttpUtil.postMsg(url, JsonMapper.toJsonString(prams));
			
			GetTemplateIdResp resp=(GetTemplateIdResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetTemplateIdResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			
			logger.error("通讯异常,"+e.getMessage(),e);
			
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
			
		}
	}
	
	@Override
	public GetTemplateListResp getTemplateList(String access_token)throws WeixinMpException{

		String url=MpConstant.API_URL+"/template/get_all_private_template?access_token="+access_token;
		try {
			
			HttpResp httpResp=HttpUtil.get(url);
			
			GetTemplateListResp resp=(GetTemplateListResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetTemplateListResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			
			logger.error("通讯异常,"+e.getMessage(),e);
			
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
			
		}
		
	}
	
	@Override
	public CommonResp delTemplateList(String template_id,String access_token)throws WeixinMpException{
		
		String url=MpConstant.API_URL+"/template/del_private_template?access_token="+access_token;
		
		try {
			Map<String,String>prams=new HashMap<String,String>();
			prams.put("template_id", template_id);
			
			HttpResp httpResp=HttpUtil.postMsg(url, JsonMapper.toJsonString(prams));
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			
			logger.error("通讯异常,"+e.getMessage(),e);
			
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
			
		}
		
	}
	
	
	@Override
	public TemplateMsgResp sendTemplateMsg(TemplateMsgReq req,String access_token) throws WeixinMpException {
		// TODO Auto-generated method stub 还没测试通过
		
		String url=MpConstant.API_URL+"/template/send?access_token="+access_token;
		
		try {
			String json=JsonMapper.toJsonString(req);
			
			System.out.println("准备发送模板消息,json:"+json);
			
			HttpResp httpResp=HttpUtil.postMsg(url, json);
			
			TemplateMsgResp resp=(TemplateMsgResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), TemplateMsgResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			
			logger.error("通讯异常,"+e.getMessage(),e);
			
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
			
		}
		
		
	}

	@Override
	public void dealRecTemplatesendjobfinishEventReq(RecTemplatesendjobfinishEventReq req) throws WeixinMpException {
		// TODO Auto-generated method stub
		
	}

}
