package com.jad.wx.mp.resp;

import java.io.Serializable;
import java.util.List;

public class GetMaterialListCountResp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<GetMaterialListCountItemResp> news_item;

	public List<GetMaterialListCountItemResp> getNews_item() {
		return news_item;
	}

	public void setNews_item(List<GetMaterialListCountItemResp> news_item) {
		this.news_item = news_item;
	}
}
