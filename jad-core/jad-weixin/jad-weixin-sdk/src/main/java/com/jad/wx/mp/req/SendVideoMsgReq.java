package com.jad.wx.mp.req;


/**
 * 发送视频消息
 * @author hechuan
 *
 */
public class SendVideoMsgReq extends SendMsgReq{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SendVideoContentMsgReq video;
	
	public SendVideoContentMsgReq getVideo() {
		return video;
	}
	public void setVideo(SendVideoContentMsgReq video) {
		this.video = video;
	}
	

}
