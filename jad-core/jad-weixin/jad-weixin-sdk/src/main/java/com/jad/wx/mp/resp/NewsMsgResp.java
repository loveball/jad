package com.jad.wx.mp.resp;

/**
 * 回复图文消息
 * @author hechuan
 *
 */
public class NewsMsgResp  extends MsgReplyResp{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String articleCount ;//	是 	图文消息个数，限制为10条以内
	private NewsArticlesMsgResp articles ;//	是 	多条图文消息信息，默认第一个item为大图,注意，如果图文数超过10，则将会无响应

	public String getArticleCount() {
		return articleCount;
	}
	public void setArticleCount(String articleCount) {
		this.articleCount = articleCount;
	}
	public NewsArticlesMsgResp getArticles() {
		return articles;
	}
	public void setArticles(NewsArticlesMsgResp articles) {
		this.articles = articles;
	}

}
