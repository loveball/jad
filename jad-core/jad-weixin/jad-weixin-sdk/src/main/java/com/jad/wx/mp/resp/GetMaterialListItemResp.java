package com.jad.wx.mp.resp;

import java.io.Serializable;

/**
 * 获得永久素材列表
 * 
 * @author hechuan
 *
 */
public class GetMaterialListItemResp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String media_id;// "media_id": MEDIA_ID,
	private String name;//  	文件名称 
	private String update_time;// 这篇图文消息素材的最后更新时间 
	private String url;// "url":URL

	private GetMaterialListCountResp content;

	public GetMaterialListCountResp getContent() {
		return content;
	}

	public void setContent(GetMaterialListCountResp content) {
		this.content = content;
	}

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
