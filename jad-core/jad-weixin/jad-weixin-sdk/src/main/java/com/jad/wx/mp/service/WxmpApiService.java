package com.jad.wx.mp.service;

import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.resp.GetAccessTokenResp;

public interface WxmpApiService {
	
	/**
	 * 获取access_token
	 * https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
	 * @param appid
	 * @param secret
	 * @return
	 * @throws WeixinMpException
	 */
	GetAccessTokenResp getAccessToken(String appid,String secret)throws WeixinMpException;

}
