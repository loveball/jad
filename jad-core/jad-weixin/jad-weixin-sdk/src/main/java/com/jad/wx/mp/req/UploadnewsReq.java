package com.jad.wx.mp.req;

import java.io.Serializable;
import java.util.List;

/**
 * 上传图文消息素材【订阅号与服务号认证后均可用】 
 * @author hechuan
 *
 */
public class UploadnewsReq implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public List<UploadnewsItemReq>articles;// 	是 	图文消息，一个图文消息支持1到10条图文 

	public List<UploadnewsItemReq> getArticles() {
		return articles;
	}

	public void setArticles(List<UploadnewsItemReq> articles) {
		this.articles = articles;
	}

}
