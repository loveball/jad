package com.jad.wx.mp.resp;

/**
 * 上传永久素材响应
 * @author hechuan
 *
 */
public class AddAttachmentResp extends CommonResp {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String media_id 	;//媒体文件上传后，获取时的唯一标识
	
	public String getMedia_id() {
		return media_id;
	}
	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}
	

}
