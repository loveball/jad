package com.jad.wx.mp.req;

import java.io.Serializable;

public class GetWxmpUserInfoListItemReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String openid;
	private String lang;
	
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	
	
}
