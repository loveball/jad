package com.jad.wx.mp.resp;

import java.util.List;

public class GetKfSessionListResp extends CommonResp{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<GetKfSessionListItemResp>sessionlist;

	public List<GetKfSessionListItemResp> getSessionlist() {
		return sessionlist;
	}

	public void setSessionlist(List<GetKfSessionListItemResp> sessionlist) {
		this.sessionlist = sessionlist;
	}
	
	

}
