package com.jad.wx.mp.resp;

import java.io.Serializable;
import java.util.List;

import com.jad.wx.mp.req.MenuButton;

public class GetMenuButtonResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	private List<MenuButton>button;


	public List<MenuButton> getButton() {
		return button;
	}


	public void setButton(List<MenuButton> button) {
		this.button = button;
	}

}
