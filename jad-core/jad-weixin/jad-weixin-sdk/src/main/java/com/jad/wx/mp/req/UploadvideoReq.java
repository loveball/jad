package com.jad.wx.mp.req;

import java.io.Serializable;

/**
 * 上传视频消息素材【订阅号与服务号认证后均可用】 
 * @author hechuan
 *
 */
public class UploadvideoReq implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String media_id;// 	是 	用于群发的消息的media_id 
	private String title;// 否 	消息的标题 
	private String description ;//  	否 	消息的描述 
	
	public String getMedia_id() {
		return media_id;
	}
	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
