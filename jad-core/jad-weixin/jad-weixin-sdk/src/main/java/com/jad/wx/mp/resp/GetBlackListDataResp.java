package com.jad.wx.mp.resp;

import java.io.Serializable;
import java.util.List;


/**
 * 获取微信用户列表
 * @author hechuan
 *
 */
public class GetBlackListDataResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<String>openid;

	public List<String> getOpenid() {
		return openid;
	}

	public void setOpenid(List<String> openid) {
		this.openid = openid;
	}
	
	

}
