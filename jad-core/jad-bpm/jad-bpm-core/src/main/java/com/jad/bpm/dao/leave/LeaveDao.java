/**
 * There are <a href="https://github.com/thinkgem/jeesite">JeeSite</a> code generation
 */
package com.jad.bpm.dao.leave;

import com.jad.bpm.entity.LeaveEo;
import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;

/**
 * 请假DAO接口
 * @author liuj
 * @version 2013-8-23
 */
@JadDao
public interface LeaveDao extends JadEntityDao<LeaveEo,String> {
	
	/**
	 * 更新流程实例ID
	 * @param leave
	 * @return
	 */
	public int updateProcessInstanceId(LeaveEo leave);
	
	/**
	 * 更新实际开始结束时间
	 * @param leave
	 * @return
	 */
	public int updateRealityTime(LeaveEo leave);
	
}
