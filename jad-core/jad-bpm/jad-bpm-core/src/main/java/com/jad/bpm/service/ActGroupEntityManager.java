/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.bpm.service;

import java.util.List;
import java.util.Map;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.GroupQuery;
import org.activiti.engine.impl.GroupQueryImpl;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.persistence.entity.GroupEntity;

/**
 * Activiti Group Entity Service
 * @author ThinkGem
 * @version 2013-12-05
 */
public interface ActGroupEntityManager  extends ActGroupService {

//	public SystemService getSystemService() ;
	
	public Group createNewGroup(String groupId) ;

	public void insertGroup(Group group) ;

	public void updateGroup(GroupEntity updatedGroup) ;

	public void deleteGroup(String groupId) ;

	public GroupQuery createNewGroupQuery() ;

	public List<Group> findGroupByQueryCriteria(GroupQueryImpl query, Page page) ;

	public long findGroupCountByQueryCriteria(GroupQueryImpl query) ;

	public List<Group> findGroupsByUser(String userId) ;

	public List<Group> findGroupsByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults) ;

	public long findGroupCountByNativeQuery(Map<String, Object> parameterMap) ;

}


