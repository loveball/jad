package com.jad.bpm.service.impl;

import java.util.UUID;

import org.activiti.engine.impl.cfg.IdGenerator;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;


@Service("proIdGenerator")
@Lazy(false)
public class ProIdGenerator  implements IdGenerator{

	@Override
	public String getNextId() {
		return uuid();
	}

	private static String uuid() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
}
