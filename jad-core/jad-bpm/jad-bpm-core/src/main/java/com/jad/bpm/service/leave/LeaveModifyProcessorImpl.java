package com.jad.bpm.service.leave;

import java.util.Date;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.bpm.dao.leave.LeaveDao;
import com.jad.bpm.dto.leave.Leave;
import com.jad.bpm.entity.LeaveEo;
import com.jad.commons.service.AbstractPreService;
import com.jad.dao.utils.EntityUtils;

/**
 * 调整请假内容处理器
 * @author liuj
 */
@Service
@Transactional
public class LeaveModifyProcessorImpl extends AbstractPreService implements LeaveModifyProcessor,TaskListener {
	
	private static final long serialVersionUID = 1L;

	@Autowired
	private LeaveDao leaveDao;
	@Autowired
	private RuntimeService runtimeService;

	public void notify(DelegateTask delegateTask) {
		String processInstanceId = delegateTask.getProcessInstanceId();
		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
		Leave leave = new Leave(processInstance.getBusinessKey());
		leave.setLeaveType((String) delegateTask.getVariable("leaveType"));
		leave.setStartTime((Date) delegateTask.getVariable("startTime"));
		leave.setEndTime((Date) delegateTask.getVariable("endTime"));
		leave.setReason((String) delegateTask.getVariable("reason"));
//		leave.preUpdate();
//		leaveDao.update(leave);
		super.preUpdate(leave);
		leaveDao.updateById(EntityUtils.copyToEo(leave, LeaveEo.class), leave.getId().toString());
		
	}

}
