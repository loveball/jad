/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.bpm.dao;

import com.jad.bpm.dto.ActVo;
import com.jad.bpm.entity.ActEo;
import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;

/**
 * 审批DAO接口
 * @author thinkgem
 * @version 2014-05-16
 */
@JadDao
public interface ActDao extends JadEntityDao<ActEo,String> {

	public int updateProcInsIdByBusinessId(ActVo act);
	
}
