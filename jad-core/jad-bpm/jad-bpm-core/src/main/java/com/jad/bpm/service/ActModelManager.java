/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.bpm.service;

import java.io.UnsupportedEncodingException;

import com.jad.bpm.service.ActModelService;
import com.jad.commons.vo.Page;

/**
 * 流程模型相关Controller
 * @author ThinkGem
 * @version 2013-11-03
 */
public interface ActModelManager extends ActModelService {

	/**
	 * 流程模型列表
	 */
	public Page<org.activiti.engine.repository.Model> modelList(Page<org.activiti.engine.repository.Model> page, String category) ;

	/**
	 * 创建模型
	 * @throws UnsupportedEncodingException 
	 */
	public org.activiti.engine.repository.Model create(String name, String key, String description, String category) throws UnsupportedEncodingException ;

	
	
	
}
