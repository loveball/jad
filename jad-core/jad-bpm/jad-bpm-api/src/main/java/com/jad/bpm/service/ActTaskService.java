package com.jad.bpm.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import com.jad.bpm.dto.ActProcessDefinition;
import com.jad.bpm.dto.ActProcessInstance;
import com.jad.bpm.dto.ActTask;
import com.jad.bpm.dto.ActVo;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;

public interface ActTaskService {

	
	
	/**
	 * 读取带跟踪的图片
	 * @param executionId	环节ID
	 * @return	封装了各种节点信息
	 */
//	TODO 待重写
	public InputStream tracePhoto(String processDefinitionId, String executionId);
	
	/**
	 * 流程跟踪图信息
	 * @param processInstanceId		流程实例ID
	 * @return	封装了各种节点信息
	 */
//	TODO 待重写
	public List<Map<String, Object>> traceProcess(String processInstanceId) throws Exception ;
	
	/**
	 * 获取待办列表
	 * @param procDefKey 流程定义标识
	 * @return
	 */
	public List<ActVo> todoList(ActVo act,String userId);
	
	
	/**
	 * 获取已办任务
	 * @param page
	 * @param procDefKey 流程定义标识
	 * @return
	 */
	public Page<ActVo> historicList(PageQo page, ActVo act,String userId);
	
	
	/**
	 * 获取流转历史列表
	 * @param procInsId 流程实例
	 * @param startAct 开始活动节点名称
	 * @param endAct 结束活动节点名称
	 */
	public List<ActVo> histoicFlowList(String procInsId, String startAct, String endAct);
	
	/**
	 * 流程定义列表
	 */
	public Page<ActProcessDefinition> queryActiveProcessList(PageQo page, String category);
	
	/**
	 * 获取流程表单（首先获取任务节点表单KEY，如果没有则取流程开始节点表单KEY）
	 * @return
	 */
	public String getFormKey(String procDefId, String taskDefKey);
	
	/**
	 * 获取流程实例对象
	 * @param procInsId
	 * @return
	 */
	public ActProcessInstance getActProcIns(String procInsId);
	
	
	/**
	 * 启动流程
	 * @param procDefKey 流程定义KEY
	 * @param businessTable 业务表表名
	 * @param businessId	业务表编号
	 * @return 流程实例ID
	 */
	public String startProcess(String procDefKey, String businessTable, String businessId, String title
			, Map<String, Object> vars,String userId);
	
	/**
	 * 获取任务
	 * @param taskId
	 * @return
	 */
	public ActTask getActTask(String taskId);
	
	/**
	 * 删除任务
	 * @param taskId 任务ID
	 * @param deleteReason 删除原因
	 */
	public void deleteTask(String taskId, String deleteReason);
	
	/**
	 * 签收任务
	 * @param taskId 任务ID
	 * @param userId 签收用户ID（用户登录名）
	 */
	public void claim(String taskId, String userId);
	
	/**
	 * 提交任务, 并保存意见
	 * @param taskId 任务ID
	 * @param procInsId 流程实例ID，如果为空，则不保存任务提交意见
	 * @param comment 任务提交意见的内容
	 * @param title			流程标题，显示在待办任务标题
	 * @param vars 任务变量
	 */
	public void complete(String taskId, String procInsId, String comment, String title, Map<String, Object> vars);
	
	/**
	 * 完成第一个任务
	 * @param procInsId
	 * @param comment
	 * @param title
	 * @param vars
	 */
	public void completeFirstTask(String procInsId, String comment, String title, Map<String, Object> vars,String userId);
	
	
	/**
	 * 读取详细数据
	 * @param id
	 * @return
	 */
	public Map<String, Object>getVariables(String taskId);
	
}
