package com.jad.bpm.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.springframework.web.multipart.MultipartFile;

import com.jad.bpm.dto.ActModel;
import com.jad.bpm.dto.ActProcessDefinition;
import com.jad.bpm.dto.ActProcessInstance;
import com.jad.bpm.dto.ActVo;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;

public interface ActProcessService {
	
	
	/**
	 * 读取资源，通过部署ID
	 * @param processDefinitionId  流程定义ID
	 * @param processInstanceId 流程实例ID
	 * @param resourceType 资源类型(xml|image)
	 * 
	 */
//	TODO 需要重写
	public InputStream resourceRead(String procDefId, String proInsId, String resType) throws Exception;
	
	/**
	 * 部署流程 - 保存
	 * @param file
	 * @return
	 */
//	TODO 需要重写
	public String deploy(String exportDir, String category, MultipartFile file);
	
	/**
	 * 流程定义列表
	 */
	public Page<ActProcessDefinition> queryProcessList(PageQo page, String category);
	
	/**
	 * 流程实例列表
	 */
	public Page<ActProcessInstance> runningInstanceList(PageQo page, String procInsId, String procDefKey);
	
	/**
	 * 设置流程分类
	 */
	public void updateCategory(String procDefId, String category) ;
	
	/**
	 * 挂起、激活流程实例
	 */
	public String updateState(String state, String procDefId) ;
	
	
	/**
	 * 导出图片文件到硬盘
	 */
	public List<String> exportDiagrams(String exportDir) throws IOException ;

	/**
	 * 删除部署的流程，级联删除流程实例
	 * @param deploymentId 流程部署ID
	 */
	public void deleteDeployment(String deploymentId) ;
	
	/**
	 * 删除部署的流程实例
	 * @param procInsId 流程实例ID
	 * @param deleteReason 删除原因，可为空
	 */
	public void deleteProcIns(String procInsId, String deleteReason) ;
	
	/**
	 * 将部署的流程转换为模型
	 * @param procDefId
	 * @throws UnsupportedEncodingException
	 * @throws XMLStreamException
	 */
	public ActModel convertToModel(String procDefId);
	
	
	/**
	 * 获取流程表单URL
	 * @param formKey
	 * @param act 表单传递参数
	 * @return
	 */
	public String getFormUrl(String formKey, ActVo act);
	

}
