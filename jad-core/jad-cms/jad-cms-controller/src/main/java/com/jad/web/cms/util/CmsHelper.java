/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.cms.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.ui.Model;

import com.jad.commons.context.SpringContextHolder;
import com.jad.commons.json.JsonMapper;
import com.jad.commons.utils.StringUtils;
import com.jad.core.cms.qo.ArticleQo;
import com.jad.core.cms.qo.CategoryQo;
import com.jad.core.cms.qo.LinkQo;
import com.jad.core.cms.qo.SiteQo;
import com.jad.core.cms.service.ArticleDataService;
import com.jad.core.cms.service.ArticleService;
import com.jad.core.cms.service.CategoryService;
import com.jad.core.cms.service.LinkService;
import com.jad.core.cms.service.SiteService;
import com.jad.core.cms.utils.ArticleUtil;
import com.jad.core.cms.utils.CategoryUtil;
import com.jad.core.cms.vo.ArticleVo;
import com.jad.core.cms.vo.CategoryVo;
import com.jad.core.cms.vo.LinkVo;
import com.jad.core.cms.vo.SiteVo;

/**
 * 内容管理工具类
 * 
 * @author ThinkGem
 * @version 2013-5-29
 */
public class CmsHelper {

//	private static SiteService siteService = SpringContextHolder.getBean("siteService");
//	private static CategoryService categoryService = SpringContextHolder.getBean("categoryService");
//	private static ArticleService articleService = SpringContextHolder.getBean("articleService");
//	private static ArticleDataService articleDataService = SpringContextHolder.getBean("articleDataService");
//	private static LinkService linkService = SpringContextHolder.getBean("linkService");
	
	
	private static SiteService siteService ;
	private static CategoryService categoryService ;
	private static ArticleService articleService ;
	private static ArticleDataService articleDataService ;
	private static LinkService linkService ;
	
	public static void init(){
		siteService = SpringContextHolder.getBean("siteService");
		categoryService = SpringContextHolder.getBean("categoryService");
		articleService = SpringContextHolder.getBean("articleService");
		articleDataService = SpringContextHolder.getBean("articleDataService");
		linkService = SpringContextHolder.getBean("linkService");
	}
	
//	private static ServletContext context = SpringContextHolder
//			.getBean(ServletContext.class);

//	private static final String CMS_CACHE = "cmsCache";

	/**
	 * 获得站点列表
	 */
	public static List<SiteVo> getSiteList() {
//		@SuppressWarnings("unchecked")
//		List<SiteVo> siteList = (List<SiteVo>) CacheUtils
//				.get(CMS_CACHE, "siteList");
//		if (siteList == null) {
//			Page<SiteVo> page = new Page<SiteVo>(1, -1);
//			page = siteService.findPage(page, new Site());
//			siteList = page.getList();
//			CacheUtils.put(CMS_CACHE, "siteList", siteList);
//		}
		
//		siteService.findById(id)
//		SiteUtil.getDefaultSiteId();
		List<SiteVo> list = siteService.findList(new SiteQo());
		
		return list;
	}

	/**
	 * 获得站点信息
	 * 
	 * @param siteId
	 *            站点编号
	 */
	public static SiteVo getSite(String siteId) {
//		TODO
		return siteService.findById(siteId);
	}

	/**
	 * 获得主导航列表
	 * 
	 * @param siteId
	 *            站点编号
	 */
	public static List<CategoryVo> getMainNavList(String siteId) {
		
		CategoryQo qo = new CategoryQo();
		qo.setInMenu("1");
		qo.setParentId("1");
		return categoryService.findList(qo);
		
	}
	
	public static List<CategoryVo> getMainNavList2(String siteId,String contextPath,String frontPath,String urlSuffix) {
		
		CategoryQo qo = new CategoryQo();
		qo.setInMenu("1");
		qo.setParentId("1");
		List<CategoryVo> categoryList = categoryService.findList(qo);
		for(CategoryVo vo:categoryList){
			vo.setUrl(CategoryUtil.getUrlDynamic(vo, contextPath, frontPath, urlSuffix));
		}
		return categoryList;
		
	}
	

	/**
	 * 获取栏目
	 * 
	 * @param categoryId
	 *            栏目编号
	 * @return
	 */
	public static CategoryVo getCategory(String categoryId) {
		return categoryService.findById(categoryId);
	}

	/**
	 * 获取栏目列表通过父类ID
	 * 
	 * @param categoryId
	 *            栏目编号
	 * @return
	 */
	public static List<CategoryVo> getCategoryByParentId(String parentId,
			String siteId) {
		
		CategoryQo qo = new CategoryQo();
		qo.setParentId(parentId);
		qo.setSiteId(siteId);
		
		return categoryService.findList(qo);
	}

	/**
	 * 获得栏目列表
	 * 
	 * @param siteId
	 *            站点编号
	 * @param parentId
	 *            分类父编号
	 * @param number
	 *            获取数目
	 * @param param
	 *            预留参数，例： key1:'value1', key2:'value2' ...
	 */
	public static List<CategoryVo> getCategoryList(String siteId,
			String parentId, int number, String param) {
		
		CategoryQo qo = new CategoryQo();
		qo.setParentId(parentId);
		qo.setSiteId(siteId);
		
		
		return categoryService.findList(qo);
		
//		Page<Category> page = new Page<Category>(1, number, -1);
//		Category category = new Category();
//		category.setSite(new Site(siteId));
//		category.setParent(new Category(parentId));
//		if (StringUtils.isNotBlank(param)) {
//			@SuppressWarnings({ "unused", "rawtypes" })
//			Map map = JsonMapper.getInstance().fromJson("{" + param + "}",
//					Map.class);
//		}
//		page = categoryService.find(page, category);
//		return page.getList();
	}

	/**
	 * 获取栏目
	 * 
	 * @param categoryIds
	 *            栏目编号
	 * @return
	 */
	public static List<CategoryVo> getCategoryListByIds(String categoryIds) {
		return categoryService.findByIdList(Arrays.asList(StringUtils.split(categoryIds,",")));
				
	}

	/**
	 * 获取文章
	 * 
	 * @param articleId
	 *            文章编号
	 * @return
	 */
	public static ArticleVo getArticle(String articleId) {

		ArticleVo article = articleService.findById(articleId);
		if(article!=null){
			article.setArticleData(articleDataService.findById(articleId));
		}
		return article;
	}

	/**
	 * 获取文章列表
	 * 
	 * @param siteId
	 *            站点编号
	 * @param categoryId
	 *            分类编号
	 * @param number
	 *            获取数目
	 * @param param
	 *            预留参数，例： key1:'value1', key2:'value2' ... posid
	 *            推荐位（1：首页焦点图；2：栏目页文章推荐；） image 文章图片（1：有图片的文章） orderBy 排序字符串
	 * @return ${fnc:getArticleList(category.site.id, category.id, not empty
	 *         pageSize?pageSize:8, 'posid:2, orderBy: \"hits desc\"')}"
	 */
	public static List<ArticleVo> getArticleList(String siteId,
			String categoryId, int number, String param) {
		
		ArticleQo qo = new ArticleQo();
		
		qo.setCategoryId(categoryId);
		
		return articleService.findList(qo);
	}
	public static List<ArticleVo> getArticleList2(String siteId,
			String categoryId, int number, String param,String contextPath,String frontPath,String urlSuffix) {
		
		ArticleQo qo = new ArticleQo();
		
		qo.setCategoryId(categoryId);
		
		List<ArticleVo> articleList = articleService.findList(qo);
		for(ArticleVo vo:articleList){
			vo.setUrl(ArticleUtil.getUrlDynamic(vo, contextPath, frontPath, urlSuffix));
		}
		
		return articleList;
	}

	/**
	 * 获取链接
	 * 
	 * @param linkId
	 *            文章编号
	 * @return
	 */
	public static LinkVo getLink(String linkId) {
		return linkService.findById(linkId);
	}

	/**
	 * 获取链接列表
	 * 
	 * @param siteId
	 *            站点编号
	 * @param categoryId
	 *            分类编号
	 * @param number
	 *            获取数目
	 * @param param
	 *            预留参数，例： key1:'value1', key2:'value2' ...
	 * @return
	 */
	public static List<LinkVo> getLinkList(String siteId, String categoryId,
			int number, String param) {
		
		LinkQo qo = new LinkQo();
		
		return linkService.findList(qo);
		
	}


	/**
	 * 获得文章动态URL地址
	 * 
	 * @param article
	 * @return url
	 */
//	public static String getUrlDynamic(ArticleVo article) {
//		return ArticleUtil.getUrlDynamic(article, "contextPath", "frontPath", "urlSuffix");
//		
//		
//	}


	/**
	 * 获得栏目动态URL地址
	 * 
	 * @param category
	 * @return url
	 */
	public static String getUrlDynamic(CategoryVo category) {
		return "";
	}

	/**
	 * 从图片地址中去除ContextPath地址
	 * 
	 * @param src
	 * @return src
	 */
	public static String formatImageSrcToDb(String src) {
//		if (StringUtils.isBlank(src))
//			return src;
//		if (src.startsWith(context.getContextPath() + "/userfiles")) {
//			return src.substring(context.getContextPath().length());
//		} else {
//			return src;
//		}
		return src;
	}

	/**
	 * 从图片地址中加入ContextPath地址
	 * 
	 * @param src
	 * @return src
	 */
	public static String formatImageSrcToWeb(String src) {
//		if (StringUtils.isBlank(src))
//			return src;
//		if (src.startsWith(context.getContextPath() + "/userfiles")) {
//			return src;
//		} else {
//			return context.getContextPath() + src;
//		}
		return src;
	}

	public static void addViewConfigAttribute(Model model, String param) {
		if (StringUtils.isNotBlank(param)) {
			@SuppressWarnings("rawtypes")
			Map map = JsonMapper.getInstance().fromJson(param, Map.class);
			if (map != null) {
				for (Object o : map.keySet()) {
					model.addAttribute("viewConfig_" + o.toString(), map.get(o));
				}
			}
		}
	}

	public static void addViewConfigAttribute(Model model, CategoryVo category) {
//		List<CategoryVo> categoryList = Lists.newArrayList();
//		Category c = category;
//		boolean goon = true;
//		do {
//			if (c.getParent() == null || c.getParent().isRoot()) {
//				goon = false;
//			}
//			categoryList.add(c);
//			c = c.getParent();
//		} while (goon);
//		Collections.reverse(categoryList);
//		for (Category ca : categoryList) {
//			addViewConfigAttribute(model, ca.getViewConfig());
//		}
	}

	public static ArticleVo getPreArticle(String articleId) {
		
		return articleService.findById(articleId);
				

//		Article articletmp = articleService.get(articleId);
//
//		if (articletmp == null) {
//			return null;
//		}
//		Article article = articleService.getPreArticle(articletmp);
//		if (article == null) {
//			return null;
//		}
//		article.setArticleData(articleDataService.get(articleId));
//
//		return article;
	}

	public static ArticleVo getNextArticle(String articleId) {

		return articleService.findById(articleId);
//		Article articletmp = articleService.get(articleId);
//
//		if (articletmp == null) {
//			return null;
//		}
//		Article article = articleService.getNextArticle(articletmp);
//
//		if (article == null) {
//			return null;
//		}
//		article.setArticleData(articleDataService.get(articleId));
//
//		return article;
	}
}