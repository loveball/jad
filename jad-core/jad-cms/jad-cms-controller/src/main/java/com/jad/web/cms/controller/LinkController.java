/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.cms.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.annotation.ApiCurdType;
import com.jad.commons.context.Constants;
import com.jad.commons.context.Global;
import com.jad.commons.enums.CurdType;
import com.jad.commons.json.JsonMapper;
import com.jad.commons.security.shiro.SecurityHelper;
import com.jad.commons.utils.CollectionUtils;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.utils.Validate;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.web.api.Result;
import com.jad.core.cms.qo.CommentQo;
import com.jad.core.cms.qo.LinkQo;
import com.jad.core.cms.service.CategoryService;
import com.jad.core.cms.service.LinkService;
import com.jad.core.cms.vo.CategoryVo;
import com.jad.core.cms.vo.LinkVo;
import com.jad.core.cms.vo.LinkVo;
import com.jad.core.sys.service.UserService;
import com.jad.web.mvc.BaseController;

/**
 * 链接Controller
 * @author ThinkGem
 * @version 2013-3-23
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/link")
@Api(description = "链接管理")
public class LinkController extends BaseController {

	@Autowired
	private LinkService linkService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private UserService userService;
	
//	@ModelAttribute
//	public LinkVo get(@RequestParam(required=false) String id) {
//		if (StringUtils.isNotBlank(id)){
//			return linkService.findById(id);
//		}else{
//			return new LinkVo();
//		}
//	}
	
	
	/**
	 * 查看单条记录
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:link:view")
	@RequestMapping(value = {"find"})
	@ResponseBody
	@ApiOperation(value = "查看链接信息",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_ONE)
	public Result<LinkVo> find(@RequestParam(required=true) String id){
		return Result.newSuccResult(linkService.findById(id));
	}
	
	/**
	 * 查询列表
	 * @param userQo
	 * @param pageQo
	 * @return
	 */
	@RequiresPermissions("sys:link:view")
	@ResponseBody
	@RequestMapping(value = {"findPage"})
	@ApiOperation(value = "查询链接列表",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_LIST)
	public Result<Page<LinkVo>> findPage(LinkQo linkQo,PageQo pageQo){
		Page<LinkVo> page = linkService.findPage(pageQo, linkQo);
		return Result.newSuccResult(page);
	}
	
	/**
	 * 修改
	 * @param linkVo
	 * @return
	 */
	@RequiresPermissions("sys:link:edit")
	@RequestMapping(value = "update")
	@ResponseBody
	@ApiOperation(value = "修改链接信息",httpMethod = "POST")
	@ApiCurdType(CurdType.UPDATE)
	public Result<?> update(LinkVo linkVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, linkVo)){
			return result;
		}

		if(StringUtils.isBlank(linkVo.getId())){
			result.setParamFail("修改失败,末知的id");
			return result;
		}
		preUpdate(linkVo,SecurityHelper.getCurrentUserId());
		linkService.update(linkVo);
		return result;
	}
	
	/**
	 * 新增
	 * @param userVo
	 * @return
	 */
	@RequiresPermissions("sys:link:edit")
	@RequestMapping(value = "add")
	@ResponseBody
	@ApiOperation(value = "新增链接信息",httpMethod = "POST")
	@ApiCurdType(CurdType.ADD)
	public Result<?> add(LinkVo linkVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		
		if (!beanValidator(result, linkVo)){
			return result;
		}
		preInsert(linkVo,SecurityHelper.getCurrentUserId());
		linkService.add(linkVo);
		return result;
	}
	
	/**
	 * 删除或退回审核
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:link:edit")
	@RequestMapping(value = "del")
	@ResponseBody
	@ApiOperation(value = "删除链接信息",httpMethod = "POST")
	@ApiCurdType(CurdType.DELETE)
	public Result<?> delete(@RequestParam(required=true)String id,@RequestParam(required=false) Boolean isRe){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		LinkVo delVo = new LinkVo();
		delVo.setId(id);
		String delFlag = (isRe!=null && isRe.booleanValue())? Constants.DEL_FLAG_AUDIT:Constants.DEL_FLAG_DELETE;
		delVo.setDelFlag(delFlag);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		linkService.delete(delVo);
		return result;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@RequiresPermissions("cms:link:view")
	@RequestMapping(value = {"list", ""})
	public String list(LinkQo linkQo, HttpServletRequest request, HttpServletResponse response, Model model) {
//		User user = UserUtils.getUser();
//		if (!user.isAdmin() && !SecurityUtils.getSubject().isPermitted("cms:link:audit")){
//			link.setUser(user);
//		}
//        Page<LinkVo> page = linkService.findPage(getPage(request, response), linkQo, true); 
		
		LinkVo linkVo = new LinkVo();
		
		CategoryVo categoryVo = new CategoryVo();
		if(StringUtils.isNotBlank(linkQo.getCategoryId())){
			categoryVo = categoryService.findById(linkQo.getCategoryId());
		}
		linkVo.setCategory(categoryVo);
		
		Page<LinkVo> page = linkService.findPage(getPage(request, response), linkQo);
		if(Page.isNotEmpty(page)){
	        for(LinkVo vo:page.getList()){
	        	if(StringUtils.isNotBlankStr(vo.getCreateBy())){
	        		vo.setUser(userService.findById(vo.getCreateBy()));
	        	}
	        }
	    }
		model.addAttribute("linkVo", linkVo);
        model.addAttribute("page", page);
		return "modules/cms/linkList";
	}

	@RequiresPermissions("cms:link:view")
	@RequestMapping(value = "form")
	public String form(LinkQo linkQo, Model model) {
		LinkVo linkVo = new LinkVo();
		// 如果当前传参有子节点，则选择取消传参选择
//		if (linkVo.getCategory()!=null && StringUtils.isNotBlank(linkVo.getCategory().getId())){
//			CategoryQo qo=new CategoryQo();
//			qo.setParentId(linkVo.getCategory().getId());
//			qo.setSiteId(SiteHelper.getCurrentSiteId());
//			
//			List<CategoryVo> list = categoryService.findList(qo);
//			if (list.size() > 0){
//				linkVo.setCategory(null);
//			}else{
//				linkVo.setCategory(categoryService.findById(linkVo.getCategory().getId()));
//			}
//		}
		
		CategoryVo categoryVo = new CategoryVo();
		if(StringUtils.isNotBlank(linkQo.getCategoryId())){
			categoryVo = categoryService.findById(linkQo.getCategoryId());
		}
		linkVo.setCategory(categoryVo);
		
		if(StringUtils.isNotBlank(linkQo.getId())){
			linkVo = linkService.findById(linkQo.getId());
		}
		
		
		return toForm(linkVo,model);
		
	}
	
	public String toForm(LinkVo linkVo, Model model) {
		model.addAttribute("linkVo", linkVo);
		return "modules/cms/linkForm";
	}

	@RequiresPermissions("cms:link:edit")
	@RequestMapping(value = "save")
	public String save(LinkVo linkVo, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, linkVo)){
			return toForm(linkVo, model);
		}
		if(StringUtils.isNotBlank(linkVo.getId())){
			preUpdate(linkVo, SecurityHelper.getCurrentUserId());
			linkService.update(linkVo);
		}else{
			preInsert(linkVo, SecurityHelper.getCurrentUserId());
			linkService.add(linkVo);
		}
		
		addMessage(redirectAttributes, "保存链接'" + StringUtils.abbr(linkVo.getTitle(),50) + "'成功");
		String categoryId = linkVo.getCategory()!=null?linkVo.getCategory().getId():null;
		return "redirect:" + adminPath + "/cms/link/?repage&categoryId="+categoryId;
	}
	
	@RequiresPermissions("cms:link:edit")
	@RequestMapping(value = "delete")
	public String delete(@RequestParam(required=true)String id, String categoryId, @RequestParam(required=false) Boolean isRe, RedirectAttributes redirectAttributes) {
//		linkService.delete(linkVo, isRe);
//		linkService.delete(linkVo);
		
		LinkVo delVo = new LinkVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		
		String delFlag = (isRe!=null && isRe.booleanValue())? Constants.DEL_FLAG_AUDIT:Constants.DEL_FLAG_DELETE;
		delVo.setDelFlag(delFlag);
		
		preUpdate(delVo, SecurityHelper.getCurrentUserId());
		linkService.update(delVo);
		
//		addMessage(redirectAttributes, (isRe!=null&&isRe?"发布":"删除")+"链接成功");
		addMessage(redirectAttributes, "操作成功");
		return "redirect:" + adminPath + "/cms/link/?repage&categoryId="+categoryId;
	}

	/**
	 * 链接选择列表
	 */
	@RequiresPermissions("cms:link:view")
	@RequestMapping(value = "selectList")
	public String selectList(LinkQo linkQo, HttpServletRequest request, HttpServletResponse response, Model model) {
        list(linkQo, request, response, model);
		return "modules/cms/linkSelectList";
	}
	
	/**
	 * 通过编号获取链接名称
	 */
	@RequiresPermissions("cms:link:view")
	@ResponseBody
	@RequestMapping(value = "findByIds")
	public String findByIds(String ids) {
		
		Validate.notBlank(ids);
		
		List<LinkVo> voList = linkService.findByIdList(Arrays.asList(StringUtils.split(ids,",")));
//		List<Object[]> list = linkService.findByIds(ids);
		
		List<Object[]> list = new ArrayList<Object[]>();
		
		if(CollectionUtils.isNotEmpty(voList)){
			for(LinkVo vo:voList){
				list.add(new Object[]{vo.getId(),StringUtils.abbr(vo.getTitle(),50)});
			}
		}
		
		return JsonMapper.nonDefaultMapper().toJson(list);
	}
}
