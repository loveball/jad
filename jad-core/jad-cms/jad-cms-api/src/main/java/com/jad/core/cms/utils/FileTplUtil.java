package com.jad.core.cms.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.jad.core.cms.vo.FileTplVo;

public class FileTplUtil {
	
	public static List<String> getNameListByPrefix(String realPath,String rootPath) {
		
		List<FileTplVo> list = getListByPath(realPath, false, rootPath);
		
		List<String> result = new ArrayList<String>(list.size());
		
		for (FileTplVo tpl : list) {
			result.add(tpl.getName());
		}
		return result;
	}

	public static List<FileTplVo> getListByPath(String realPath,boolean directory, String rootPath) {
		File f = new File(realPath);
		if (!f.exists()) {
			return new ArrayList<FileTplVo>(0);
		}
		File[] files = f.listFiles();
		if (files == null) {
			return new ArrayList<FileTplVo>(0);
		}
		List<FileTplVo> list = new ArrayList<FileTplVo>();
		for (File file : files) {
			if (file.isFile() || directory){
				list.add(new FileTplVo(file, rootPath));
			}
		}
		return list;
	}

	public static List<FileTplVo> getListForEdit(String realPath,String rootPath) {
		List<FileTplVo> list = getListByPath(realPath, true, rootPath);
		List<FileTplVo> result = new ArrayList<FileTplVo>();
		result.add(new FileTplVo(new File(realPath), rootPath));
		getAllDirectory(result, list, rootPath);
		return result;
	}

	private static void getAllDirectory(List<FileTplVo> result,List<FileTplVo> list, String rootPath) {
		for (FileTplVo tpl : list) {
			result.add(tpl);
			if (tpl.isDirectory()) {
				getAllDirectory(result,getListByPath(tpl.getName(), true, rootPath), rootPath);
			}
		}
	}

	public static FileTplVo getFileTpl(String realPath) {
		File f = new File(realPath);
		if (f.exists()) {
			return new FileTplVo(f, "");
		} else {
			return null;
		}
	}

}
