/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.service;

import java.util.List;
import java.util.Map;

import com.jad.commons.service.BaseService;
import com.jad.core.cms.vo.CategoryVo;

/**
 * 统计Service
 * @author ThinkGem
 * @version 2013-05-21
 */
public interface StatsService extends BaseService {
	
	public List<CategoryVo> article(Map<String, Object> paramMap) ;

}
