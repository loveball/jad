package com.jad.core.cms.utils;


public class SiteUtil {

	public static final String DEFAULT_SITE_ID = "1";
	/**
	 * 获取默认站点ID
	 */
	public static String getDefaultSiteId(){
		return DEFAULT_SITE_ID;
	}
	
	/**
	 * 判断是否为默认（主站）站点
	 */
	public static boolean isDefault(String id){
		return id != null && id.equals(getDefaultSiteId());
	}
	
	public static String getCurrentSiteId() {
//		TODO 待完成
		return getDefaultSiteId();
	}
}
