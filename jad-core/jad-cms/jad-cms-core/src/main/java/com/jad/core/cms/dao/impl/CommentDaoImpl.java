package com.jad.core.cms.dao.impl;

import com.jad.core.cms.dao.CommentDao;
import com.jad.core.cms.entity.CommentEo;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;

@JadDao("commentDao")
public class CommentDaoImpl extends AbstractJadEntityDao<CommentEo,String>implements CommentDao{


}
