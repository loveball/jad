/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.dao;

import com.jad.core.cms.entity.GuestbookEo;
import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;

/**
 * 留言DAO接口
 * @author ThinkGem
 * @version 2013-8-23
 */
@JadDao
public interface GuestbookDao extends JadEntityDao<GuestbookEo,String> {

}
