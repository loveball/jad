/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.service.impl.AbstractServiceImpl;
import com.jad.core.cms.entity.SiteEo;
import com.jad.core.cms.service.SiteService;
import com.jad.core.cms.vo.SiteVo;

/**
 * 站点Service
 * @author ThinkGem
 * @version 2013-01-15
 */
@Service("siteService")
@Transactional(readOnly = true)
public class SiteServiceImpl extends AbstractServiceImpl<SiteEo, String,SiteVo>implements SiteService {

//	@Autowired
//	private SiteCao siteCao;
	
//	public Page<SiteVo> findPage(Page<SiteVo> page, SiteQo site) {
////		DetachedCriteria dc = siteDao.createDetachedCriteria();
////		if (StringUtils.isNotEmpty(site.getName())){
////			dc.add(Restrictions.like("name", "%"+site.getName()+"%"));
////		}
////		dc.add(Restrictions.eq(Site.FIELD_DEL_FLAG, site.getDelFlag()));
////		//dc.addOrder(Order.asc("id"));
////		return siteDao.find(page, dc);
//		
////		site.getSqlMap().put("site", dataScopeFilter(site.getCurrentUser(), "o", "u"));
////		UserVo user=UserUtils.getUser();
////		site.getSqlMap().put("dsf", DataFilters.dataScopeFilter(user, "o", "u"));
//		
//		return super.findPage(page, site);
//	}

//	@Transactional(readOnly = false)
//	public void save(SiteVo site) {
//		if (site.getCopyright()!=null){
//			site.setCopyright(StringEscapeUtils.unescapeHtml4(site.getCopyright()));
//		}
//		
//		if(StringUtils.isNotBlank(site.getId())){
//			super.update(site);
//		}else{
//			super.add(site);
//		}
////		super.save(site);
////		CmsUtils.removeCache("site_"+site.getId());
////		CmsUtils.removeCache("siteList");
////		siteCao.remove(site);
////		siteCao.removeAllSiteList();
//		
//	}
	
//	@Transactional(readOnly = false)
//	public void delete(SiteVo site, Boolean isRe) {
//		//siteDao.updateDelFlag(id, isRe!=null&&isRe?Site.DEL_FLAG_NORMAL:Site.DEL_FLAG_DELETE);
//		site.setDelFlag(isRe!=null&&isRe?Constants.DEL_FLAG_NORMAL:Constants.DEL_FLAG_DELETE);
//		super.delete(site);
////		CmsUtils.removeCache("site_"+site.getId());
////		CmsUtils.removeCache("siteList");
////		siteCao.remove(site);
////		siteCao.removeAllSiteList();
//	}

//	@Override
//	public String getCurrentSiteId() {
////		String siteId = (String)UserUtils.getCache("siteId");
//		String siteId = null;
//		if(StringUtils.isBlank(siteId)){
//			siteId=defaultSiteId();
////			UserUtils.putCache("siteId", siteId);
//		}
//		return siteId;
//	}
//	/**
//	 * 获取默认站点ID
//	 */
//	public String defaultSiteId(){
//		return "1";
//	}
	
}
