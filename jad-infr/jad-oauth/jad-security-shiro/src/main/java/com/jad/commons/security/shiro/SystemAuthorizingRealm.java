/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.commons.security.shiro;

import java.util.Collection;

import javax.annotation.PostConstruct;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jad.commons.context.Constants;
import com.jad.commons.context.Global;
import com.jad.commons.encrypt.Encodes;
import com.jad.commons.utils.StringUtils;
import com.jad.core.sys.service.LogService;
import com.jad.core.sys.service.MenuService;
import com.jad.core.sys.service.RoleService;
import com.jad.core.sys.service.UserService;
import com.jad.core.sys.vo.MenuVo;
import com.jad.core.sys.vo.RoleVo;
import com.jad.core.sys.vo.UserVo;

/**
 * 系统安全认证实现类
 */
public class SystemAuthorizingRealm extends AuthorizingRealm {

	
	@Autowired
	private SessionDAO sessionDao;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private LogService logService;
	
	@Autowired
	private RoleService roleService;

	@Autowired
	private MenuService menuService;
	
	
	/**
	 * 认证回调函数, 登录时调用
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) {
		UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
		
		
		if (logger.isDebugEnabled()){
			int activeSessionSize = sessionDao.getActiveSessions().size();
			logger.debug("login submit, active session size: {}, username: {}", activeSessionSize, token.getUsername());
		}
		
//		// 校验登录验证码
//		if (getSecurityService().isValidateCodeLogin(token.getUsername(), false, false)){
////			Session session = SessionUtils.getSession();
////			String code = (String)session.getAttribute(Constants.VALIDATE_CODE);
//			String code=SecurityHelper.getSessionValidateCode();
//			if (token.getCaptcha() == null || !token.getCaptcha().toUpperCase().equals(code)){
//				throw new AuthenticationException("msg:验证码错误, 请重试.");
//			}
//		}
		
		UserVo user = userService.findByLoginName(token.getUsername());
		if (user != null) {
			if (Constants.NO.equals(user.getLoginFlag())){
				throw new AuthenticationException("msg:该已帐号禁止登录.");
			}
			
			byte[] salt = Encodes.decodeHex(user.getPassword().substring(0,16));
			return new SimpleAuthenticationInfo(
					new Principal(user.getId().toString(),user.getLoginName(),user.getName(), token.isMobileLogin()), 
					user.getPassword().substring(16), ByteSource.Util.bytes(salt), getName());
		} else {
			return null;
		}
	}
	
	
	

	/**
	 * 
	 * 授权回调
	 * 
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		Principal principal = (Principal) getAvailablePrincipal(principals);
		// 获取当前已登录的用户
		if (!Constants.TRUE.equals(Global.getConfig("user.multiAccountLogin"))){  //判断是否重复登录
			Collection<Session> sessions = sessionDao.getActiveSessions( principal, SecurityHelper.getSessionId());
			if (sessions.size() > 0){
				// 如果是登录进来的，则踢出已在线用户
				if(SecurityHelper.isAuthenticated()){
					for (Session session : sessions){
						sessionDao.delete(session);
					}
				}
				// 记住我进来的，并且当前用户已登录，则退出当前用户提示信息。
				else{
					SecurityHelper.logout();
					throw new AuthenticationException("msg:账号已在其它地方登录，请重新登录。");
				}
			}
		}
		
		UserVo user = (UserVo)principal.getData();
		if(user == null ){
			user = userService.findByLoginName(principal.getLoginName());
			if(user == null){
				logger.warn ("授权失败，无法获取loginName为"+principal.getLoginName()+"的用户");
				return null;
			}
			
			user.setMenuList(menuService.findByUserId(user.getId()));//菜单列表
			user.setRoleList(roleService.findByUserId(user.getId()));//权限列表
			principal.setData(user);
		}
		
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		for (MenuVo menu : user.getMenuList()){
			if (StringUtils.isNotBlank(menu.getPermission())){
				// 添加基于Permission的权限信息
				for (String permission : StringUtils.split(menu.getPermission(),",")){
					info.addStringPermission(permission);
				}
			}
		}
		// 添加用户权限
		info.addStringPermission("user");

		// 添加用户角色信息
		for (RoleVo role : user.getRoleList()){
			info.addRole(role.getEnname());
		}
		
		return info;
	}
	

	/**
	 * 设定密码校验的Hash算法与迭代次数
	 */
	@PostConstruct
	public void initCredentialsMatcher() {
		HashedCredentialsMatcher matcher = new HashedCredentialsMatcher(Constants.HASH_ALGORITHM);
		matcher.setHashIterations(Constants.HASH_INTERATIONS);
		setCredentialsMatcher(matcher);
	}
	
	
}


