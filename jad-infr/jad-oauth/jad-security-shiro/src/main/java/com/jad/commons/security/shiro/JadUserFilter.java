package com.jad.commons.security.shiro;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.web.filter.authc.UserFilter;

import com.jad.commons.json.JsonMapper;
import com.jad.commons.web.api.RespCode;
import com.jad.commons.web.api.Result;
import com.jad.commons.web.utils.RequestUtils;

public class JadUserFilter extends UserFilter{
	
	  protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
	        boolean isAjax = RequestUtils.isAjax((HttpServletRequest)request);
	        if(isAjax){
	        	response.getWriter().print(JsonMapper.toJsonString(Result.newResult(RespCode.NO_LOGIN, null)));
	        	response.getWriter().flush();
	        	return false;
	        }else{
	        	return super.onAccessDenied(request, response);
	        }
	        
	    }
	  
    
}
