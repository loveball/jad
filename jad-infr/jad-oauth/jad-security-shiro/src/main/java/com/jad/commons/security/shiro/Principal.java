package com.jad.commons.security.shiro;

import java.io.Serializable;


/**
 * 身份
 * @author Administrator
 *
 */
public class Principal implements Serializable{

	
private static final long serialVersionUID = 1L;
	
	private String id; // 编号
	private String loginName; // 登录名
	private String name; // 姓名
	private boolean mobileLogin; // 是否手机登录  //这个属性待完善
	
	private Object data;//此身份携带的附加数，比如用户信息，权限等
	
	public Principal(String id,String loginName,String name, boolean mobileLogin) {
		this.id = id;
		this.loginName = loginName;
		this.name = name;
		this.mobileLogin = mobileLogin;
	}

	
	/**
	 * 获取SESSIONID
	 */
	public String getSessionid() {
		String sessionId=SecurityHelper.getSessionId();
		return sessionId;
	}
	
	
	
	public Object getData() {
		return data;
	}


	public void setData(Object data) {
		this.data = data;
	}


	public String getId() {
		return id;
	}

	public String getLoginName() {
		return loginName;
	}

	public String getName() {
		return name;
	}

	public boolean isMobileLogin() {
		return mobileLogin;
	}
	
	public String toString(){
		return id;
	}
	


}
