package com.jad.dao.entity;

import com.jad.commons.vo.EntityObject;

public class EoFieldVal<E extends EntityObject> {
	
	private EoFieldInfo<E> info;
	
	private Object value;

	public EoFieldVal(EoFieldInfo<E> info, Object value) {
		super();
		this.info = info;
		this.value = value;
	}

	public EoFieldInfo<E> getInfo() {
		return info;
	}

	public void setInfo(EoFieldInfo<E> info) {
		this.info = info;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}


}
