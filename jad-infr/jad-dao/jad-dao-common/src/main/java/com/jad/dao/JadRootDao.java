package com.jad.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.data.repository.NoRepositoryBean;

import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.vo.QueryObject;

@NoRepositoryBean
public interface JadRootDao<EO extends EntityObject, ID extends Serializable> extends RootDao{

	
	int executeSql(String sql) ;
	
	<QO extends QueryObject> int updateByQo(EO entity, QO qo,Class<EO>entityClass);
	
	<QO extends QueryObject>int deleteByQo(QO qo,Class<EO>entityClass);
	
	int deleteById(ID id,Class<EO>entityClass);
	
	int deleteByIdList(List<ID>idList,Class<EO>entityClass);
	
	<E extends EntityObject> E findById(ID id,Class<E>entityClass);
	<E extends EntityObject> List<E> findByIdList(List<ID> idList,Class<E>entityClass);
	<E extends EntityObject> List<E> findByIdList(List<ID> idList,String orderBy,Class<E>entityClass);
	
	<E extends EntityObject,QO extends QueryObject> List<E> findByQo(QO qo,Class<E>entityClass);
	
	<E extends EntityObject,QO extends QueryObject> Long findCountByQo(QO qo,Class<E>entityClass);
	Long findCountBySql(String sql,List<?>params);
	
	<E extends EntityObject,QO extends QueryObject> List<E> findByQo(QO qo,String orderBy,Class<E>entityClass);
	
	<E> List<E> findBySql(String sql,List<?>params,Class<E> resultTypeClass);
	
	<E extends EntityObject,QO extends QueryObject> Page<E> findPageByQo(PageQo page,QO qo,Class<E>entityClass);
	
	<E extends EntityObject,QO extends QueryObject> Page<E> findPageByQo(PageQo page,QO qo,String orderBy,Class<E>entityClass);

	<E> Page<E> findPageBySql(PageQo page,String sql,List<?>params,Class<E>resultTypeClass);
	
	<E extends EntityObject,QO extends QueryObject> List<Map<String,?>> findMapByQo(QO qo,Class<E>entityClass);
	
	<E extends EntityObject,QO extends QueryObject> List<Map<String,?>> findMapByQo(QO qo,String orderBy,Class<E>entityClass);
	
	List<Map<String,?>> findMapBySql(String sql,List<?> params );
	
	<E extends EntityObject,QO extends QueryObject> Page<Map<String,?>> findPageMapByQo(PageQo page,QO qo,Class<E>entityClass);
	
	<E extends EntityObject,QO extends QueryObject> Page<Map<String,?>> findPageMapByQo(PageQo page,QO qo,String orderBy,Class<E>entityClass);
	
	Page<Map<String,?>> findPageMapBySql(PageQo page,String sql,List<?>params );
	


}
