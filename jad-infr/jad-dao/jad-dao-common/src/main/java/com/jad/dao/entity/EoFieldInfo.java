package com.jad.dao.entity;

import java.lang.reflect.Field;

import com.jad.commons.vo.EntityObject;
import com.jad.dao.enums.RelateLoadMethod;

/**
 * 实体属性信息
 * @author hechuan
 *
 * @param <E>
 */
public class EoFieldInfo<E extends EntityObject> extends ObjectFieldInfo{
	
	/**
	 * 实体信息
	 */
	private EoMetaInfo<E> eoInfo;
	
	/**
	 * 字段列名
	 */
	private String column;
	
	/**
	 * 是否为实体表的列
	 */
	private boolean isColumn=false;
	
	private boolean isRelateColumn=false;
	
	/**
	 * 是否为实体表的id
	 */
	private boolean isId=false;
	
	/**
	 * 关联列,如果实体属性类型为其它实体类型的对象，这里保存关联属信息
	 */
	private RelateLoadMethod relateLoadMethod=null;//关联对像加载方式
	
	/**
	 * 关联表别名 
	 */
	private String relateAlias;
	
	private String orderBy; 
	
	private boolean unique=false;
	private boolean nullable=true;
	private boolean insertable=true;
	private boolean updatable=true;

	public EoFieldInfo(EoMetaInfo<E> eoInfo,Field field) {
		super(field);
		this.eoInfo=eoInfo;
	}
	
	

	public boolean isRelateColumn() {
		return isRelateColumn;
	}



	public void setRelateColumn(boolean isRelateColumn) {
		this.isRelateColumn = isRelateColumn;
	}



	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public boolean isUnique() {
		return unique;
	}

	public void setUnique(boolean unique) {
		this.unique = unique;
	}

	public boolean isNullable() {
		return nullable;
	}

	public void setNullable(boolean nullable) {
		this.nullable = nullable;
	}

	public boolean isInsertable() {
		return insertable;
	}

	public void setInsertable(boolean insertable) {
		this.insertable = insertable;
	}

	public boolean isUpdatable() {
		return updatable;
	}

	public void setUpdatable(boolean updatable) {
		this.updatable = updatable;
	}


	public EoMetaInfo<E> getEoInfo() {
		return eoInfo;
	}



	public void setEoInfo(EoMetaInfo<E> eoInfo) {
		this.eoInfo = eoInfo;
	}



	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public boolean isColumn() {
		return isColumn;
	}

	public void setColumn(boolean isColumn) {
		this.isColumn = isColumn;
	}

	public boolean isId() {
		return isId;
	}

	public void setId(boolean isId) {
		this.isId = isId;
	}

	public RelateLoadMethod getRelateLoadMethod() {
		return relateLoadMethod;
	}

	public void setRelateLoadMethod(RelateLoadMethod relateLoadMethod) {
		this.relateLoadMethod = relateLoadMethod;
	}



	public String getRelateAlias() {
		return relateAlias;
	}



	public void setRelateAlias(String relateAlias) {
		this.relateAlias = relateAlias;
	}


}
