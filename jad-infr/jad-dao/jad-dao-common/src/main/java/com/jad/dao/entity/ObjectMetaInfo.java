package com.jad.dao.entity;

/**
 * 
 * 对像元信息
 * 
 */
public class ObjectMetaInfo<T> {
	
	protected Class<T> metaClass;
	
	private String objName;
	
	public ObjectMetaInfo(Class<T> metaClass){
		this.metaClass=metaClass;
		this.objName=metaClass.getName();
	}

	public Class<T> getMetaClass() {
		return metaClass;
	}

	public void setMetaClass(Class<T> metaClass) {
		this.metaClass = metaClass;
	}

	public String getObjName() {
		return objName;
	}

	public void setObjName(String objName) {
		this.objName = objName;
	}

	
	
	
	

}


