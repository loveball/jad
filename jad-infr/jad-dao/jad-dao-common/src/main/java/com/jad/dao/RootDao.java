package com.jad.dao;

import java.util.List;

import com.jad.commons.vo.Page;
import com.jad.dao.annotation.Param;

public interface RootDao {
	
	int executeSql(
			@Param("jadSql")String sql,
			@Param("jadListParam")List<?> params) ;
	
	<E>List<E> findBySql(
			@Param("jadPage")Page<?> page,
			@Param("jadSql")String sql,
			@Param("jadListParam")List<?> params,
			@Param("jadResultType")Class<E>entityClass);
	
}






