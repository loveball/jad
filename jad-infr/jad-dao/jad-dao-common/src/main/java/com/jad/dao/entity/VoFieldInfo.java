package com.jad.dao.entity;

import java.lang.reflect.Field;

import com.jad.commons.vo.ValueObject;

public class VoFieldInfo <V extends ValueObject> extends ObjectFieldInfo{

	
	private VoMetaInfo<V> voInfo;
	
	public VoFieldInfo(VoMetaInfo<V> voInfo,Field field) {
		super(field);
		this.voInfo=voInfo;
	}

	public VoMetaInfo<V> getVoInfo() {
		return voInfo;
	}

	public void setVoInfo(VoMetaInfo<V> voInfo) {
		this.voInfo = voInfo;
	}
	
	


}
