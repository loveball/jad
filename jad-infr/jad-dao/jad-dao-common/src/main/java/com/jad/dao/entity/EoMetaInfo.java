package com.jad.dao.entity;

import java.util.HashMap;
import java.util.Map;

import com.jad.commons.vo.EntityObject;

public class EoMetaInfo<E extends EntityObject> extends ObjectMetaInfo<E> {

	public EoMetaInfo(Class<E> clazz) {
		super(clazz);
	}
	
	/**
	 * 表名称
	 */
	private String tableName;
	

	/**
	 * 记录实体属性上所有的 OrderBy 注解
	 */
	private String orderBy; 
	
	
	/**
	 * 实体字段
	 * key:field name,val:EoFieldInfo
	 */
	private Map<String,EoFieldInfo<E>>fieldInfoMap;
//	private Set<EoFieldInfo<E>> fieldInfos;
	
	/**
	 * 主键字段信息
	 */
	private EoFieldInfo<E> keyFieldInfo;
	
	
	/**
	 * 缓存 where条件
	 * key:where key_sepeciType,val:WhereCondition
	 */
	private Map<String,WhereCondition<E>>whereConditionMap=new HashMap<String,WhereCondition<E>>();
	
	


	public String getTableName() {
		return tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getOrderBy() {
		return orderBy;
	}


	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}



	public EoFieldInfo<E> getKeyFieldInfo() {
		return keyFieldInfo;
	}


	public void setKeyFieldInfo(EoFieldInfo<E> keyFieldInfo) {
		this.keyFieldInfo = keyFieldInfo;
	}


	public Map<String, WhereCondition<E>> getWhereConditionMap() {
		return whereConditionMap;
	}


	public void setWhereConditionMap(Map<String, WhereCondition<E>> whereConditionMap) {
		this.whereConditionMap = whereConditionMap;
	}


	public Map<String, EoFieldInfo<E>> getFieldInfoMap() {
		return fieldInfoMap;
	}


	public void setFieldInfoMap(Map<String, EoFieldInfo<E>> fieldInfoMap) {
		this.fieldInfoMap = fieldInfoMap;
	}


	
	
	
	


}
