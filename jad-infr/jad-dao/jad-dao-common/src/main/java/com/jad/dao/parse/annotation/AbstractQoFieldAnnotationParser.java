package com.jad.dao.parse.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import com.jad.commons.reflection.ReflectionUtils;
import com.jad.commons.vo.QueryObject;
import com.jad.dao.entity.QoFieldInfo;
import com.jad.dao.parse.QoFieldParser;

public abstract class AbstractQoFieldAnnotationParser<A extends Annotation> implements QoFieldParser {
	
	private Class<A> annotationClass;
	
	@SuppressWarnings("unchecked")
	public AbstractQoFieldAnnotationParser(){
		this.annotationClass=ReflectionUtils.getSuperClassGenricType(this.getClass(),0);
	}
	
	public abstract <QO extends QueryObject> boolean parseAnnotationQoField(QoFieldInfo<QO>qoFieldInfo, Field field,A annotation);
	
	@Override
	public <QO extends QueryObject> boolean parseQoField(QoFieldInfo<QO> qoFieldInfo, Field field) {
		
		Method getterMethod=ReflectionUtils.findGetterMethod(qoFieldInfo.getQoInfo().getMetaClass(),field.getName());
		
		boolean parseRes=parseQoField(qoFieldInfo,field,getterMethod);//先解析getter方法上的注解
		
		if(!parseRes){//getter方法上没有注解，再从字段上解析
			
			A annotation=field.getAnnotation(annotationClass);
			
			if(annotation!=null){
				
				parseRes = parseAnnotationQoField(qoFieldInfo,field,annotation);
				
			}
		}
		
		return parseRes;
		
	}
	
	private <QO extends QueryObject> boolean parseQoField(QoFieldInfo<QO> qoFieldInfo, Field field, Method getterMethod) {
		
		A annotation=getterMethod.getAnnotation(annotationClass);
		
		if(annotation!=null){
			
			return parseAnnotationQoField(qoFieldInfo,field,annotation);
			
		}else{
			
			return false;
			
		}
	}



}
