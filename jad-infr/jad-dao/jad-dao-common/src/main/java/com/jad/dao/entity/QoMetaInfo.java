package com.jad.dao.entity;

import java.util.Map;

import com.jad.commons.vo.QueryObject;

public class QoMetaInfo <QO extends QueryObject> extends ObjectMetaInfo<QO>{

	public QoMetaInfo(Class<QO> metaClass) {
		super(metaClass);
	}
	
	/**
	 * 字段信息
	 * key:field name,val:VoFieldInfo
	 */
	private Map<String,QoFieldInfo<QO>>fieldInfoMap;

	public Map<String, QoFieldInfo<QO>> getFieldInfoMap() {
		return fieldInfoMap;
	}

	public void setFieldInfoMap(Map<String, QoFieldInfo<QO>> fieldInfoMap) {
		this.fieldInfoMap = fieldInfoMap;
	}
	



	

}
