package com.jad.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.util.Assert;

import com.jad.commons.json.JsonMapper;
import com.jad.commons.reflection.ReflectionUtils;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.vo.QueryObject;
import com.jad.dao.entity.EoFieldVal;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.enums.RespositoryType;
import com.jad.dao.exception.JadEntityParseException;
import com.jad.dao.utils.DaoReflectionUtil;
import com.jad.dao.utils.EntityUtils;
import com.jad.dao.utils.SqlHelper;

@NoRepositoryBean
public class AbstractJadEntityDao<EO extends EntityObject, ID extends Serializable> 
	implements JadEntityDao<EO,ID>,InitializingBean,ApplicationContextAware,BeanNameAware{
	
	Logger logger = LoggerFactory.getLogger(AbstractJadEntityDao.class);
	
	/**
	 * 用于真正执行sql语句的dao
	 * 框架会跟据不同持久化技术自动注入
	 * 目前只支持 hibernate,jpa,mybatis
	 */
//	private JadEntityDao<EO,ID> delegate;
	private RootDao delegate;
	
	/**
	 * spring自动注入的 上下文
	 */
	protected ApplicationContext applicationContext;
	
	/**
	 * spring自动注入的当前bean的名称
	 */
	protected String beanName;
	
	/**
	 * 通用 delegate 对象在applicationContext中的名称
	 */
	public static final String COMMON_DELETAGE_BEANNAME="commonJadDao";
	
	public static final String REPOSITORY_TYPE_BEAN_NAME="repositoryType";
	
//	
	
	/**
	 * 当前 bean对像的实体类
	 * 可通过范型声明自动获取
	 * 也可通过覆盖 getEntityClass()方法指定
	 */
	private Class<EO>entityClass;
	
	/**
	 * delegate的后缀
	 */
	public static final String DELEGATE_BEAN_NAME_SUFFIX="Delegate";
	
	/**
	 * dao实现类默认后缀
	 */
	public static final String DEFAULT_IMPL_SUFFIX="Impl";
	
	
	
	/**
	 * 待久化类型,由框架自动注入
	 * 目前只支持 hibernate,jpa,mybatis
	 */
	protected String repositoryType;
	
	/**
	 * 获得当前dao对应的实体
	 * 默认通过范型声明获得
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Class<EO> getEntityClass(){
		if(entityClass==null){
			entityClass=ReflectionUtils.getSuperClassGenricType(getClass(), 0);
		}
		Assert.notNull(entityClass);
		return entityClass;
	}
	
	/**
	 * 获得当前dao的delegate类
	 * 对于 hibernate 来说，就是 commonJadDao
	 * 但对于 mybatis 和 jpa 来说，是当前 beanName+Delegate
	 * @return
	 */
	protected String getDelegateBeanName(){
		
		String delegateBeanName=COMMON_DELETAGE_BEANNAME;
		if(RespositoryType.MYBATIS.getRespositoryType().equals(repositoryType) 
				|| RespositoryType.JPA.getRespositoryType().equals(repositoryType) ){
			
			delegateBeanName = beanName + DELEGATE_BEAN_NAME_SUFFIX;
			
		}
		
		return delegateBeanName;
	}
	
	public RootDao getDelegate(){
		if(delegate==null){
			String delegateName=getDelegateBeanName();
			if(applicationContext.containsBean(delegateName)){
				delegate=(RootDao)applicationContext.getBean(delegateName);
			}
			if(delegate==null){
				throw new RuntimeException("can not find bean:"+delegateName);
			}
		}
		return delegate;
	}
	
//	@SuppressWarnings("unchecked")
//	public JadEntityDao<EO,ID> getDelegate(){
//		if(delegate==null){
//			String delegateName=getDelegateBeanName();
//			if(applicationContext.containsBean(delegateName)){
//				delegate=(JadEntityDao<EO,ID>)applicationContext.getBean(delegateName);
//			}
//			if(delegate==null){
//				throw new RuntimeException("can not find bean:"+delegateName);
//			}
//		}
//		return delegate;
//	}
	
	@Override
	public EO add(EO entity) {
		return add(entity,getEntityClass());
	}

	@Override
	public int updateById(EO entity, ID id) {
		return updateById(entity, id,getEntityClass());
	}

	
//	@Override
//	public int updateByMap(EO entity, Map<String, ?> conditionParams) {
//		return updateByMap(entity, conditionParams,getEntityClass());
//	}
	
	public <QO extends QueryObject> int updateByQo(EO entity, QO qo,Class<EO>entityClass){
		EoMetaInfo<EO> ei=EntityUtils.getEoInfo(entityClass);
		List<EoFieldVal<EO>>valList=DaoReflectionUtil.getEntityFieldVals(entity,entityClass);
		if(valList.isEmpty()){
			logger.warn("entity 所有可持久化属性都是空的");
			return 0;
		}
		List<Object>params=new ArrayList<Object>();
		String sql=SqlHelper.getUpdateSql(ei,valList,params);
		
		sql=sql+SqlHelper.getWhereSql(ei, null, qo, null, params);
		
//		logger.debug("执行更新语句:"+sql);
//		logger.debug("参数:"+JsonMapper.toJsonString(params));
		return executeSql(sql, params);
	}
	
	@Override
	public <QO extends QueryObject> int updateByQo(EO entity, QO qo){
		return updateByQo(entity,qo,getEntityClass());
	}

	
	
	
	@Override
	public int updateByIdList(EO entity, List<ID> idList) {
		return updateByIdList(entity, idList,getEntityClass());
	}


	@Override
	public int deleteById(ID id) {
		return deleteById(id, getEntityClass());
	}

	@Override
	public int deleteByIdList(List<ID> idList) {
		return deleteByIdList(idList, getEntityClass());
	}

//	@Override
//	public int deleteByMap(Map<String, ?> params) {
//		return deleteByMap(params, getEntityClass());
//	}
	
	@Override
	public <QO extends QueryObject> int deleteByQo(QO qo){
		return deleteByQo(qo,getEntityClass());
	}

	
	@Override
	public <QO extends QueryObject> int deleteByQo(QO qo,Class<EO>entityClass){
		
		EoMetaInfo<EO> ei=EntityUtils.getEoInfo(entityClass);
		
		String sql="delete from "+ei.getTableName();
		
		List<Object>params=new ArrayList<Object>();
		
		sql=sql+SqlHelper.getWhereSql(ei, null, qo,null, params);
		
		return executeSql(sql, params);
		
		
	}
	

	@Override
	public EO findById(ID id) {
		return findById(id, getEntityClass());
	}
	
	@Override
	public <E extends EntityObject> List<E> findByIdList(List<ID> idList,Class<E>entityClass){
		return findByIdList(idList,null,entityClass);
	}
	
	@Override
	public <E extends EntityObject> List<E> findByIdList(List<ID> idList,String orderBy,Class<E>entityClass){
		
		List params = new ArrayList();
		
		StringBuffer sb=new StringBuffer();
		
		String alias = StringUtils.uncapitalize(entityClass.getSimpleName());
		
		
		sb.append(SqlHelper.getSelectSql(entityClass, alias, new ArrayList<String>()));
		sb.append(" where ");
		sb.append(alias).append(".");
		sb.append(EntityUtils.getEoInfo(entityClass).getKeyFieldInfo().getFieldName()+"  in ");
		sb.append(SqlHelper.getValsInSql(idList, params));
		
		sb.append(SqlHelper.getOrderBySql(StringUtils.isNotBlank(orderBy)?orderBy:EntityUtils.getEoInfo(entityClass).getOrderBy(), alias));
		
		return findBySql(sb.toString(), params, entityClass);
		
	}
	
	public List<EO> findByIdList(List<ID> idList){
		return findByIdList(idList,null,getEntityClass());
	}
	
	@Override
	public List<EO> findByIdList(List<ID> idList,String orderBy){
		return findByIdList(idList,orderBy,getEntityClass());
	}

//	@Override
//	public List<EO> findByMap(Map<String, ?> conditionParams) {
//		return findByMap(conditionParams, getEntityClass());
//	}
	
	
	@Override
	public <E extends EntityObject,QO extends QueryObject> List<E> findByQo(QO qo,Class<E>entityClass){
//		return findByMap(EntityUtils.copyQoToMap(qo),entityClass);
		return this.findByQo(qo, null, entityClass);
	}
	
	@Override
	public <QO extends QueryObject> List<EO> findByQo(QO qo){
		
//		return findByMap(EntityUtils.copyQoToMap(qo));
		
		return findByQo(qo,null,getEntityClass());
		
		
	}
	
	@Override
	public <E extends EntityObject,QO extends QueryObject> Long findCountByQo(QO qo,Class<E>entityClass){
		
		EoMetaInfo<E> ei=EntityUtils.getEoInfo(entityClass);
		
		String alias=SqlHelper.getDefAlias(ei);
		
		List<Object> params=new ArrayList<Object>();
		List<String>rcSqlList=new ArrayList<String>();
		
		StringBuffer sb=new StringBuffer();
		sb.append(" select count(*) C from ");
		sb.append(SqlHelper.getTableName(ei)).append(" ").append(alias).append(" ");
		
		sb.append(SqlHelper.getWhereSql(ei, alias,qo,rcSqlList, params));
		
		return this.findCountBySql(sb.toString(), params);
	}
	
	@Override
	public <QO extends QueryObject> Long findCountByQo(QO qo){
		return findCountByQo(qo,getEntityClass());
	}

//	@Override
//	public List<EO> findByMap(Map<String, ?> conditionParams, String orderBy) {
//		return findByMap(conditionParams,orderBy, getEntityClass());
//	}
	
	
	@Override
	public <E extends EntityObject,QO extends QueryObject> List<E> findByQo(QO qo,String orderBy,Class<E>entityClass){
//		return findByMap(EntityUtils.copyQoToMap(qo),orderBy,entityClass);
		
		EoMetaInfo<E> ei=EntityUtils.getEoInfo(entityClass);
		
		List<Object> params=new ArrayList<Object>();
		List<String>rcSqlList=new ArrayList<String>();
		
		
		String alias=SqlHelper.getDefAlias(ei);
		
		StringBuffer sb=new StringBuffer(SqlHelper.getSelectSql(entityClass,alias,rcSqlList));
		
		sb.append(SqlHelper.getWhereSql(ei, alias,qo,rcSqlList, params));
		
		sb.append(SqlHelper.getOrderBySql(StringUtils.isNotBlank(orderBy)?orderBy:ei.getOrderBy(), alias));
		
		List<E>list=findBySql(sb.toString(), params, entityClass);
		return list;
	}
	
	
	@Override
	public <QO extends QueryObject> List<EO> findByQo(QO qo, String orderBy) {
		return findByQo(qo,orderBy,getEntityClass());
	}


	@Override
	public <E extends EntityObject,QO extends QueryObject> Page<Map<String,?>> findPageMapByQo(PageQo page,QO qo,Class<E>entityClass){
		return findPageMapByQo(page,qo,null,entityClass);
	}
	
	@Override
	public <E extends EntityObject,QO extends QueryObject> Page<Map<String,?>> findPageMapByQo(PageQo page,QO qo,String orderBy,Class<E>entityClass){
//		return findPageMapByMap(page,EntityUtils.copyQoToMap(qo),orderBy,entityClass);
		
		EoMetaInfo<E> ei=EntityUtils.getEoInfo(entityClass);
//		
		List<Object> params=new ArrayList<Object>();
		
		List<String>rcSqlList=new ArrayList<String>();
		
		String alias=SqlHelper.getDefAlias(ei);
		
		StringBuffer sb=new StringBuffer(SqlHelper.getSelectSql(entityClass,alias,rcSqlList));
		
		sb.append(SqlHelper.getWhereSql(ei, alias,qo, rcSqlList,params));
		
		sb.append(SqlHelper.getOrderBySql(StringUtils.isNotBlank(orderBy)?orderBy:ei.getOrderBy(), alias));
		
		return findPageMapBySql(page,sb.toString(),params);
		
	}
	
//	@Override
//	public Page<EO> findPageByMap(Page<?> page, Map<String, ?> conditionParams) {
//		return findPageByMap(page,conditionParams, getEntityClass());
//	}
	
	
	
	@Override
	public <E extends EntityObject,QO extends QueryObject> Page<E> findPageByQo(PageQo page,QO qo,Class<E>entityClass){
//		return findPageByMap(page,EntityUtils.copyQoToMap(qo), entityClass);
		return this.findPageByQo(page, qo, null, entityClass);
	}
	
	
	@Override
	public <E extends EntityObject,QO extends QueryObject> Page<E> findPageByQo(PageQo page,QO qo,String orderBy,Class<E>entityClass){
		
		EoMetaInfo<E> ei = EntityUtils.getEoInfo(entityClass);
		
		List<Object> params = new ArrayList<Object>();
		
		List<String> rcSqlList = new ArrayList<String>();
		
		String alias = SqlHelper.getDefAlias(ei);
		
		StringBuffer sb = new StringBuffer(SqlHelper.getSelectSql(entityClass,alias,rcSqlList));
		
		sb.append(SqlHelper.getWhereSql(ei, alias,qo,rcSqlList, params));
		
		Long totalCount = this.findCountBySql(SqlHelper.convertToCountSql(sb.toString()),params);
		
		Page<E> resPage = new Page<E>(page.getPageNo(),page.getPageSize());
		
		resPage.setCount(totalCount);
		resPage.setNotCount(true);
		
		if(totalCount == 0){
			List<E> data=new ArrayList<E>();
			resPage.setList(data);
			return resPage;
		}
		
		sb.append(SqlHelper.getOrderBySql(StringUtils.isNotBlank(orderBy)?orderBy:ei.getOrderBy(), alias));
		
		List<E> list = this.findBySql(resPage, sb.toString(), params,entityClass);
		
		resPage.setList(list);
		
		return resPage;
		
	}
	
	
	
	@Override
	public <QO extends QueryObject>Page<EO> findPageByQo(PageQo page, QO qo){
//		return findPageByMap(page,EntityUtils.copyQoToMap(qo));
		return this.findPageByQo(page, qo, null, getEntityClass());
	}


//	@Override
//	public Page<EO> findPageByMap(Page<?> page, Map<String, ?> conditionParams,String orderBy) {
//		return findPageByMap(page,conditionParams,orderBy, getEntityClass());
//	}
	
	@Override
	public <QO extends QueryObject>Page<EO> findPageByQo(PageQo page, QO qo,String orderBy) {
		return this.findPageByQo(page, qo, orderBy, getEntityClass());
		
	}

	@Override
	public List<EO> findBySql(String sql, List<?> params) {
		return findBySql(sql,params, getEntityClass());
	}

	@Override
	public Page<EO> findPageBySql(PageQo page, String sql, List<?> params) {
		return findPageBySql(page,sql,params, getEntityClass());
	}

	@Override
	public int executeSql(String sql) {
		return executeSql(sql,new ArrayList<Object>());
	}
	

	@Override
	public int executeSql(String sql, List<?>  params) {
		logger.debug("执行语句:"+sql);
		logger.debug("参数:"+JsonMapper.toJsonString(params));
		return getDelegate().executeSql(sql, params);
	}
	
	private EO add(EO entity, Class<EO>entityClass) {
		
		EoMetaInfo<EO> ei = EntityUtils.getEoInfo(entityClass);
		List<EoFieldVal<EO>>valList=DaoReflectionUtil.getEntityFieldVals(entity,entityClass);
		
		if(valList.isEmpty()){
			logger.warn("entity 所有可持久化属性都是空的");
			return entity;
		}
		List<Object> params=new ArrayList<Object>();
		String addSql=SqlHelper.getAddSql(ei, valList,params);
		executeSql(addSql, params);
		return entity;
	}
	
	
	
	
	public int updateId(ID newId,ID oldId){
		EoMetaInfo<EO> ei=EntityUtils.getEoInfo(getEntityClass());
		if(ei.getKeyFieldInfo()==null){
			throw new JadEntityParseException(String.format("updateId执行出错，实体%s没有定义key", entityClass.getName()));
		}
		Assert.notNull(newId);
		Assert.notNull(oldId);
		List<Object>params=new ArrayList<Object>();
		params.add(newId);
		params.add(oldId);
		
		StringBuffer sb=new StringBuffer();
		sb.append("update ").append(ei.getTableName()).append(" set ");
		String pkColumn=ei.getKeyFieldInfo().getColumn();
		sb.append(pkColumn).append(" = ? where ").append(pkColumn).append("=?") ;
		
		return executeSql(sb.toString(), params);
	}
	
	private int updateById(EO entity, ID id, Class<EO>entityClass) {
		
		EoMetaInfo<EO> ei=EntityUtils.getEoInfo(entityClass);
		
		if(ei.getKeyFieldInfo()==null){
			throw new JadEntityParseException(String.format("updateById执行出错，实体%s没有定义key", entityClass.getName()));
		}
		
		List<EoFieldVal<EO>>valList=DaoReflectionUtil.getEntityFieldVals(entity,entityClass);
		if(valList.isEmpty()){
			logger.warn("entity 所有可持久化属性都是空的");
			return 0;
		}
		List<Object>params=new ArrayList<Object>();
		String sql=SqlHelper.getUpdateSql(ei,valList,params);
		
		sql=sql+" where "+ei.getKeyFieldInfo().getColumn()+"=? ";
		params.add(id);
		
		return executeSql(sql, params);
	}
	
	
	
//	private int updateByVo(T entity,RootVo conditionParams,Class<T>entityClass){
//		return updateByMap(entity,EoInfoUtils.toMap(conditionParams),entityClass);
//	}
	
	
//	private int updateByMap(EO entity, Map<String, ?> conditionParams, Class<EO>entityClass) {
//		
//		Assert.notNull(entity, "entity can not null");
//		Assert.notNull(entityClass, "entityClass can not null");
//		
//		
//		EoMetaInfo<EO> ei=EntityUtils.getEoInfo(entityClass);
//		List<EoFieldVal<EO>>valList=ReflectionUtils.getEntityFieldVals(entity,entityClass);
//		if(valList.isEmpty()){
//			logger.warn("entity 所有可持久化属性都是空的");
//			return 0;
//		}
//		List<Object>params=new ArrayList<Object>();
//		String sql=SqlHelper.getUpdateSql(ei,valList,params);
//		
//		sql=sql+SqlHelper.getWhereSql(ei,null,conditionParams,null,params);
//		
//		logger.debug("执行更新语句:"+sql);
//		return executeSql(sql, params);
//	}
	
	
	private int updateByIdList(EO entity, List<ID> idList,Class<EO>entityClass) {
		
		EoMetaInfo<EO> ei=EntityUtils.getEoInfo(entityClass);
		
		if(ei.getKeyFieldInfo()==null){
			throw new JadEntityParseException(String.format("updateByIdList执行出错，实体%s没有定义key", entityClass.getName()));
		}
		
		
		List<EoFieldVal<EO>>valList=DaoReflectionUtil.getEntityFieldVals(entity,entityClass);
		if(valList.isEmpty()){
			logger.warn("entity 所有可持久化属性都是空的");
			return 0;
		}
		List<Object>params=new ArrayList<Object>();
		String sql=SqlHelper.getUpdateSql(ei,valList,params);
		
		sql=sql+" where "+ei.getKeyFieldInfo().getColumn()+" in "+SqlHelper.getValsInSql(idList,params);
		
		return executeSql(sql, params);
		
	}
	
	
	@Override
	public int deleteById(ID id, Class<EO>entityClass) {
		
		
		EoMetaInfo<EO> ei=EntityUtils.getEoInfo(entityClass);
		Assert.notNull(ei.getKeyFieldInfo(), String.format("deleteById执行出错，实体%s没有定义key", entityClass.getName()));
		
		String sql="delete from "+ei.getTableName()+" where "+ei.getKeyFieldInfo().getColumn()+"=? ";
		List<Object>params=new ArrayList<Object>();
		params.add(id);
		return executeSql(sql, params);
	}
	
	
	@Override
	public int deleteByIdList(List<ID> idList,Class<EO>entityClass) {
		
		EoMetaInfo<EO> ei=EntityUtils.getEoInfo(entityClass);
		
		Assert.notNull(ei.getKeyFieldInfo(), String.format("deleteByIdList执行出错，实体%s没有定义key", entityClass.getName()));
		if(!StringUtils.isNotBlank(ei.getKeyFieldInfo().getColumn())){
			throw new IllegalArgumentException(ei.getObjName()+"中，没有找到id字段");
		}
		
		List<Object>params=new ArrayList<Object>();
		
		StringBuffer sb=new StringBuffer();
		sb.append("delete from ").append(ei.getTableName());
		sb.append(" where ").append(ei.getKeyFieldInfo().getColumn());
		sb.append(" in ").append(SqlHelper.getValsInSql(idList, params));
		
		
		return executeSql(sb.toString(), params);
	}

	
	
	
	
//	@Override
//	public int deleteByMap(Map<String, ?> mapParams, Class<EO>entityClass) {
//		
//		Assert.notNull(mapParams, "mapParams can not null");
//		Assert.notNull(entityClass, "entityClass can not null");
//		
//		
//		EoMetaInfo<EO> ei=EntityUtils.getEoInfo(entityClass);
//		
//		String sql="delete from "+ei.getTableName();
//		
//		List<Object>params=new ArrayList<Object>();
//		
//		sql=sql+SqlHelper.getWhereSql(ei, null, mapParams,null, params);
//		
//		logger.debug("执行删除语句:"+sql);
//		return executeSql(sql, params);
//	}

	
	
	@Override
	public <E extends EntityObject> E findById(ID id, Class<E>entityClass) {
		
		
		EoMetaInfo<E> ei=EntityUtils.getEoInfo(entityClass);
		
		Assert.notNull(ei.getKeyFieldInfo(), String.format("findById执行出错，实体%s没有定义key", entityClass.getName()));
		
		List<String>rcSqlList=new ArrayList<String>();
		
		String alias=SqlHelper.getDefAlias(ei);
		
		StringBuffer sb=new StringBuffer(SqlHelper.getSelectSql(entityClass,alias,rcSqlList));
		
		sb.append(" where ");
		if(!rcSqlList.isEmpty()){
			for(String rcSql:rcSqlList){
				sb.append(rcSql);
			}
			sb.append(" and ");
		}
		
		sb.append(alias).append(".").append(ei.getKeyFieldInfo().getColumn()).append("=? ");
		
		List<Object> params=new ArrayList<Object>();
		params.add(id);
		
		List<E>list=findBySql(sb.toString(), params, entityClass);
		
		if(list!=null && !list.isEmpty()){
			return list.get(0);
		}
		return null;
		
	}

	
//	@Override
//	public <E extends EntityObject> List<E> findByMap(Map<String, ?> conditionParams,Class<E>entityClass) {
//		return findByMap(conditionParams,null,entityClass);
//	}

	
//	@Override
//	public<E extends EntityObject> Page<E> findPageByMap(Page<?> page,
//			Map<String, ?> conditionParams, Class<E>entityClass) {
//		return findPageByMap(page,conditionParams, null,entityClass);
//	}
//
	@SuppressWarnings("rawtypes")
	public Long findCountBySql(String sql,List<?>params) {
		
		List<Map> list=this.findBySql(sql, params, Map.class);
		
		if(list==null || list.isEmpty() || list.get(0) == null || list.get(0).isEmpty() ){
			return 0L;
		}
		
		String key = SqlHelper.TOTAL_COUNT_COLUMN.toUpperCase();
		if(!list.get(0).containsKey(key)){
			key = SqlHelper.TOTAL_COUNT_COLUMN.toLowerCase();
			if(!list.get(0).containsKey(key)){
				Iterator it = list.get(0).keySet().iterator();
				key = it.next().toString();
			}
		}
		
		return MapUtils.getLongValue(list.get(0), key);
	}

	
//	@Override
//	public <E extends EntityObject> Page<E> findPageByMap(Page<?> page,
//			Map<String, ?> conditionParams, String orderBy,
//			Class<E>entityClass) {
//		
//		
//		Assert.notNull(conditionParams, "conditionParams can not null");
//		Assert.notNull(entityClass, "entityClass can not null");
//		
//		EoMetaInfo<E> ei=EntityUtils.getEoInfo(entityClass);
//		
//		List<Object> params=new ArrayList<Object>();
//		
//		List<Object>rcSqlList=new ArrayList<Object>();
//		
//		String alias=SqlHelper.getDefAlias(ei);
//		
//		StringBuffer sb=new StringBuffer(SqlHelper.getSelectSql(entityClass,alias,rcSqlList));
//		
//		sb.append(SqlHelper.getWhereSql(ei, alias,conditionParams,rcSqlList, params));
//		
//		Integer totalCount=this.countBySql(SqlHelper.convertToCountSql(sb.toString()),params);
//		
//		Page<E>resPage=new Page<E>(page.getPageNo(),page.getPageSize());
//		
//		resPage.setCount(totalCount);
//		if(totalCount==0){
//			List<E> data=new ArrayList<E>();
//			resPage.setList(data);
//			return resPage;
//		}
//		
//		sb.append(SqlHelper.getOrderBySql(StringUtils.isNotBlank(orderBy)?orderBy:ei.getOrderBy(), alias));
//		
////		if(StringUtils.isNotBlank(orderBy)){
////			sb.append(" order by ").append(orderBy);
////		}else{
////			String defOrderBy=ei.getOrderBy();
////			if(StringUtils.isNotBlank(defOrderBy)){
////				sb.append(" order by ").append(defOrderBy);
////			}
////		}
//		
//		List<E>list=this.findBySql(resPage, sb.toString(), params,entityClass);
//		resPage.setList(list);
//		
//		return resPage;
//		
//		
//	}
	
	

	@Override
	public <E> List<E> findBySql(String sql, List<?> params,
			Class<E> entityClass) {
		
		return findBySql(null,sql, params, entityClass);
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public <E> Page<E> findPageBySql(PageQo page, String sql, List<?> params,
			Class<E>resultTypeClass) {
		
		Page<E>resPage=new Page<E>(page.getPageNo(),page.getPageSize());
		
		Long totalCount=this.findCountBySql(SqlHelper.convertToCountSql(sql),params);
		
		
		
		resPage.setCount(totalCount);
		resPage.setNotCount(true);
		
		if(totalCount==0){
			List<E> data=new ArrayList<E>();
			resPage.setList(data);
			return resPage;
		}
		
		
		List<E>list=this.findBySql(resPage, sql, params,(Class<E>)this.getEntityClass());
		resPage.setList(list);
		return resPage;
		
	}


	@Override
	public <E extends EntityObject,QO extends QueryObject> List<Map<String, ?>> findMapByQo(QO qo,Class<E>entityClass){
//		return findMapByMap(EntityUtils.copyQoToMap(qo),entityClass);
		return this.findMapByQo(qo, null, entityClass);
	}
	
	@Override
	public <E extends EntityObject,QO extends QueryObject> List<Map<String, ?>> findMapByQo(QO qo,String orderBy,Class<E>entityClass){
//		return findMapByMap(EntityUtils.copyQoToMap(qo),orderBy,entityClass);
		
		
		EoMetaInfo<E> ei=EntityUtils.getEoInfo(entityClass);
		
		List<Object> params=new ArrayList<Object>();
		
		List<String>rcSqlList=new ArrayList<String>();
		
		String alias=SqlHelper.getDefAlias(ei);
		
		StringBuffer sb=new StringBuffer(SqlHelper.getSelectSql(entityClass,alias,rcSqlList));
		
		sb.append(SqlHelper.getWhereSql(ei, alias,qo,rcSqlList, params));
		
		sb.append(SqlHelper.getOrderBySql(StringUtils.isNotBlank(orderBy)?orderBy:ei.getOrderBy(), alias));
		
		List<Map<String, ?>>list=findMapBySql(sb.toString(), params);
		
		return list;
		
	}


//	@Override
//	public <E extends EntityObject> List<Map<String, ?>> findMapByMap(
//			Map<String, ?> conditionParams, Class<E>entityClass) {
//		return findMapByMap(conditionParams,null,entityClass);
//	}

	
//	@Override
//	public <E extends EntityObject> List<E> findByMap(Map<String, ?> conditionParams,String orderBy, Class<E>entityClass) {
//		
//		
//		Assert.notNull(conditionParams, "conditionParams can not null");
//		Assert.notNull(entityClass, "entityClass can not null");
//		
//		EoMetaInfo<E> ei=EntityUtils.getEoInfo(entityClass);
//		
//		List<Object> params=new ArrayList<Object>();
//		List<Object>rcSqlList=new ArrayList<Object>();
//		
//		String alias=SqlHelper.getDefAlias(ei);
//		
//		StringBuffer sb=new StringBuffer(SqlHelper.getSelectSql(entityClass,alias,rcSqlList));
//		
//		sb.append(SqlHelper.getWhereSql(ei, alias,conditionParams,rcSqlList, params));
//		
//		sb.append(SqlHelper.getOrderBySql(StringUtils.isNotBlank(orderBy)?orderBy:ei.getOrderBy(), alias));
//		
//		List<E>list=findBySql(sb.toString(), params, entityClass);
//		
//		return list;
//	}
	
	
//	@Override
//	public <E extends EntityObject> List<Map<String, ?>> findMapByMap(Map<String, ?> conditionParams, String orderBy,Class<E>entityClass) {
//		
//		
//		Assert.notNull(conditionParams, "conditionParams can not null");
//		Assert.notNull(entityClass, "entityClass can not null");
//		
//		EoMetaInfo<E> ei=EntityUtils.getEoInfo(entityClass);
//		
//		List<Object> params=new ArrayList<Object>();
//		
//		List<Object>rcSqlList=new ArrayList<Object>();
//		
//		String alias=SqlHelper.getDefAlias(ei);
//		
//		StringBuffer sb=new StringBuffer(SqlHelper.getSelectSql(entityClass,alias,rcSqlList));
//		
//		sb.append(SqlHelper.getWhereSql(ei, alias,conditionParams,rcSqlList, params));
//		
//		sb.append(SqlHelper.getOrderBySql(StringUtils.isNotBlank(orderBy)?orderBy:ei.getOrderBy(), alias));
//		
////		if(StringUtils.isNotBlank(orderBy)){
////			sb.append(" order by ").append(orderBy);
////		}else{
////			String defOrderBy=ei.getOrderBy();
////			if(StringUtils.isNotBlank(defOrderBy)){
////				sb.append(" order by ").append(defOrderBy);
////			}
////		}
//		
//		List<Map<String, ?>>list=findMapBySql(sb.toString(), params);
//		
//		return list;
//	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Map<String, ?>> findMapBySql(String sql,
			List<?> params) {
		List list=this.findBySql(sql, params, Map.class);
		return list;
	}

//	@Override
//	public <E extends EntityObject> Page<Map<String, ?>> findPageMapByMap(Page<?> page,
//			Map<String, ?> conditionParams, Class<E>entityClass) {
//		return findPageMapByMap(page,conditionParams,null,entityClass);
//	}
//
//	@Override
//	public <E extends EntityObject> Page<Map<String, ?>> findPageMapByMap(Page<?> page,
//			Map<String, ?> conditionParams, String orderBy,
//			Class<E>entityClass) {
//		
//		Assert.notNull(conditionParams, "conditionParams can not null");
//		Assert.notNull(entityClass, "entityClass can not null");
//		
//		
//		EoMetaInfo<E> ei=EntityUtils.getEoInfo(entityClass);
//		
//		List<Object> params=new ArrayList<Object>();
//		
//		List<Object>rcSqlList=new ArrayList<Object>();
//		
//		String alias=SqlHelper.getDefAlias(ei);
//		
//		StringBuffer sb=new StringBuffer(SqlHelper.getSelectSql(entityClass,alias,rcSqlList));
//		
//		sb.append(SqlHelper.getWhereSql(ei, alias,conditionParams, rcSqlList,params));
//		
//		sb.append(SqlHelper.getOrderBySql(StringUtils.isNotBlank(orderBy)?orderBy:ei.getOrderBy(), alias));
//		
////		if(StringUtils.isNotBlank(orderBy)){
////			sb.append(" order by ").append(orderBy);
////		}else{
////			String defOrderBy=ei.getOrderBy();
////			if(StringUtils.isNotBlank(defOrderBy)){
////				sb.append(" order by ").append(defOrderBy);
////			}
////		}
//		return findPageMapBySql(page,sb.toString(),params);
//	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Page<Map<String, ?>> findPageMapBySql(PageQo page, String sql,
			List<?>params) {
		
		Long totalCount=this.findCountBySql(SqlHelper.convertToCountSql(sql),params);

		
		Page<Map<String, ?>>resPage=new Page<Map<String, ?>>(page.getPageNo(),page.getPageSize());
		resPage.setCount(totalCount);
		resPage.setNotCount(true);
		
		if(totalCount==0){
			List<Map<String, ?>> data=new ArrayList<Map<String, ?>>();
			resPage.setList(data);
			return resPage;
		}
		
		List list=this.findBySql(resPage,sql, params,Map.class);
		resPage.setList(list);
		return resPage;
		
		
	}
	
	
	@Override
	public <E> List<E> findBySql(Page<?> page,String sql, List<?>params,Class<E> entityClass) {
		logger.debug("执行查询:"+sql);
		logger.debug("参数:"+JsonMapper.toJsonString(params));
		return this.getDelegate().findBySql(page, sql, params, entityClass);
	}
	

	@Override
	public void setBeanName(String name) {
		this.beanName=name;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext=applicationContext;
	}

	public String getRepositoryType() {
		return repositoryType;
	}

	public void setRepositoryType(String repositoryType) {
		this.repositoryType = repositoryType;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if(repositoryType==null){
			if(applicationContext.containsBean(REPOSITORY_TYPE_BEAN_NAME)){
				repositoryType=applicationContext.getBean(REPOSITORY_TYPE_BEAN_NAME).toString();
			}
		}
	}

	public void setEntityClass(Class<EO> entityClass) {
		this.entityClass = entityClass;
	}



	
}
