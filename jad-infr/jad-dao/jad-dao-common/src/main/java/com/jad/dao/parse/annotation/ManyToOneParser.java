package com.jad.dao.parse.annotation;

import java.lang.reflect.Field;

import javax.persistence.ManyToOne;

import com.jad.commons.vo.EntityObject;
import com.jad.dao.entity.EoFieldInfo;
import com.jad.dao.enums.RelateLoadMethod;

public class ManyToOneParser extends AbstractEoFieldAnnotationParser<ManyToOne>{

	@Override
	public <EO extends EntityObject> boolean parseAnnotationEoField(
			EoFieldInfo<EO> eoFieldInfo, Field field, ManyToOne annotation) {
		eoFieldInfo.setColumn(true);
		
		if(EntityObject.class.isAssignableFrom(field.getType())){
			eoFieldInfo.setRelateColumn(true);
		}
		
		eoFieldInfo.setRelateLoadMethod(RelateLoadMethod.NOT_AUTO);
		
		eoFieldInfo.setFieldName(field.getName());
		eoFieldInfo.setFieldType(field.getType());
		
		eoFieldInfo.setColumn(field.getName());
		
		return true;
	}


}
