/**
 * Copyright (c) 2011-2014, hubin (jobob@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.jad.dao.utils;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.enums.QueryOperateType;
import com.jad.commons.reflection.ReflectionUtils;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.QueryObject;
import com.jad.dao.entity.EoFieldInfo;
import com.jad.dao.entity.EoFieldVal;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.entity.QoFieldInfo;
import com.jad.dao.entity.QoFieldVal;
import com.jad.dao.entity.WhereCondition;
import com.jad.dao.enums.JavaType;
import com.jad.dao.enums.RelateLoadMethod;
import com.jad.dao.exception.JadSqlException;

/**
 *
 */
public class SqlHelper {
	
	public static final Logger logger = LoggerFactory.getLogger(SqlHelper.class);

	/**
	 * 空字符
	 */
	public static final String EMPTY = "";

	/**
	 * 下划线字符
	 */
	public static final char UNDERLINE = '_';

	
	public static final String TOTAL_COUNT_COLUMN="C";
	
	
//	public static <E extends EntityObject> String getWhereSql(EoMetaInfo<E> ei,String alias,
//			Map<String, Object> conditionParams,List<Object>params){
//		return getWhereSql(ei,alias,conditionParams,null,params);
//	}
	
	private static <E extends EntityObject> String getWhereField(String alias,
			WhereCondition<E> wc,String conditionKey,Object conditionVal,List<Object>params){
		
		EoFieldInfo<E> fi=wc.getFieldInfo();
		
		StringBuffer sb=new StringBuffer();
		
		String column=fi.getColumn();
		
//		alias
		if(StringUtils.isNotBlank(alias)){
			sb.append(alias).append(".").append(column);
		}else{
			sb.append(column);
		}
		
		EoFieldVal<E> fv=new EoFieldVal<E>(fi,conditionVal);
		
		sb.append(" ").append(wc.getOperateType().getExpression()).append(" ");
		
		String type=JavaType.getJavaType(fi.getFieldType().getName()).getType();
		
		if(JavaType.isBool(type)){
			if(QueryOperateType.isSimpleSingleOperate(wc.getOperateType().getType())){
				
				toConvertBoolean(sb,params,fv);
			}
			
		}else if(JavaType.isDateOrTime(type)){
			if(QueryOperateType.isSimpleSingleOperate(wc.getOperateType().getType())){
				toConvertData(sb,params,fv);
			}
			else if(QueryOperateType.between.getType().equals(wc.getOperateType().getType())){
				Object val=fv.getValue();
				if(val.getClass().isArray()){
					Object[]valArr=(Object[])val;
					if(valArr.length!=2 || valArr[0]==null || valArr[1]==null){
						throw new JadSqlException("在表"+wc.getEi().getTableName()+"跟据"+conditionKey+"自动匹配where条件的类型为："
								+wc.getOperateType().getType()+"这需要传入一个数组类型的参数值，且有且只有两个非空元素");
					}
					
					EoFieldVal<E> fv1=new EoFieldVal<E>(fi,valArr[0]);
					EoFieldVal<E> fv2=new EoFieldVal<E>(fi,valArr[1]);
					
					toConvertData(sb,params,fv1);
					sb.append(" and ");
					toConvertData(sb,params,fv2);
					
				}else{
					throw new JadSqlException("在表"+wc.getEi().getTableName()+"跟据"+conditionKey+"自动匹配where条件的类型为："
							+wc.getOperateType().getType()+"这需要传入一个数组类型的参数值");
				}
				
			}
		}else{
			if(QueryOperateType.isSimpleSingleOperate(wc.getOperateType().getType())){
				sb.append("?");
				params.add(fv.getValue());
			}else if(QueryOperateType.like.getType().equals(wc.getOperateType().getType()) 
					|| QueryOperateType.notLike.getType().equals(wc.getOperateType().getType()) ){
				sb.append("CONCAT('%', ?, '%')");
				
				params.add(fv.getValue());
			}else if(QueryOperateType.likeLeft.getType().equals(wc.getOperateType().getType()) 
					|| QueryOperateType.notLikeLeft.getType().equals(wc.getOperateType().getType()) ){
				sb.append("CONCAT('%', ? )");
				params.add(fv.getValue());
			}else if(QueryOperateType.likeRight.getType().equals(wc.getOperateType().getType()) 
					|| QueryOperateType.notLikeRight.getType().equals(wc.getOperateType().getType()) ){
				
				sb.append("CONCAT( ?, '%')");
				params.add(fv.getValue());
				
			}else if(QueryOperateType.in.getType().equals(wc.getOperateType().getType()) 
					|| QueryOperateType.notIn.getType().equals(wc.getOperateType().getType()) ){
				
				Object val=fv.getValue();
				if(List.class.isAssignableFrom(val.getClass()) || ((List<?>)val).isEmpty()){
					sb.append(getValsInSql((List<?>)val,params));
				}else{
					throw new JadSqlException("在表"+wc.getEi().getTableName()+"跟据"+conditionKey+"自动匹配where条件的类型为："
							+wc.getOperateType().getType()+"这需要传入一列表类型的参数值");
				}
			}
			else if(QueryOperateType.between.getType().equals(wc.getOperateType().getType())){
				Object val=fv.getValue();
				if(val.getClass().isArray()){
					Object[]valArr=(Object[])val;
					if(valArr.length!=2 || valArr[0]==null || valArr[1]==null){
						throw new JadSqlException("在表"+wc.getEi().getTableName()+"跟据"+conditionKey+"自动匹配where条件的类型为："
								+wc.getOperateType().getType()+"这需要传入一个数组类型的参数值，且有且只有两个非空元素");
					}
					
					sb.append(" ?");
					params.add(valArr[0]);
					sb.append(" and ? ");
					params.add(valArr[1]);
					
				}else{
					throw new JadSqlException("在表"+wc.getEi().getTableName()+"跟据"+conditionKey+"自动匹配where条件的类型为："
							+wc.getOperateType().getType()+"这需要传入一个数组类型的参数值");
				}
				
				
			}
			
		}
		return sb.toString();
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static <EO extends EntityObject,QO extends QueryObject> String getWhereField(
			EoMetaInfo<EO> ei,String alias,QoFieldVal<QO> qfv,List<Object>params){
		
		//保存返回的结果
		StringBuffer sb=new StringBuffer();
		
		//qo字段信息
		QoFieldInfo<QO> fieldInfo=qfv.getInfo();
		
		//where条件
		WhereCondition<EO> wc = null;
		
		//条件操作类型
		QueryOperateType specialType = null;
		
		String key = null;
		
		if(fieldInfo.getCurd()!=null && fieldInfo.getCurd().queryConf()!=null 
				&& fieldInfo.getCurd().queryConf().property() !=null ){
			specialType = fieldInfo.getCurd().queryConf().queryOperateType();
			key = fieldInfo.getCurd().queryConf().property();
		}
		
		if(!StringUtils.isNotBlank(key)){
			key = fieldInfo.getFieldName();
		}
			
		Object val=qfv.getValue();
		
		wc=EntityUtils.getWhereCondition(ei, key,specialType);
		if(wc==null || !wc.isValid() ){
			throw new JadSqlException("在eo:"+ei.getObjName()+"中无法跟据,qo:"+qfv.getInfo().getQoInfo().getObjName()+"的"+key+"自动匹配 where条件");
		}
		
//		if(!wc.isValid()){
////			return sb.toString();
//			throw new JadSqlException("在实体"+ei.getObjName()+"中无法跟据"+key+"自动匹配 where条件");
//		}
		
		EoFieldInfo<EO> fi=wc.getFieldInfo();
		
		if(fi.isRelateColumn() && EntityUtils.isRelateProperty(key,EntityUtils.MAX_RELATE_DEPTH) 
				&& !RelateLoadMethod.NOT_AUTO.equals(fi.getRelateLoadMethod()) ){
			
			String rAlia = fi.getRelateAlias();
			
			if(!StringUtils.isNotBlank(rAlia)){
				rAlia = fi.getFieldName();
			}
			
			EoMetaInfo rEi = EntityUtils.getEoInfo(fi.getFieldType());
			String rKey = EntityUtils.getRelatePropertyName(key);
			
			WhereCondition rWc = EntityUtils.getWhereCondition(rEi, rKey,wc.getOperateType());
			if(rWc == null || !rWc.isValid() ){
				throw new JadSqlException("在实体"+rEi.getObjName()+"中无法跟据"+rKey+"自动匹配 where条件");
			}
			sb.append(getWhereField(rAlia,rWc,rKey,val,params));
		}else{
			
			sb.append(getWhereField(alias,wc,key,val,params));
		}
			
		return sb.toString();
	}
	
	public static <EO extends EntityObject,QO extends QueryObject> String getWhereSql(
			EoMetaInfo<EO> ei,String alias,
			QO qo,List<String>relateConditionSql,List<Object>params,boolean isfirst){

		
		StringBuffer sb=new StringBuffer();
		
		if(relateConditionSql!=null && !relateConditionSql.isEmpty()){
			for(String rcSql:relateConditionSql){
				if(isfirst){
					sb.append(" where ");
					isfirst=false;
				}else{
					sb.append(" and ");
				}
				sb.append(rcSql);
			}
		}
		
		if(qo==null){
			logger.warn("qo is null,ei name:"+ei.getObjName());
			return sb.toString();
		}
		
		List<QoFieldVal<QO>>fieldValList=DaoReflectionUtil.getQoFieldVals(qo);
		
		for(QoFieldVal<QO> fieldVal:fieldValList){
			
			if( fieldVal.getValue() == null){
				continue;
			}
			
			String subSql=getWhereField(ei, alias, fieldVal,  params);
			
			if(StringUtils.isNotBlank(subSql)){
				if(isfirst){
					sb.append(" where ");
					isfirst=false;
				}else{
					sb.append(" and ");
				}
				sb.append(subSql);
			}
		}
		
		return sb.toString();
		
	
	}
	
	public static <EO extends EntityObject,QO extends QueryObject> String getWhereSql(
			EoMetaInfo<EO> ei,String alias,
			QO qo,List<String>relateConditionSql,List<Object>params){
		
		return getWhereSql(ei,alias,qo,relateConditionSql,params,true);
		
	}
//	public static <E extends EntityObject> String getWhereSql(EoMetaInfo<E> ei,String alias,
//			Map<String, ?> conditionParams,List<Object>relateConditionSql,List<Object>params){
//		
//		StringBuffer sb=new StringBuffer();
//		
//		boolean isfirst=true;
//		if(relateConditionSql!=null && !relateConditionSql.isEmpty()){
//			for(Object rcSql:relateConditionSql){
//				if(isfirst){
//					sb.append(" where ");
//					isfirst=false;
//				}else{
//					sb.append(" and ");
//				}
//				sb.append(rcSql);
//			}
//			
//		}
//		
//		if(conditionParams==null || conditionParams.isEmpty() || params==null){
//			return sb.toString();
//		}
//		
//		for(Map.Entry<String, ?>ent:conditionParams.entrySet()){
//			
//			if(!StringUtils.isNotBlank(ent.getKey()) ){
//				continue;
//			}
//			
//			WhereCondition<E> wc=EntityUtils.getWhereCondition(ei, ent.getKey(),null);
//			if(wc==null){
//				throw new JadSqlException("在实体"+ei.getObjName()+"中无法跟据"+ent.getKey()+"自动匹配 where条件");
//			}
//			if(!wc.isValid()){
//				continue;
//			}
//			
//			if(isfirst){
//				sb.append(" where ");
//				isfirst=false;
//			}else{
//				sb.append(" and ");
//			}
//			
//			EoFieldInfo<E> fi=wc.getFieldInfo();
//			
//			if(fi.isRelateColumn() && EntityUtils.isRelateProperty(ent.getKey()) 
//					&& !RelateLoadMethod.NOT_AUTO.equals(fi.getRelateLoadMethod()) ){
//				
//				String rAlia=fi.getRelateAlias();
//				if(!StringUtils.isNotBlank(rAlia)){
//					rAlia=fi.getFieldName();
//				}
//				
//				EoMetaInfo rEi=EntityUtils.getEoInfo(fi.getFieldType());
//				String rKey=EntityUtils.getRelatePropertyName(ent.getKey());
//				
//				WhereCondition<E> rWc=EntityUtils.getWhereCondition(rEi, rKey,null);
//				if(rWc==null){
//					throw new JadSqlException("在实体"+rEi.getObjName()+"中无法跟据"+rKey+"自动匹配 where条件");
//				}
//				
//				
//				sb.append(getWhereField(rAlia,rWc,rKey,ent.getValue(),params));
//				
//			}else{
//				
//				sb.append(getWhereField(alias,wc,ent.getKey(),ent.getValue(),params));
//			}
//			
//			
//			
//			
//		}
//		return sb.toString();
//	}
	
	public static String getOrderBySql(String orderBy,String alias){
		
		String orderByStr=getOrderByField(orderBy,alias);
		if(StringUtils.isNotBlank(orderByStr)){
			return " order by "+orderByStr;
		}else{
			return "";
		}
		
	}
	
	public static String getOrderByField(String orderBy,String alias){
		
		if(!StringUtils.isNotBlank(alias) || !StringUtils.isNotBlank(orderBy)){
			return orderBy;
		}
		
		StringBuffer sb=new StringBuffer();
		
		String[]fieldArr=orderBy.trim().split(",");
		
		if(fieldArr.length == 1){
			sb.append(alias).append(".").append(orderBy);
			return sb.toString();
		}
		
		boolean isFirst=true;
		for(String field:fieldArr){
			if(isFirst){
				isFirst=false;
			}else{
				sb.append(",");
			}
			sb.append(alias).append(".").append(field);
		}
		return sb.toString();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String getValsInSql(List  valList,List params){
		StringBuffer idin=new StringBuffer();
		boolean isfirst=true;
		for(Object val:valList){
			if(isfirst){
				isfirst=false;
			}else{
				idin.append(",");
			}
			idin.append("?");
			params.add(val);
		}
		return " ("+idin.toString()+") ";
	}
	
	
	public static <E extends EntityObject> String getSelectFrom(EoMetaInfo<E> ei,String alias,List<String>rcSqlList){

		StringBuffer sb=new StringBuffer();
		
		sb.append(ei.getTableName());
		if(!StringUtils.isNotBlank(alias)){
			alias=ei.getTableName();
		}
		
		if(!ei.getTableName().equalsIgnoreCase(alias)){
			sb.append(" ").append(alias);
		}
		
		
		for(EoFieldInfo<E> fi:ei.getFieldInfoMap().values()){
			if(!fi.isColumn() || !fi.isRelateColumn() ){
				continue;
			}
			EoMetaInfo rEi=EntityUtils.getEoInfo(fi.getFieldType());//关联信息
			
			//左关联
			if(RelateLoadMethod.LEFT_JOIN.getType().equals(fi.getRelateLoadMethod().getType())){
				
				
				String relateAlias=fi.getRelateAlias();
				if(!StringUtils.isNotBlank(relateAlias)){
//					relateAlias=fi.getFieldName()+"_1";
					relateAlias=fi.getFieldName();
				}
				
				
				
				sb.append(" left join ").append(rEi.getTableName()).append(" ").append(relateAlias).append(" on ");
				sb.append(alias).append(".").append(fi.getColumn()).append("=");
				
				String relateColumn=EntityUtils.getEoInfo(fi.getFieldType()).getKeyFieldInfo().getColumn();
				
				sb.append(relateAlias).append(".").append(relateColumn);
				
			}else if(RelateLoadMethod.DECLARE.getType().equals(fi.getRelateLoadMethod().getType())){
//				String relateAlias=fi.getFieldName()+"_1";
				
				String relateAlias=fi.getRelateAlias();
				if(!StringUtils.isNotBlank(relateAlias)){
//					relateAlias=fi.getFieldName()+"_1";
					relateAlias=fi.getFieldName();
				}
				
				sb.append(",").append(rEi.getTableName());
				if(!rEi.getTableName().equalsIgnoreCase(alias)){
					sb.append(" ").append(relateAlias);
				}
				
				String relateColumn=EntityUtils.getEoInfo(fi.getFieldType()).getKeyFieldInfo().getColumn();
				
				StringBuffer rcSql=new StringBuffer();
				rcSql.append(alias).append(".").append(fi.getColumn()).append("=");
				rcSql.append(relateAlias).append(".").append(relateColumn);
				
				rcSqlList.add(rcSql.toString());
			}
			
		}
		
		return sb.toString();
	
	}
	
	public static String getTableName(EoMetaInfo<?> ei){
		return ei.getTableName();
	}
	
	public static String getDefAlias(EoMetaInfo<?> ei){
		return StringUtils.uncapitalize(ei.getMetaClass().getSimpleName());
	}
	
	public static <E extends EntityObject> String getSelectSql(
			Class<E> entityClass,String alias,List<String>rcSqlList){
		
		EoMetaInfo<E> ei=EntityUtils.getEoInfo(entityClass);
		
		if(!StringUtils.isNotBlank(alias)){
			alias=getDefAlias(ei);
		}
		
		StringBuffer sb=new StringBuffer();
		sb.append("select ");
		
		sb.append(getSelectColumn(ei,null,alias,0));
		
		sb.append(" from ");
		
		sb.append(getSelectFrom(ei,alias,rcSqlList));
		
		
		return sb.toString();
	}
	
	
	public static <E extends EntityObject> String getSelectColumn(EoMetaInfo<E> ei,String alias){
		return getSelectColumn(ei,null,alias,0);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static <E extends EntityObject> String getSelectColumn(
			EoMetaInfo<E> ei,String relateField,String alias,int relateDepth){


		if(ei==null || ei.getFieldInfoMap()==null || ei.getFieldInfoMap().isEmpty()){
			throw new JadSqlException("执行查询出错，参数错误");
		}
		
		if(!StringUtils.isNotBlank(relateField) && relateDepth>0){
			throw new JadSqlException("relateField can not blank");
		}
		
		StringBuffer sb=new StringBuffer();

		boolean isfirst=true;
		
		for(EoFieldInfo<E> fi:ei.getFieldInfoMap().values()){
			
			if(!fi.isColumn()){
				continue;
			}
			
			
			if(!fi.isRelateColumn()){//不是关联列
				
				if(isfirst){
					isfirst=false;
				}else{
					sb.append(",");
				}
				
				if(StringUtils.isNotBlank(alias)){
					sb.append(alias).append(".");					
				}
				sb.append(fi.getColumn());
				sb.append(" as ");
				if(relateDepth>0){
					if(StringUtils.isNotBlank(relateField)){
						sb.append("\"").append(relateField).append(".");
					}
					
					sb.append(fi.getFieldName()).append("\"");
				}else{
					sb.append("\"").append(fi.getFieldName()).append("\"");
				}
				
			}else{
				
				if(RelateLoadMethod.NOT_AUTO.getType().equals(fi.getRelateLoadMethod().getType())  || relateDepth>0 ){
					
					if(isfirst){
						isfirst=false;
					}else{
						sb.append(",");
					}
//					if(StringUtils.isNotBlank(relateField)){
//						sb.append(relateField).append(".");
//					}
					if(StringUtils.isNotBlank(alias)){
						sb.append(alias).append(".");
					}
//					primary_person
					
					sb.append(fi.getColumn());
					sb.append(" as ");
					sb.append("\"");
					
					if(relateDepth>0 && StringUtils.isNotBlank(relateField)){
						sb.append(relateField).append(".");
					}
					sb.append(fi.getFieldName());
					String relateProperty=EntityUtils.getEoInfo(fi.getFieldType()).getKeyFieldInfo().getFieldName();
					sb.append(".").append(relateProperty).append("\"");
				}else{
					//放到下一个循环中
				}
				
			}
			
		}
		
		//处理关联列
//		for(EoFieldInfo<E> fi:ei.getFieldInfos()){
		for(EoFieldInfo<E> fi:ei.getFieldInfoMap().values()){

			if(fi.isRelateColumn() && relateDepth==0 
					&& !RelateLoadMethod.NOT_AUTO.getType().equals(fi.getRelateLoadMethod().getType()) ){
				
				if(sb.length()>0){
					sb.append(",");
				}
				
				EoMetaInfo rEi=EntityUtils.getEoInfo(fi.getFieldType());//关联信息
				
				String relateAlias=fi.getRelateAlias();
				if(!StringUtils.isNotBlank(relateAlias)){
					relateAlias=fi.getField().getName();
				}
				
				
				sb.append(getSelectColumn(rEi,fi.getFieldName(),relateAlias,relateDepth+1 ));//递归把关联列都整出来
			}
		}
		
		return sb.toString();
	}
	
//	public static <E extends EntityObject> String getSelectColumn(EoMetaInfo<E> ei,String relateField,int relateDepth){
//		return getSelectColumn(ei,relateField,relateField,relateDepth);
//	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <E extends EntityObject> String getUpdateSql(EoMetaInfo<E> ei,List<EoFieldVal<E>>valList,List<Object>params){
		StringBuffer sb=new StringBuffer();
		sb.append("update ").append(ei.getTableName()).append(" set ");
		
		boolean isfirst=true;
		
		for(EoFieldVal<E> fv:valList){
			if(!fv.getInfo().isColumn() || fv.getValue()==null || !fv.getInfo().isUpdatable() ){
				continue;
			}
			if(!fv.getInfo().isRelateColumn()){
				
				if(isfirst){
					isfirst=false;
				}else{
					sb.append(",");
				}
				
				sb.append(fv.getInfo().getColumn()).append(" = ");
				
//				String type=JavaType.getJavaType(fv.getInfo().getFieldName()).getType();
				String type=JavaType.getJavaType(fv.getInfo().getFieldType().getName()).getType();
				
				if(JavaType.isBool(type)){
					
					toConvertBoolean(sb,params,fv);
					
				}else if(JavaType.isDateOrTime(type)){
					
					toConvertData(sb,params,fv);
					
				}
				else{
					sb.append("?");
					params.add(fv.getValue());
				}
				
				
			}else{//是关联属性
				
				EoMetaInfo rEi=EntityUtils.getEoInfo(fv.getInfo().getFieldType());
				
				
				Object rVal=ReflectionUtils.getFieldValue(fv.getValue(), rEi.getKeyFieldInfo().getFieldName());
				
				
				if(rVal!=null){
					
					EoFieldVal rFv=new EoFieldVal(rEi.getKeyFieldInfo(),rVal);
					
					if(isfirst){
						isfirst=false;
					}else{
						sb.append(",");
					}
					
					sb.append(fv.getInfo().getColumn()).append(" = ");
					
//					String type=JavaType.getJavaType(fv.getInfo().getFieldName()).getType();
					String type=JavaType.getJavaType(fv.getInfo().getFieldType().getName()).getType();
					
					if(JavaType.isBool(type)){
						
						toConvertBoolean(sb,params,rFv);
						
					}else if(JavaType.isDateOrTime(type)){
						
						toConvertData(sb,params,rFv);
						
					}
					else{
						sb.append("?");
						params.add(rFv.getValue());
					}
					
				}
			}
				
		}
		if(isfirst){
			throw new JadSqlException(String.format("参数错误，无法更新%s",ei.getObjName()));
		}
		return sb.toString();
	}
	
	public static <E extends EntityObject> String getAddSql(EoMetaInfo<E> ei,List<EoFieldVal<E>>valList,List<Object> params){
		
		StringBuffer sb=new StringBuffer();
		sb.append("insert into ").append(ei.getTableName()).append("(");
		
		boolean isfirst=true;
		for(EoFieldVal<E> fv:valList){
			
			if(!fv.getInfo().isColumn() || fv.getValue()==null || !fv.getInfo().isInsertable()){
				continue;
			}
			
			if(!fv.getInfo().isRelateColumn()){
				
				if(fv.getValue()!=null ){
					
					if(isfirst){
						isfirst=false;
					}else{
						sb.append(",");
					}
					sb.append(fv.getInfo().getColumn());
				
				}else if(!fv.getInfo().isNullable()){
					throw new JadSqlException(String.format("插入失败,字段%s的值不能为空", fv.getInfo().getColumn()));
				}
				
			}else{//是关联属性
				
				EoMetaInfo rEi=EntityUtils.getEoInfo(fv.getInfo().getFieldType());
				EoFieldInfo refi=rEi.getKeyFieldInfo();
				Object rVal=ReflectionUtils.getFieldValue(fv.getValue(), refi.getFieldName());
				
				if(rVal!=null){
					
					if(isfirst){
						isfirst=false;
					}else{
						sb.append(",");
					}
					sb.append(fv.getInfo().getColumn());
					
				}else if(!refi.isNullable()){
					throw new JadSqlException(String.format("插入失败,字段%s的值不能为空", refi.getColumn()));
				}
			}
		}
		
		sb.append(")values(");
		isfirst=true;
		for(EoFieldVal<E> fv:valList){
			
			if(!fv.getInfo().isColumn() || fv.getValue()==null || !fv.getInfo().isInsertable()){
				continue;
			}
			if(!fv.getInfo().isRelateColumn()){
				if(isfirst){
					isfirst=false;
				}else{
					sb.append(",");
				}
//				String type=JavaType.getJavaType(fv.getInfo().getFieldName()).getType();
				String type=JavaType.getJavaType(fv.getInfo().getFieldType().getName()).getType();
				
				if(JavaType.isBool(type)){
					
					toConvertBoolean(sb,params,fv);
					
				}else if(JavaType.isDateOrTime(type)){
					
					toConvertData(sb,params,fv);
					
				}else{
					sb.append("?");
					params.add(fv.getValue());
				}
				
			}else{ //是关联属性
				EoMetaInfo rEi=EntityUtils.getEoInfo(fv.getInfo().getFieldType());
				
				Object rVal=ReflectionUtils.getFieldValue(fv.getValue(), rEi.getKeyFieldInfo().getFieldName());
				
				if(rVal!=null){
					
					EoFieldVal rFv=new EoFieldVal(rEi.getKeyFieldInfo(),rVal);
					
					if(isfirst){
						isfirst=false;
					}else{
						sb.append(",");
					}
					
//					String type=JavaType.getJavaType(fv.getInfo().getFieldName()).getType();
					String type=JavaType.getJavaType(fv.getInfo().getFieldType().getName()).getType();
					
					if(JavaType.isBool(type)){
						
						toConvertBoolean(sb,params,rFv);
						
					}else if(JavaType.isDateOrTime(type)){
						
						toConvertData(sb,params,rFv);
						
					}else{
						sb.append("?");
						params.add(rFv.getValue());
					}
					
				}
			}
		}
		sb.append(")");
		if(isfirst){
			throw new JadSqlException(String.format("参数错误，无法更新%s",ei.getObjName()));
		}
		return sb.toString();
	}
	
	
	private static void toConvertBoolean(StringBuffer sb,List<Object> params,EoFieldVal fv){
		
		sb.append(" ? ");
		
		Boolean val=(Boolean)fv.getValue();
		
		String param=val.booleanValue()?"1":"0";
		params.add(param);
	}
	
	
	private static void toConvertData(StringBuffer sb,List<Object> params,EoFieldVal fv){
//		TODO 目前只支持myaql
//		str_to_date('2014-08-20 00:00:00', '%Y-%m-%d %H:%i:%s');
		
		EoFieldInfo info=fv.getInfo();
		
		String type=JavaType.getJavaType(fv.getInfo().getFieldType().getName()).getType();
		
		if(JavaType.utilDate.getType().equalsIgnoreCase(type) ){
			
			java.util.Date param=(java.util.Date)fv.getValue();
			
			if(info.getTemporalType()!=null){
				
				if(TemporalType.DATE.equals(info.getTemporalType())){
					
					sb.append(" str_to_date(?,'%Y-%m-%d') ");
					params.add(new SimpleDateFormat("yyyy-MM-dd").format(param));
					return;
				}else if(TemporalType.TIME.equals(info.getTemporalType())){
					
					sb.append(" str_to_date(?,'%H:%i:%s') ");
					params.add(new SimpleDateFormat("HH:mm:ss").format(param));
					return;
					
				}else{
					sb.append(" str_to_date(?,'%Y-%m-%d %H:%i:%s') ");
					params.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(param));
					return;
				}
				
			}else{
				sb.append(" str_to_date(?,'%Y-%m-%d') ");
				params.add(new SimpleDateFormat("yyyy-MM-dd").format(param));
				return;
			}
			
		}else if(JavaType.Calendar.getType().equalsIgnoreCase(type) ){
			
			java.util.Date param=((java.util.Calendar)fv.getValue()).getTime();
			
			if(info.getTemporalType()!=null){
				
				if(TemporalType.DATE.equals(info.getTemporalType())){
					
					sb.append(" str_to_date(?,'%Y-%m-%d') ");
					params.add(new SimpleDateFormat("yyyy-MM-dd").format(param));
					return;
					
				}else if(TemporalType.TIME.equals(info.getTemporalType())){
					
					sb.append(" str_to_date(?,'%H:%i:%s') ");
					params.add(new SimpleDateFormat("HH:mm:ss").format(param));
					return;
					
				}else{
					sb.append(" str_to_date(?,'%Y-%m-%d %H:%i:%s') ");
					params.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(param));
					return;
				}
				
			}else{
				sb.append(" str_to_date(?,'%Y-%m-%d') ");
				params.add(new SimpleDateFormat("yyyy-MM-dd").format(param));
				return;
			}
			
		}else if(JavaType.sqlDate.getType().equalsIgnoreCase(type) ){
			
			java.sql.Date param=(java.sql.Date)fv.getValue();
			sb.append(" str_to_date(?,'%Y-%m-%d') ");
			params.add(new SimpleDateFormat("yyyy-MM-dd").format(param));
			return;
			
		}else if(JavaType.sqlTime.getType().equalsIgnoreCase(type) ){
			
			java.sql.Time param=(java.sql.Time)fv.getValue();
			sb.append(" str_to_date(?,'%H:%i:%s') ");
			params.add(new SimpleDateFormat("HH:mm:ss").format(param));
			return;
			
		}else if(JavaType.Timestamp.getType().equalsIgnoreCase(type) ){
			
			java.sql.Timestamp param=(java.sql.Timestamp)fv.getValue();
			sb.append(" str_to_date(?,'%Y-%m-%d %H:%i:%s') ");
			params.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(param));
			return;
			
		}else{
			sb.append(" str_to_date(?,'%Y-%m-%d %H:%i:%s') ");
			params.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fv.getValue().toString()));
			return;
		}
		
	}
	
	

	public static String convertToCountSql(String sql){
		return "select count(*)  "+TOTAL_COUNT_COLUMN+" "+sql.substring(sql.toUpperCase().indexOf("FROM"));
	}
	
	
	/** 
     * 去除sql的orderBy子句。 
     * @param hql 
     * @return 
     */  
    @SuppressWarnings("unused")
	public static String removeOrders(String qlString) {  
        Pattern p = Pattern.compile("order\\s*by[\\w|\\W|\\s|\\S]*", Pattern.CASE_INSENSITIVE);  
        Matcher m = p.matcher(qlString);  
        StringBuffer sb = new StringBuffer();  
        while (m.find()) {  
            m.appendReplacement(sb, "");  
        }
        m.appendTail(sb);
        return sb.toString();  
    }
	

	/**
	 * <p>
	 * 字符串驼峰转下划线格式
	 * </p>
	 *
	 * @param param
	 *            需要转换的字符串
	 * @return 转换好的字符串
	 */
//	public static String camelToUnderline(String param) {
//		if (StringUtils.isEmpty(param)) {
//			return EMPTY;
//		}
//		int len = param.length();
//		StringBuilder sb = new StringBuilder(len);
//		for (int i = 0; i < len; i++) {
//			char c = param.charAt(i);
//			if (Character.isUpperCase(c) && i > 0) {
//				sb.append(UNDERLINE);
//			}
//			sb.append(Character.toLowerCase(c));
//		}
//		return sb.toString();
//	}

	/**
	 * <p>
	 * 字符串下划线转驼峰格式
	 * </p>
	 *
	 * @param param
	 *            需要转换的字符串
	 * @return 转换好的字符串
	 */
//	public static String underlineToCamel(String param) {
//		if (StringUtils.isEmpty(param)) {
//			return EMPTY;
//		}
//		String temp = param.toLowerCase();
//		int len = temp.length();
//		StringBuilder sb = new StringBuilder(len);
//		for (int i = 0; i < len; i++) {
//			char c = temp.charAt(i);
//			if (c == UNDERLINE) {
//				if (++i < len) {
//					sb.append(Character.toUpperCase(temp.charAt(i)));
//				}
//			} else {
//				sb.append(c);
//			}
//		}
//		return sb.toString();
//	}

	

//	/**
//	 * <p>
//	 * 使用单引号包含字符串
//	 * </p>
//	 *
//	 * @param obj
//	 *            原字符串
//	 * @return 单引号包含的原字符串
//	 */
//	public static String quotaMark(Object obj) {
//		String srcStr = String.valueOf(obj);
//		if (obj instanceof String) {
//			// fix #79
//			return StringEscape.escapeString(srcStr);
//		}
//		return srcStr;
//	}

//	/**
//	 * <p>
//	 * 用%连接like
//	 * </p>
//	 *
//	 * @param str
//	 *            原字符串
//	 * @return
//	 */
//	public static String concatLike(String str, SQLlike type) {
//		switch (type) {
//		case LEFT:
//			str = "%" + str;
//			break;
//		case RIGHT:
//			str += "%";
//			break;
//		default:
//			str = "%" + str + "%";
//		}
//		return StringEscape.escapeString(str);
//	}

	/**
	 * <p>
	 * 使用单引号包含字符串
	 * </p>
	 *
	 * @param coll
	 *            集合
	 * @return 单引号包含的原字符串的集合形式
	 */
//	public static String quotaMarkList(Collection<?> coll) {
//		StringBuilder sqlBuild = new StringBuilder();
//		sqlBuild.append("(");
//		int _size = coll.size();
//		int i = 0;
//		Iterator<?> iterator = coll.iterator();
//		while (iterator.hasNext()) {
//			String tempVal = SqlHelper.quotaMark(iterator.next());
//			if (i + 1 == _size) {
//				sqlBuild.append(tempVal);
//			} else {
//				sqlBuild.append(tempVal);
//				sqlBuild.append(",");
//			}
//			i++;
//		}
//		sqlBuild.append(") ");
//		return sqlBuild.toString();
//	}

//	/**
//	 * <p>
//	 * 拼接字符串第二个字符串第一个字母大写
//	 * </p>
//	 *
//	 * @param concatStr
//	 * @param str
//	 * @return
//	 */
//	public static String concatCapitalize(String concatStr, final String str) {
//		if (StringUtils.isEmpty(concatStr)) {
//			concatStr = EMPTY;
//		}
//		int strLen;
//		if (str == null || (strLen = str.length()) == 0) {
//			return str;
//		}
//
//		final char firstChar = str.charAt(0);
//		if (Character.isTitleCase(firstChar)) {
//			// already capitalized
//			return str;
//		}
//
//		StringBuilder sb = new StringBuilder(strLen);
//		sb.append(concatStr);
//		sb.append(Character.toTitleCase(firstChar));
//		sb.append(str.substring(1));
//		return sb.toString();
//	}
//
//	/**
//	 * <p>
//	 * 字符串第一个字母大写
//	 * </p>
//	 *
//	 * @param str
//	 * @return
//	 */
//	public static String capitalize(final String str) {
//		return concatCapitalize(null, str);
//	}



}
