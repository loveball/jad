package com.jad.dao.jpa.support;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.json.JsonMapper;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.Page;
import com.jad.dao.enums.JavaType;
import com.jad.dao.jpa.hibernate.JadAliasToBeanResultTransformer;

public class JpaDaoHelper {
	
	private static Logger logger=LoggerFactory.getLogger(JpaDaoHelper.class);

	public static int executeSql(EntityManager entityManager,String sql, List<?> params) {
		
		logger.debug("执行sql:"+sql);
		logger.debug("参数:"+JsonMapper.toJsonString(params));
		Query query=entityManager.createNativeQuery(sql);
		if(params!=null){
			for(int i=0;i<params.size();i++){
				query.setParameter(i+1, params.get(i));
			}
		}
		return query.executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public static <E> List<E> findBySql(EntityManager entityManager,Page<?> page,String sql, List<?> params,Class<E> entityClass) {
		if(!StringUtils.isNotBlank(sql) || params==null || entityClass==null ){
			throw new IllegalArgumentException("执行查询出错，参数错误");
		}
		
		Session session = (Session) entityManager.getDelegate();   // 强转为 hibernate来执行
		
		logger.debug("执行sql:"+sql);
		logger.debug("参数:"+JsonMapper.toJsonString(params));
		
		SQLQuery query=session.createSQLQuery(sql);
		if(params!=null){
			for(int i=0;i<params.size();i++){
				query.setParameter(i, params.get(i));
			}
		}
		
		if(page!=null){
			
			int pageNo=page.getPageNo();
			pageNo=pageNo<=0?1:pageNo;
			
			int pageSize=page.getPageSize();
			pageSize=pageSize<=0?Page.DEF_PAGE_SIZE:pageSize;
			
			int firstResult = (pageNo - 1) * pageSize;
			
	        query.setMaxResults(pageSize);  
	        query.setFirstResult(firstResult);  
		}
		
		
		if(Map.class.isAssignableFrom(entityClass)){
			return query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
		}else if(JavaType.isBaseType(JavaType.getJavaType(entityClass.getName()).getType())){
			return query.list();
		}else{
			return query.setResultTransformer(JadAliasToBeanResultTransformer.getResultTransformer(entityClass)).list();
		}
	}
	
}
