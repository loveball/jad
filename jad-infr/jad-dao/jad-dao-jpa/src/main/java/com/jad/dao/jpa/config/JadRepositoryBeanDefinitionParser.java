package com.jad.dao.jpa.config;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.parsing.ReaderContext;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.beans.factory.xml.XmlReaderContext;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.repository.config.RepositoryBeanDefinitionParser;
import org.springframework.data.repository.config.RepositoryConfigurationExtension;
import org.springframework.data.repository.config.RepositoryConfigurationUtils;
import org.springframework.data.repository.config.XmlRepositoryConfigurationSource;
import org.springframework.util.Assert;
import org.w3c.dom.Element;

import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.enums.RespositoryType;

public class JadRepositoryBeanDefinitionParser extends RepositoryBeanDefinitionParser {
	
	private final RepositoryConfigurationExtension extension;

	public JadRepositoryBeanDefinitionParser(RepositoryConfigurationExtension extension) {
		super(extension);
		Assert.notNull(extension);
		this.extension = extension;
	}
	
	public BeanDefinition parse(Element element, ParserContext parser) {

		XmlReaderContext readerContext = parser.getReaderContext();

		try {
			
			Environment environment = readerContext.getEnvironment();
			ResourceLoader resourceLoader = readerContext.getResourceLoader();
			BeanDefinitionRegistry registry = parser.getRegistry();
			
			//注入持久化类型
		    if(!registry.containsBeanDefinition(AbstractJadEntityDao.REPOSITORY_TYPE_BEAN_NAME) 
		    		&& !registry.isAlias(AbstractJadEntityDao.REPOSITORY_TYPE_BEAN_NAME) ){
				RootBeanDefinition beanDefinition=new RootBeanDefinition(String.class);
				beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
				beanDefinition.setLazyInit(false);
				ConstructorArgumentValues constructorArgumentValues=new ConstructorArgumentValues();
				constructorArgumentValues.addIndexedArgumentValue(0, RespositoryType.JPA.getRespositoryType());
				beanDefinition.setConstructorArgumentValues(constructorArgumentValues);
				registry.registerBeanDefinition(AbstractJadEntityDao.REPOSITORY_TYPE_BEAN_NAME, beanDefinition);//注册 repositoryType
			}

			XmlRepositoryConfigurationSource configSource = new XmlRepositoryConfigurationSource(element, parser, environment);
			JadRepositoryConfigurationDelegate delegate = new JadRepositoryConfigurationDelegate(configSource, resourceLoader,
					environment);

			RepositoryConfigurationUtils.exposeRegistration(extension, registry, configSource);

			for (BeanComponentDefinition definition : delegate.registerRepositoriesIn(registry, extension)) {
				readerContext.fireComponentRegistered(definition);
			}
			
			

		} catch (RuntimeException e) {
			handleError(e, element, readerContext);
		}

		return null;
	}
	
	private void handleError(Exception e, Element source, ReaderContext reader) {
		reader.error(e.getMessage(), reader.extractSource(source), e);
	}
	
	

}
