package com.jad.dao.jpa.support;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.repository.NoRepositoryBean;

import com.jad.commons.vo.Page;
import com.jad.dao.RootDao;

public class JpaDaoDelegate implements  RootDao{

	private EntityManager entityManager;
	
	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public int executeSql(String sql, List<?> params) {
		return JpaDaoHelper.executeSql(entityManager, sql, params);
	}

	@Override
	public <E> List<E> findBySql(Page<?> page, String sql, List<?> params,
			Class<E> entityClass) {
		return JpaDaoHelper.findBySql(entityManager, page, sql, params, entityClass);
	}
	
	
}
