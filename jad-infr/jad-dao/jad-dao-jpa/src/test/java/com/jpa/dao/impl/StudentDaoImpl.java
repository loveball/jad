package com.jpa.dao.impl;

import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;
import com.jpa.dao.StudentDao;
import com.jpa.entity.Student;

//@JadDao("studentDao")
@JadDao
public class StudentDaoImpl extends AbstractJadEntityDao<Student, String> implements StudentDao{

}
