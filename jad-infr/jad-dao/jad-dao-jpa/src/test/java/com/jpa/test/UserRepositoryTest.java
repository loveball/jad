package com.jpa.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.testng.annotations.Test;

import com.jpa.dao.UserRepository2;
import com.jpa.entity.User;

public class UserRepositoryTest {
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
    public void baseTest() throws Exception {
    	
    	JpaRepositoryFactoryBean b=null;
    	ApplicationContext context = new ClassPathXmlApplicationContext("spring-jpa.xml");
    	Object obj=context.getBean("userRepository2");//
    	UserRepository2 userRepository2=(UserRepository2)obj;
    	
        User user = new User();
        user.setName("Jay");
        user.setPassword("123456dd"+System.currentTimeMillis());
//        user.setBirthday("2008-08-08");
//        userRepository.save(user);
//        userRepository.delete(user);
//        userRepository.findOne(1);
        
//        beanName:studentDao
//        beanName:studentDaoDelegate
//        beanName:studentDaoInterface
        Object studentDao=context.getBean("studentDao");
        Object studentDaoImplDelegate=context.getBean("studentDaoImplDelegate");
        Object studentDaoImpl=context.getBean("studentDaoImpl");
        
//        List<User> userList=userRepository2.findAll();
        
        String[]names=context.getBeanDefinitionNames();
        for(String name:names){
        	System.out.println("beanName:"+name);
        }
        User user2=userRepository2.findUserByNameAndPassword("Jay", "ssffd");
        
//        User user2=userRepository2.findUserByAdddddd("Jay", "ssffd");
        
        Map params=new HashMap();
        params.put("name", "mybatis");
        
//        List<User>userlist=userRepository2.findByMap(params,User.class);
        
        List<User>userlist=null;
//        User usersave=new User();
//        usersave.setBirthday("2016-08-08");
//        usersave.setId(new Random().nextInt(10000));
//        usersave.setName("jad");
//        usersave.setPassword("password1");
//        userRepository2.insert(usersave);
//        
//        
//        usersave=new User();
//        usersave.setBirthday("2016-08-08");
//        usersave.setId(new Random().nextInt(10000));
//        usersave.setName("jad");
//        usersave.setPassword("password2");
//        userRepository2.insert(usersave);
        
//    	System.out.println("ok:"+userlist.size());	
//        System.out.println("查询结果:"+(user2==null)+",pwd:"+user2.getPassword());
    }

}
