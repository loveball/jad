package com.jad.dao.hibernate;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import com.jad.commons.json.JsonMapper;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.Page;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.RootDao;
import com.jad.dao.annotation.JadDao;
import com.jad.dao.enums.JavaType;

@JadDao(AbstractJadEntityDao.COMMON_DELETAGE_BEANNAME)
@Lazy(false)
public class HibernateDaoDelegate implements  RootDao  {
	
	
	private static Logger logger=LoggerFactory.getLogger(HibernateDaoDelegate.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public int executeSql(String sql, List params) {
		Query query=getSession().createSQLQuery(sql);
		if(params!=null && !params.isEmpty()){
			int i=0;
			for (Object param : params) {
				query.setParameter(i,param);
				i++;
			}
		}
		
		System.out.println("执行sql:"+sql);
		System.out.println("参数:"+(params==null?"null":JsonMapper.toJsonString(params)));
		
		return query.executeUpdate();
	}
	
	@SuppressWarnings("hiding")
	@Override
	public List findBySql(Page page,String sql, List params,Class entityClass) {
		
		if(!StringUtils.isNotBlank(sql) || entityClass==null ){
			throw new IllegalArgumentException("执行查询出错，参数错误");
		}
		Query query=getSession().createSQLQuery(sql);
		if(params!=null){
			for(int i=0;i<params.size();i++){
				query.setParameter(i, params.get(i));
			}
		}
		
		if(page!=null){
			
			int pageNo=page.getPageNo();
			pageNo=pageNo<=0?1:pageNo;
			
			int pageSize=page.getPageSize();
			pageSize=pageSize<=0?Page.DEF_PAGE_SIZE:pageSize;
			
			int firstResult = (pageNo - 1) * pageSize;
			
	        query.setMaxResults(pageSize);  
	        query.setFirstResult(firstResult);  
		}
		
		
		if(Map.class.isAssignableFrom(entityClass)){
			
			return query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
			
		}else if(JavaType.isBaseType(JavaType.getJavaType(entityClass.getName()).getType())){
			
			return query.list();
			
		}else{
			
			return query.setResultTransformer(JadAliasToBeanResultTransformer.getResultTransformer(entityClass)).list();
			
		}
		
		
	}



	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}


	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}







}
