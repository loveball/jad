package com.jad.dao.hibernate;

import java.lang.reflect.Method;

import org.hibernate.property.access.spi.Getter;
import org.hibernate.property.access.spi.GetterMethodImpl;
import org.hibernate.property.access.spi.PropertyAccess;
import org.hibernate.property.access.spi.PropertyAccessStrategy;
import org.hibernate.property.access.spi.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.reflection.ReflectionUtils;
import com.jad.dao.utils.EntityUtils;

public class JadPropertyAccessBasicImpl implements PropertyAccess {

	private static final Logger logger = LoggerFactory.getLogger( JadPropertyAccessBasicImpl.class );
	
	private final JadPropertyAccessStrategyBasicImpl strategy;
	private final GetterMethodImpl getter;
	private final JadSetterMethodImpl setter;

	public JadPropertyAccessBasicImpl(
			JadPropertyAccessStrategyBasicImpl strategy,
			Class containerJavaType,
			final String propertyName) {
	
		this.strategy = strategy;
		
		String realPropertyName=propertyName;
		if(EntityUtils.isRelateProperty(propertyName)){//属性是关联对像
			realPropertyName=EntityUtils.getRelateEntityName(propertyName);
		}
//		
//		final Method getterMethod = ReflectHelper.findGetterMethod( containerJavaType, realPropertyName );
//		logger.debug("findGetterMethod,containerJavaType:"+containerJavaType.getName()+",realPropertyName:"+realPropertyName);
		final Method getterMethod = ReflectionUtils.findGetterMethod( containerJavaType, realPropertyName );
		this.getter = new JadGetterMethodImpl( containerJavaType, propertyName, getterMethod );

//		final Method setterMethod = ReflectHelper.findSetterMethod( containerJavaType, realPropertyName, getterMethod.getReturnType() );
//		logger.debug("findSetterMethod,containerJavaType:"+containerJavaType.getName()+",realPropertyName:"+realPropertyName);
		final Method setterMethod = ReflectionUtils.findSetterMethod( containerJavaType, realPropertyName, getterMethod.getReturnType() );
		this.setter = new JadSetterMethodImpl( containerJavaType, propertyName, setterMethod,(JadGetterMethodImpl)getter );
	}
	
	@Override
	public PropertyAccessStrategy getPropertyAccessStrategy() {
		return strategy;
	}

	@Override
	public Getter getGetter() {
		return getter;
	}

	@Override
	public Setter getSetter() {
		return setter;
	}


}
