package com.jad.service;

import java.util.List;
import java.util.Map;

import com.jad.commons.vo.Page;
import com.jad.entity.User;


public interface UserService {
	User findById(String id);
	
	
	List<User> findBy(Map<String,Object>conditionParams,String orderBy);
	
	Page<User> findPageBy(Page<User> page,Map<String,Object>conditionParams,String orderBy);
	
	Page<Map<String,?>> findPageMapBy(Page page,Map<String,Object>conditionParams,String orderBy);
	
	
	List<User> findAll(String orderBy);

	
}
