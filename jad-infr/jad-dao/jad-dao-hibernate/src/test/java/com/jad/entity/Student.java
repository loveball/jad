package com.jad.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.jad.dao.annotation.RelateColumn;
import com.jad.dao.enums.RelateLoadMethod;


@Entity
@Table(name = "student")
public class Student extends BaseEntity{

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	
	@ManyToOne
	@RelateColumn(name="user_id",loadMethod=RelateLoadMethod.LEFT_JOIN)
	private User user;
	
	@Column
	private String name;
	
	@Column
	private String email;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	

}
