package com.jad.service;

import java.util.List;
import java.util.Map;

import com.jad.commons.vo.Page;
import com.jad.entity.TestEntityType;

public interface TestEntityTypeService {

	
	TestEntityType add(TestEntityType entity);
	
	TestEntityType findById(String id);
	
	List<TestEntityType> findBy(Map<String,Object>conditionParams );
	
	Page<TestEntityType> findPageBy(Page<TestEntityType> page,Map<String,Object>conditionParams,String orderBy);
	
	Page<Map<String,?>> findPageMapBy(Page page,Map<String,Object>conditionParams,String orderBy);
	
//	List<TestEntityType> findBy(String propertyName,Object value);
	
	List<TestEntityType> findAll();
	
	int updateByMap(TestEntityType entity,Map<String,Object>conditionParams);
	
}
