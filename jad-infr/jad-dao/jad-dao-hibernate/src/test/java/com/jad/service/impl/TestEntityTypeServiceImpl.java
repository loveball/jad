package com.jad.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jad.commons.vo.Page;
import com.jad.dao.TestEntityTypeDao;
import com.jad.entity.TestEntityType;
import com.jad.service.TestEntityTypeService;

@Service("testEntityTypeService")
public class TestEntityTypeServiceImpl implements TestEntityTypeService	{

	
	@Autowired
	private TestEntityTypeDao testEntityTypeDao;
	
	public int updateByMap(TestEntityType entity,Map<String,Object>conditionParams){
//		return testEntityTypeDao.updateByMap(entity, conditionParams);
		return 0; 
	}
	
	@Override
	public TestEntityType findById(String id) {
		
		return testEntityTypeDao.findById(id);
	}

	public TestEntityType add(TestEntityType entity){
		return testEntityTypeDao.add(entity);
	}
	@Override
	public List<TestEntityType> findBy(Map<String, Object> conditionParams) {
		
//		return testEntityTypeDao.findByMap(conditionParams);
		return null; 
	}

	@Override
	public Page<TestEntityType> findPageBy(Page<TestEntityType> page,
			Map<String, Object> conditionParams, String orderBy) {
//		return testEntityTypeDao.findPageByMap(page, conditionParams,orderBy);
		return null; 
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Page<Map<String,?>> findPageMapBy(Page page,
			Map<String, Object> conditionParams, String orderBy) {
//		return testEntityTypeDao.findPageMapByMap(page, conditionParams, orderBy,TestEntityType.class);
		return null; 
	}

//	@Override
//	public List<TestEntityType> findBy(String propertyName, Object value) {
//		return testEntityTypeDao.findBy(propertyName,value);
//	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<TestEntityType> findAll() {
		return findBy(new HashMap());
	}


}
