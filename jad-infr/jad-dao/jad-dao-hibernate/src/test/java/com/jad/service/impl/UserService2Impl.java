package com.jad.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jad.commons.vo.Page;
import com.jad.dao.UserDao2;
import com.jad.entity.User;
import com.jad.service.UserService;

@Service("userService2Impl")
public class UserService2Impl  implements UserService{

	@Autowired
	private UserDao2 userDao2;

	@Override
	public User findById(String id) {
		
		return userDao2.findById(id);
	}
	
	public List<User> findAll(String orderBy) {
		return findBy(new HashMap(),orderBy);
	}
	
	public Page<Map<String,?>> findPageMapBy(Page page,Map<String,Object>conditionParams,String orderBy) {
//		return userDao2.findPageMapByMap(page, conditionParams,orderBy,User.class);
		return null; 
	}
	public Page<User> findPageBy(Page<User> page,Map<String,Object>conditionParams,String orderBy) {
//		return userDao2.findPageByMap(page, conditionParams);
		return null;
	}
	
	public List<User> findBy(Map<String,Object>conditionParams,String orderBy) {
//		return userDao2.findByMap(conditionParams, orderBy);
		return null;
	}
	
	
	

}




