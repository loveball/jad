package com.jad.dao.impl;

import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.TestEntityTypeDao;
import com.jad.dao.annotation.JadDao;
import com.jad.entity.TestEntityType;
import com.jad.vo.TestEntityTypeVo;


@JadDao("testEntityTypeDao")
public class TestEntityTypeDaoImpl extends AbstractJadEntityDao<TestEntityType, String>
implements TestEntityTypeDao{


}
