package com.jad.dao.mybatis.service;

import java.util.List;

import com.jad.dao.mybatis.eo.TestDictEo;

public interface TestDictService {
	
	TestDictEo findById(String id);


}
