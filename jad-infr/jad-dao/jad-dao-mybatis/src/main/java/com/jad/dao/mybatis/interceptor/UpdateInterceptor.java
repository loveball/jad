package com.jad.dao.mybatis.interceptor;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;

/**
 * 拦截 insert、update、delete 语句，解析sql中问号占位符 
 * 
 * @author hechuan
 */
@Intercepts({ @Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class }) })
public class UpdateInterceptor extends JadAbstractInterceptor {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Object intercept(Invocation invocation) throws Throwable {

		MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
		
		Object parameter = invocation.getArgs()[1];
		
		MappedStatement newMs = super.parseParams(mappedStatement, parameter); //处理参数占位符
		if(mappedStatement!=newMs){
			invocation.getArgs()[0]  = newMs;
		}
		
		return invocation.proceed();
	}

}
