package com.jad.dao.mybatis.interceptor;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

/**
 * 拦截 select 语句，处理分页，返回值类型，解析sql中问号占位符
 * 
 * @author hechuan
 * 
 */
@Intercepts({ @Signature(type = Executor.class, method = "query", args = {
		MappedStatement.class, Object.class, RowBounds.class,
		ResultHandler.class }) })
public class QueryInterceptor extends JadAbstractInterceptor {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	


	@Override
	public Object intercept(Invocation invocation) throws Throwable {

		MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];

		// //拦截需要分页的SQL
		Object parameter = invocation.getArgs()[1];
		
		
		MappedStatement newMs = this.parseParams(mappedStatement, parameter);//处理参数占位符
		
		newMs = this.parsePageParams(newMs, parameter); //处理分页
		
		newMs = this.parseResultType(newMs, parameter);//处理返回值类型
		
		if(mappedStatement!=newMs){
	        invocation.getArgs()[0] = newMs;
		}

		return invocation.proceed();
	}
	
	
	

	
	
	
	
	
    
	


}
