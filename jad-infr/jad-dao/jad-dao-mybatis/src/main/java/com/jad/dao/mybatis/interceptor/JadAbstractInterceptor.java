package com.jad.dao.mybatis.interceptor;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.ibatis.binding.MapperMethod;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.ResultMap;
import org.apache.ibatis.mapping.ResultMapping;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.scripting.defaults.DefaultParameterHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.context.Global;
import com.jad.commons.reflection.ReflectionUtils;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.Page;
import com.jad.dao.dialect.Dialect;
import com.jad.dao.dialect.db.DB2Dialect;
import com.jad.dao.dialect.db.DerbyDialect;
import com.jad.dao.dialect.db.H2Dialect;
import com.jad.dao.dialect.db.HSQLDialect;
import com.jad.dao.dialect.db.MySQLDialect;
import com.jad.dao.dialect.db.OracleDialect;
import com.jad.dao.dialect.db.PostgreSQLDialect;
import com.jad.dao.dialect.db.SQLServer2005Dialect;
import com.jad.dao.dialect.db.SybaseDialect;
import com.jad.dao.utils.SqlHelper;

/**
 * 拦截器，提供分页，返回值类型，参数占位符等基本方法
 * 
 * @author hechuan
 *
 */
public abstract class JadAbstractInterceptor implements Interceptor,Serializable {

	private static final Logger logger = LoggerFactory.getLogger(JadAbstractInterceptor.class);
	
	private static final long serialVersionUID = 1L;
	
	protected static final String PAGE = "jadPage";

	protected static final String LIST_PARAM_PREFIX = "jadlistparamprefix";

	protected static final String LIST_PARAM_NAME = "jadListParam";
	
    public static final String DEFAULT_KEY = "jadResultType";
    
    private String jadResultType = DEFAULT_KEY;   
    
    protected Dialect dialect;

	/**
	 * 解析参数占位符
	 * 
	 * @param mappedStatement
	 * @param paramMap
	 * @param boundSql
	 */
	protected MappedStatement parseParams(MappedStatement mappedStatement,Object parameter) {
		if(parameter==null){
			return mappedStatement;
		}
		BoundSql boundSql = mappedStatement.getBoundSql(parameter);
		Object parameterObject = boundSql.getParameterObject();
		if(parameterObject==null || !(parameterObject instanceof MapperMethod.ParamMap)){
			return mappedStatement;
		}
		MapperMethod.ParamMap paramMap =(MapperMethod.ParamMap)parameterObject;
		
		if (!paramMap.containsKey(LIST_PARAM_NAME)
				|| !(paramMap.get(LIST_PARAM_NAME) instanceof List)) {
			return mappedStatement;
		}
		List paramList = (List) paramMap.get(LIST_PARAM_NAME);
		if (paramList == null || paramList.isEmpty()) {
			return mappedStatement;
		}

		List<ParameterMapping> addParamMappingList = new ArrayList<ParameterMapping>();

		for (int i = 0; i < paramList.size(); i++) {
			String key = LIST_PARAM_PREFIX + i;
			ParameterMapping pm = new ParameterMapping.Builder(
					mappedStatement.getConfiguration(), key, Object.class).build();
			addParamMappingList.add(pm);
			paramMap.put(key, paramList.get(i));
		}

		List<ParameterMapping> oldList = boundSql.getParameterMappings();

		List<ParameterMapping> newParamMappingList = new ArrayList<ParameterMapping>();

		newParamMappingList.addAll(oldList);

		newParamMappingList.addAll(addParamMappingList);

//		BoundSql newBoundSql = new BoundSql(mappedStatement.getConfiguration(), boundSql.getSql().trim(), 
//				newParamMappingList, paramMap);
//         Object metaObject = ReflectionUtils.getFieldValue(boundSql, "metaParameters");
//         if ( metaObject!= null) {
//             MetaObject mo = (MetaObject) metaObject;
//             ReflectionUtils.setFieldValue(newBoundSql, "metaParameters", mo);
//         }
		
		BoundSql newBoundSql = copyFromBoundSql(mappedStatement,boundSql,newParamMappingList,paramMap);
         
         MappedStatement newMs = copyFromMappedStatement(mappedStatement, new BoundSqlSqlSource(newBoundSql));
         
         return newMs;
	}
	
	/**
	 * 解析返回值类型
	 * @param ms
	 * @param parameterObject
	 * @return
	 */
	protected MappedStatement parseResultType(MappedStatement ms,Object parameterObject){
		
		if(parameterObject == null){
			return ms;
		}
		Class resultType = getResultType(parameterObject);
        
        if(resultType == null){
            return ms;
        }
        
        List<ResultMap> rmlist=ms.getResultMaps();
        if(rmlist!=null && rmlist.size()==1){
        	boolean eq = resultType.equals(rmlist.get(0).getType());
        	if(eq){
        		return ms;
        	}
        }
        MappedStatement newMs = this.copyFromMappedStatement(ms, resultType);
		return newMs;
	}
	
	
	/**
	 * 处理分页
	 * @param mappedStatement
	 * @param parameter
	 * @return
	 * @throws Throwable
	 */
	protected MappedStatement parsePageParams(MappedStatement mappedStatement,Object parameter) throws Throwable {
		
		if(parameter==null){
			return mappedStatement;
		}
		
		BoundSql boundSql = mappedStatement.getBoundSql(parameter);
		Object parameterObject = boundSql.getParameterObject();
		
		 //获取分页参数对象
        Page<Object> page = null;
        if (parameterObject != null) {
            page = convertParameter(parameterObject);
        }
        
        //如果设置了分页对象，则进行分页
        if (page == null || page.isDisabled()) {
        	return mappedStatement;
        }
        
        if (!StringUtils.isNotBlank(boundSql.getSql())){
        	return mappedStatement;
        }
        
        String originalSql = boundSql.getSql().trim();
        
      //得到总记录数
        if( !page.isNotCount()) {
        	page.setCount(getCount(originalSql, null, mappedStatement, parameterObject, boundSql));	
        }
      //分页查询 本地化对象 修改数据库注意修改实现
        String pageSql = generatePageSql(originalSql, page, dialect);
        if (logger.isDebugEnabled()) {
        	logger.debug("PAGE SQL:" + StringUtils.replace(pageSql, "\n", ""));
        }
        
        
        BoundSql newBoundSql = copyFromBoundSql(mappedStatement,boundSql,pageSql);
        
        MappedStatement newMs = copyFromMappedStatement(mappedStatement, new BoundSqlSqlSource(newBoundSql));

        
		return newMs;
		
	}

	
	
	/**
     * 查询总纪录数
     * @param sql             SQL语句
     * @param connection      数据库连接
     * @param mappedStatement mapped
     * @param parameterObject 参数
     * @param boundSql        boundSql
     * @return 总记录数
     * @throws SQLException sql查询错误
     */
    private int getCount(final String sql, final Connection connection,
    							final MappedStatement mappedStatement, final Object parameterObject,
    							final BoundSql boundSql) throws SQLException {
    	
		final String countSql = SqlHelper.convertToCountSql(sql);
		
        Connection conn = connection;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
        	if (logger.isDebugEnabled()) {
        		logger.debug("COUNT SQL: " + countSql );
            }
        	if (conn == null){
        		conn = mappedStatement.getConfiguration().getEnvironment().getDataSource().getConnection();
            }
        	ps = conn.prepareStatement(countSql);
        	
//            BoundSql countBS = new BoundSql(mappedStatement.getConfiguration(), countSql,
//                    boundSql.getParameterMappings(), parameterObject);
//            //解决MyBatis 分页foreach 参数失效 start
//			if (ReflectionUtils.getFieldValue(boundSql, "metaParameters") != null) {
//				MetaObject mo = (MetaObject) ReflectionUtils.getFieldValue(boundSql, "metaParameters");
//				ReflectionUtils.setFieldValue(countBS, "metaParameters", mo);
//			}
			//解决MyBatis 分页foreach 参数失效 end 
//            setParameters(ps, mappedStatement, countBS, parameterObject);
        	
        	BoundSql countBS = copyFromBoundSql(mappedStatement,boundSql,countSql);
        	DefaultParameterHandler parameterHandler = new DefaultParameterHandler(mappedStatement, parameterObject, countBS);  
            parameterHandler.setParameters(ps);  
            
            rs = ps.executeQuery();
            int count = 0;
            if (rs.next()) {
                count = rs.getInt(1);
            }
            return count;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
            	ps.close();
            }
            if (conn != null) {
            	conn.close();
            }
        }
    }
    
	
	/**
     * 对参数进行转换和检查
     * @param parameterObject 参数对象
     * @param page            分页对象
     * @return 分页对象
     * @throws NoSuchFieldException 无法找到参数
     */
    @SuppressWarnings({ "unchecked", "rawtypes"})
	private Page<Object> convertParameter(Object parameterObject) {
    	try{
            if (parameterObject instanceof Page) {
            	
                return (Page<Object>) parameterObject;
                
            }else if(parameterObject instanceof MapperMethod.ParamMap) {
            	
            	MapperMethod.ParamMap paramMap=(MapperMethod.ParamMap)parameterObject;
            	
            	if(paramMap.containsKey(PAGE) && (paramMap.get(PAGE) instanceof Page) ){
            		return (Page)paramMap.get(PAGE);
            	}
            	
            	return null;
            
            } else {
            	
            	
            	Field field = ReflectionUtils.getAccessibleField(parameterObject, PAGE);
            	if ( field != null) {
            		Object pageObject = field.get(parameterObject);
            		if(pageObject instanceof Page){
            			return (Page<Object>)pageObject;
            		}
            		
            	}
            	
            	return null;
                
            }
    	}catch (Exception e) {
    		logger.error("从参数中提取分页信息出错，"+e.getMessage(),e);
			return null;
		}
    }
    

    /**
     * 根据数据库方言，生成特定的分页sql
     * @param sql     Mapper中的Sql语句
     * @param page    分页对象
     * @param dialect 方言类型
     * @return 分页SQL
     */
    private String generatePageSql(String sql, Page<Object> page, Dialect dialect) {
        if (dialect.supportsLimit()) {
            return dialect.getLimitString(sql, page.getFirstResult(), page.getMaxResults());
        } else {
            return sql;
        }
    }

	
	
	private String getShortName(Class clazz){
//      String className = clazz.getCanonicalName();
//      className=className.substring(className.lastIndexOf(".") + 1);
      String className=clazz.getName().replaceAll("\\.", "");
      return className;
  }
	
	
	
	
	
	
	/**
	 *  复制BoundSql对象 
	 * @param ms
	 * @param oldBoundSql
	 * @param newParamMappingList
	 * @param newParamMap
	 * @return
	 */
	@SuppressWarnings("rawtypes") 
	protected BoundSql copyFromBoundSql(MappedStatement ms, BoundSql oldBoundSql,
			List<ParameterMapping> newParamMappingList,
			MapperMethod.ParamMap newParamMap) {
		
		BoundSql newBoundSql = new BoundSql(ms.getConfiguration(), oldBoundSql.getSql().trim(), 
				newParamMappingList, newParamMap);
		for (ParameterMapping mapping : oldBoundSql.getParameterMappings()) {
	        String prop = mapping.getProperty();
	        if (oldBoundSql.hasAdditionalParameter(prop)) {  
	            newBoundSql.setAdditionalParameter(prop, oldBoundSql.getAdditionalParameter(prop));  
	        }  
	    }
		return newBoundSql;
	}
	
	  /** 
	   * 复制BoundSql对象 
	   */  
	protected BoundSql copyFromBoundSql(MappedStatement ms, BoundSql oldBoundSql, String newSql) {  
	    BoundSql newBoundSql = new BoundSql(ms.getConfiguration(),newSql, oldBoundSql.getParameterMappings(), oldBoundSql.getParameterObject());  
	    for (ParameterMapping mapping : oldBoundSql.getParameterMappings()) {
	        String prop = mapping.getProperty();
	        if (oldBoundSql.hasAdditionalParameter(prop)) {  
	            newBoundSql.setAdditionalParameter(prop, oldBoundSql.getAdditionalParameter(prop));  
	        }  
	    }
	    return newBoundSql;  
	  }  
	  

	
	/**
	 * 复制新的 MappedStatement
	 * @param ms
	 * @param newSqlSource
	 * @return
	 */
	protected MappedStatement copyFromMappedStatement(MappedStatement ms,
			SqlSource newSqlSource) {
		
        //下面是新建的过程，考虑效率和复用对象的情况下，这里最后生成的ms可以缓存起来，
//    	下次根据 ms.getId() + "_" + getShortName(resultType) 直接返回 ms,省去反复创建的过程
		
		MappedStatement.Builder builder = new MappedStatement.Builder(
				ms.getConfiguration(), ms.getId(), newSqlSource,
				ms.getSqlCommandType());
		
		builder.resource(ms.getResource());
		builder.fetchSize(ms.getFetchSize());
		builder.statementType(ms.getStatementType());
		builder.keyGenerator(ms.getKeyGenerator());
//		if (ms.getKeyProperties() != null && ms.getKeyProperties().length != 0) {
//			for (String keyProperty : ms.getKeyProperties()) {
//				builder.keyProperty(keyProperty);
//			}
//		}
		if (ms.getKeyProperties() != null && ms.getKeyProperties().length != 0) {
            StringBuilder keyProperties = new StringBuilder();
            for (String keyProperty : ms.getKeyProperties()) {
                keyProperties.append(keyProperty).append(",");
            }
            keyProperties.delete(keyProperties.length() - 1, keyProperties.length());
            builder.keyProperty(keyProperties.toString());
        }
		builder.timeout(ms.getTimeout());
		builder.parameterMap(ms.getParameterMap());
		builder.resultMaps(ms.getResultMaps());
		builder.cache(ms.getCache());
		builder.flushCacheRequired(ms.isFlushCacheRequired());
		builder.useCache(ms.isUseCache());
		return builder.build();
	}
	
	
	/**
	 * 复制新的 MappedStatement
	 * @param ms
	 * @param resultType
	 * @return
	 */
	protected MappedStatement copyFromMappedStatement(MappedStatement ms, Class resultType) {
    	
        //下面是新建的过程，考虑效率和复用对象的情况下，这里最后生成的ms可以缓存起来，
//    	下次根据 ms.getId() + "_" + getShortName(resultType) 直接返回 ms,省去反复创建的过程
        
    	MappedStatement.Builder builder = new MappedStatement.Builder(
        		ms.getConfiguration(), ms.getId() + "_" + getShortName(resultType), 
        		ms.getSqlSource(), ms.getSqlCommandType());
        
        builder.resource(ms.getResource());
        builder.fetchSize(ms.getFetchSize());
        builder.statementType(ms.getStatementType());
        builder.keyGenerator(ms.getKeyGenerator());
        if (ms.getKeyProperties() != null && ms.getKeyProperties().length != 0) {
            StringBuilder keyProperties = new StringBuilder();
            for (String keyProperty : ms.getKeyProperties()) {
                keyProperties.append(keyProperty).append(",");
            }
            keyProperties.delete(keyProperties.length() - 1, keyProperties.length());
            builder.keyProperty(keyProperties.toString());
        }
        builder.timeout(ms.getTimeout());
        builder.parameterMap(ms.getParameterMap());
        //count查询返回值int
        List<ResultMap> resultMaps = new ArrayList<ResultMap>();
        List<ResultMapping> resultmapping = new ArrayList<ResultMapping>(0);
//        ResultMap resultMap = new ResultMap.Builder(ms.getConfiguration(), ms.getId(), resultType, EMPTY_RESULTMAPPING).build();
        ResultMap resultMap = new ResultMap.Builder(ms.getConfiguration(), ms.getId(), resultType, resultmapping).build();
        resultMaps.add(resultMap);
        builder.resultMaps(resultMaps);
        builder.resultSetType(ms.getResultSetType());
        builder.cache(ms.getCache());
        builder.flushCacheRequired(ms.isFlushCacheRequired());
        builder.useCache(ms.isUseCache());
        return builder.build();
    }
	
	
	/**
     * 获取设置的返回值类型
     *
     * @param parameterObject
     * @return
     */
    public Class getResultType(Object parameterObject){
        if (parameterObject == null) {
            return null;
        } else if (parameterObject instanceof Class) {
            return (Class)parameterObject;
        } else if (parameterObject instanceof Map) {
            //解决不可变Map的情况
            if(((Map)(parameterObject)).containsKey(jadResultType)){
                Object result = ((Map)(parameterObject)).get(jadResultType);
                return objectToClass(result);
            } else {
                return null;
            }
        } else {
//            MetaObject metaObject = SystemMetaObject.forObject(parameterObject);
//            Object result = metaObject.getValue(jadResultType);
//            return objectToClass(result);
        	return null;
        }
    }

    /**
     * 将结果转换为Class
     *
     * @param object
     * @return
     */
    private Class objectToClass(Object object){
        if(object == null){
            return null;
        } else if(object instanceof Class){
            return (Class)object;
        } else if(object instanceof String){
            try {
                return Class.forName((String)object);
            } catch (Exception e){
                throw new RuntimeException("非法的全限定类名字符串:" + object);
            }
        } else {
            throw new RuntimeException("方法参数类型错误，" + jadResultType + " 对应的参数类型只能为 Class 类型或者为 类的全限定名称字符串");
        }
    }



	protected static class BoundSqlSqlSource implements SqlSource {
		private BoundSql boundSql;

		public BoundSqlSqlSource(BoundSql boundSql) {
			this.boundSql = boundSql;
		}

		public BoundSql getBoundSql(Object parameterObject) {
			return boundSql;
		}
	}
	
	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

    @Override
    public void setProperties(Properties properties) {
    	parseDialect(properties);
        String resultType = properties.getProperty("jadResultType");
        if(resultType != null && resultType.length() > 0){
            this.jadResultType = resultType;
        }
    }
    
    /**
     * 设置属性，支持自定义方言类和制定数据库的方式
     * <code>dialectClass</code>,自定义方言类。可以不配置这项
     * <ode>dbms</ode> 数据库类型，插件支持的数据库
     * <code>sqlPattern</code> 需要拦截的SQL ID
     * @param p 属性
     */
    private void parseDialect(Properties p) {
    	Dialect dialect = null;
        String dbType = Global.getConfig("jdbc.type");
        if ("db2".equals(dbType)){
        	dialect = new DB2Dialect();
        }else if("derby".equals(dbType)){
        	dialect = new DerbyDialect();
        }else if("h2".equals(dbType)){
        	dialect = new H2Dialect();
        }else if("hsql".equals(dbType)){
        	dialect = new HSQLDialect();
        }else if("mysql".equals(dbType)){
        	dialect = new MySQLDialect();
        }else if("oracle".equals(dbType)){
        	dialect = new OracleDialect();
        }else if("postgre".equals(dbType)){
        	dialect = new PostgreSQLDialect();
        }else if("mssql".equals(dbType) || "sqlserver".equals(dbType)){
        	dialect = new SQLServer2005Dialect();
        }else if("sybase".equals(dbType)){
        	dialect = new SybaseDialect();
        }
        if (dialect == null) {
            throw new RuntimeException("mybatis dialect error.");
        }
        this.dialect = dialect;
    }

}
