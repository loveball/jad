/**
 * Copyright (c) 2011-2014, hubin (jobob@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.jad.dao.mybatis.enums;

/**
 * <p>
 * MybatisPlus 支持 SQL 方法
 * </p>
 * 
 * @author hubin
 * @Date 2016-01-23
 */
public enum SqlMethod {

	
	
	/**执行sql*/
	EXECUTE_SQL("executeSql", "执行sql", "<script>${jadSql}</script>"),
	
	FIND_BY_SQL("findBySql", "findBySql", "<script>${jadSql}</script>"),
	
	FIND_PAGE_BY_SQL("findPageBySql", "findPageBySql", "<script>${jadSql}</script>");
	
//	

	private final String method;
	
	private final String desc;

	private final String sql;


	SqlMethod( final String method, final String desc, final String sql ) {
		this.method = method;
		this.desc = desc;
		this.sql = sql;
	}


	public String getMethod() {
		return this.method;
	}


	public String getDesc() {
		return this.desc;
	}


	public String getSql() {
		return this.sql;
	}

}
