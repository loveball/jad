package com.jad.commons.web.listener;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;

public class WebContextListener extends org.springframework.web.context.ContextLoaderListener {
	
	public static final Logger logger = LoggerFactory.getLogger(WebContextListener.class);
	
	@Override
	public WebApplicationContext initWebApplicationContext(ServletContext servletContext) {

		WebApplicationContext wc=super.initWebApplicationContext(servletContext);
		
		return wc;
	}
}
