package com.jad.commons.web.api;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.jad.commons.json.JsonMapper;


/**
 * 操作结果
 * @author Administrator
 *
 */
public class Result<D> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String code;
	private String msg;
	
	private D data;
	
	public Result(){
	}
	
	public static void main(String[] args) {
		System.out.println(JsonMapper.toJsonString(Result.newResult(RespCode.NO_LOGIN, null)));
	}
	
	
	public Result(RespCode respCode,D data){
		this(respCode.getCode(),respCode.getMsg(),data);
	}
	public Result(RespCode respCode,String msg,D data){
		this(respCode.getCode(), msg==null?respCode.getMsg() : msg,data);
	}
	public Result(String code,String msg,D data){
		this.code = code;
		this.msg = msg;
		this.data = data;
	}
	
	public static <D> Result<D> newResult(RespCode respCode,D data){
		return new Result<D>(respCode,data);
	}
	public static <D> Result<D> newResult(RespCode respCode,String msg,D data){
		return new Result<D>(respCode,msg,data);
	}
	public static <D> Result<D> newSuccResult(){
		return new Result<D>(RespCode.SUCCESS,null);
	}
	public static <D> Result<D> newSuccResult(D data){
		return new Result<D>(RespCode.SUCCESS,data);
	}
	
	public void setSuccess(String msg) {
		this.code = RespCode.SUCCESS.getCode();
		this.msg = StringUtils.isBlank(msg)?RespCode.SUCCESS.getMsg():msg;
	}
	
	public void setParamFail(String msg) {
		this.code = RespCode.PARAM_FAIL.getCode();
		this.msg = StringUtils.isBlank(msg)?RespCode.PARAM_FAIL.getMsg():msg;
	}
	
	public void setBusiFail(String msg) {
		this.code = RespCode.BUSI_ERROR.getCode();
		this.msg = StringUtils.isBlank(msg)?RespCode.BUSI_ERROR.getMsg():msg;
	}
	
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}

	public D getData() {
		return data;
	}

	public void setData(D data) {
		this.data = data;
	}
	
//	/blog/1　HTTP　GET　=>　　得到id　=　1的blog
//			/blog/1　HTTP　DELETE　=>　删除　id　=　1的blog
//			/blog/1　HTTP　PUT　=>　　更新id　=　1的blog
//			/blog　　　HTTP　POST　=>　　新增BLOG
	
	
	

}





