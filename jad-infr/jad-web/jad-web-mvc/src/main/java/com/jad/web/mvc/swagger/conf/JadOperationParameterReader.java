package com.jad.web.mvc.swagger.conf;

import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Lists.newArrayList;
import static springfox.documentation.schema.Collections.isContainerType;
import static springfox.documentation.schema.Maps.isMapType;
import static springfox.documentation.schema.Types.isBaseType;
import static springfox.documentation.schema.Types.typeNameFor;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;

import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.ResolvedMethodParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;
import springfox.documentation.spi.service.contexts.ParameterContext;
import springfox.documentation.spring.web.plugins.DocumentationPluginsManager;

import com.fasterxml.classmate.ResolvedType;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.jad.commons.annotation.ApiCurdType;
import com.jad.commons.annotation.CURD;
import com.jad.commons.enums.CurdType;

public class JadOperationParameterReader implements OperationBuilderPlugin{

	  private final JadModelAttributeParameterExpander expander;

	  @Autowired
	  private DocumentationPluginsManager pluginsManager;

	  @Autowired
	  public JadOperationParameterReader(JadModelAttributeParameterExpander expander) {
	    this.expander = expander;
	  }

	  @Override
	  public void apply(OperationContext context) {
	    context.operationBuilder().parameters(context.getGlobalOperationParameters());
	    context.operationBuilder().parameters(readParameters(context));
	  }

	  @Override
	  public boolean supports(DocumentationType delimiter) {
	    return true;
	  }

	  private List<Parameter> readParameters(final OperationContext context) {

	    List<ResolvedMethodParameter> methodParameters = context.getParameters();
	    List<Parameter> parameters = newArrayList();

	    CurdType curdType =null;
	    Optional<ApiCurdType> curdTypeOpt = context.findAnnotation(ApiCurdType.class);
	    if(curdTypeOpt.isPresent()){
	    	 ApiCurdType type = curdTypeOpt.get();
	 	    curdType = type.value();
	    }
	   
//	    ReflectionUtils.getAccessibleField(context, "");
	    
	    for (ResolvedMethodParameter methodParameter : methodParameters) {
	      ResolvedType alternate = context.alternateFor(methodParameter.getParameterType());
	      
	      
	      alternate.getErasedType();
	      
//	      Optional<CURD> curdOpt = methodParameter.findAnnotation(CURD.class);
//	      if(curdOpt.isPresent()){
//	    	  CURD curd = curdOpt.get();
//		      String label = curd.label();
//		      System.out.println("label:"+label);
//	      }
	      
//	      TODO 这里要加过滤条件
	      
	      if (!shouldIgnore(methodParameter, alternate, context.getIgnorableParameterTypes())) {

	        ParameterContext parameterContext = new ParameterContext(methodParameter,
	            new ParameterBuilder(),
	            context.getDocumentationContext(),
	            context.getGenericsNamingStrategy(),
	            context);

	        if (shouldExpand(methodParameter, alternate)) {
	          parameters.addAll(
	              expander.expand(
	                  "",
	                  methodParameter.getParameterType(),
	                  context.getDocumentationContext(),curdType));
	        } else {
	          parameters.add(pluginsManager.parameter(parameterContext));
	        }
	      }
	    }
	    return FluentIterable.from(parameters).filter(not(hiddenParams())).toList();
	  }

	  private Predicate<Parameter> hiddenParams() {
	    return new Predicate<Parameter>() {
	      @Override
	      public boolean apply(Parameter input) {
	        return input.isHidden();
	      }
	    };
	  }

	  private boolean shouldIgnore(
	      final ResolvedMethodParameter parameter,
	      ResolvedType resolvedParameterType,
	      final Set<Class> ignorableParamTypes) {

	    if (ignorableParamTypes.contains(resolvedParameterType.getErasedType())) {
	      return true;
	    }
	    return FluentIterable.from(ignorableParamTypes)
	        .filter(isAnnotation())
	        .filter(parameterIsAnnotatedWithIt(parameter)).size() > 0;

	  }

	  private Predicate<Class> parameterIsAnnotatedWithIt(final ResolvedMethodParameter parameter) {
	    return new Predicate<Class>() {
	      @Override
	      public boolean apply(Class input) {
	        return parameter.hasParameterAnnotation(input);
	      }
	    };
	  }

	  private Predicate<Class> isAnnotation() {
	    return new Predicate<Class>() {
	      @Override
	      public boolean apply(Class input) {
	        return Annotation.class.isAssignableFrom(input);
	      }
	    };
	  }

	  private boolean shouldExpand(final ResolvedMethodParameter parameter, ResolvedType resolvedParamType) {
	    return (!parameter.hasParameterAnnotations() || parameter.hasParameterAnnotation(ModelAttribute.class))
	        && !isBaseType(typeNameFor(resolvedParamType.getErasedType()))
	        && !resolvedParamType.getErasedType().isEnum()
	        && !isContainerType(resolvedParamType)
	        && !isMapType(resolvedParamType);

	  }


}
