package com.jad.web.mvc.swagger.conf;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.BeanNameAware;

public class ApiDocketConf implements BeanNameAware {
	
	/**
	 * group名称，多个配置时不能相同
	 */
	private String groupName;
	
	/**
	 * ant 风格的path
	 */
	private String path;
	
	/**
	 * 标题
	 */
	private String title;
	

	/**
	 * 描述
	 */
	private String description;
	
	/**
	 * 团队地址
	 */
	private String termsOfServiceUrl;
	
	/**
	 * license
	 */
	private String license;
	
	/**
	 * licenseUrl
	 */
	private String licenseUrl;
	
	/**
	 * version
	 */
	private String version;
	
	/**
	 * 联系信息
	 */
	private String contactName;
	private String contactUrl;
	private String contactEmail;
	


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public void setPath(String path) {
		this.path = path;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public void setTermsOfServiceUrl(String termsOfServiceUrl) {
		this.termsOfServiceUrl = termsOfServiceUrl;
	}


	public void setLicense(String license) {
		this.license = license;
	}


	public void setLicenseUrl(String licenseUrl) {
		this.licenseUrl = licenseUrl;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public void setContactName(String contactName) {
		this.contactName = contactName;
	}


	public void setContactUrl(String contactUrl) {
		this.contactUrl = contactUrl;
	}


	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}


	public String getGroupName() {
		return groupName;
	}


	public String getPath() {
		return path;
	}


	public String getTitle() {
		return title;
	}


	public String getDescription() {
		return description;
	}


	public String getTermsOfServiceUrl() {
		return termsOfServiceUrl;
	}


	public String getLicense() {
		return license;
	}


	public String getLicenseUrl() {
		return licenseUrl;
	}


	public String getVersion() {
		return version;
	}


	public String getContactName() {
		return contactName;
	}


	public String getContactUrl() {
		return contactUrl;
	}


	public String getContactEmail() {
		return contactEmail;
	}


	@Override
	public void setBeanName(String name) {
		if(StringUtils.isBlank(groupName)){
			this.groupName = name;
		}
	}
	
	
}
