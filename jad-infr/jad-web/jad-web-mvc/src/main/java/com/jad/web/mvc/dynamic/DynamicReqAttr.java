package com.jad.web.mvc.dynamic;

import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.entity.QoMetaInfo;
import com.jad.dao.entity.VoMetaInfo;

public class DynamicReqAttr {

	private String name;
	
	private String serviceBeanName ;
	
	private String daoBeanName ;
	
	@SuppressWarnings("rawtypes")
	private EoMetaInfo eoMetaInfo;
	
	@SuppressWarnings("rawtypes")
	private VoMetaInfo voMetaInfo;
	
	@SuppressWarnings("rawtypes")
	private QoMetaInfo qoMetaInfo;
	
	private String listUrl;
	
	private String formUrl;
	
	public String getServiceBeanName() {
		return serviceBeanName;
	}


	public void setServiceBeanName(String serviceBeanName) {
		this.serviceBeanName = serviceBeanName;
	}


	public String getDaoBeanName() {
		return daoBeanName;
	}


	public void setDaoBeanName(String daoBeanName) {
		this.daoBeanName = daoBeanName;
	}


	public EoMetaInfo getEoMetaInfo() {
		return eoMetaInfo;
	}


	public void setEoMetaInfo(EoMetaInfo eoMetaInfo) {
		this.eoMetaInfo = eoMetaInfo;
	}


	public VoMetaInfo getVoMetaInfo() {
		return voMetaInfo;
	}


	public void setVoMetaInfo(VoMetaInfo voMetaInfo) {
		this.voMetaInfo = voMetaInfo;
	}


	public QoMetaInfo getQoMetaInfo() {
		return qoMetaInfo;
	}


	public void setQoMetaInfo(QoMetaInfo qoMetaInfo) {
		this.qoMetaInfo = qoMetaInfo;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getListUrl() {
		return listUrl;
	}


	public void setListUrl(String listUrl) {
		this.listUrl = listUrl;
	}


	public String getFormUrl() {
		return formUrl;
	}


	public void setFormUrl(String formUrl) {
		this.formUrl = formUrl;
	}
	
	protected DynamicReqAttr getDynamicReqAttr(){
		return null;
	}
	

}
