package com.jad.web.mvc.dynamic;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.context.SpringContextHolder;
import com.jad.commons.service.CurdService;
import com.jad.commons.utils.StringUtils;
import com.jad.web.mvc.dynamic.exception.DynamicReqException;

public class DynamicReqContext {
	
	
	private static final Logger logger = LoggerFactory.getLogger(DynamicReqContext.class);
	
	/**
	 * 请求
	 */
	private HttpServletRequest request;
	
	/**
	 * 响应
	 */
	private HttpServletResponse response;
	
	/**
	 * 模块或称
	 */
	private String modelName;
	
	/**
	 * 子模块名称
	 */
	private String subModel;
	
	private CurdService curdService;
	
	private DynamicReqAttr dynamicReqAttr;
	
	private String listUrl;
	
	private String formUrl;
	
	private DynamicReqContext() {
	}
	
	@SuppressWarnings("rawtypes")
	public static DynamicReqContext buildReqContext(HttpServletRequest request,HttpServletResponse response,
			String modelName,String subModel){
		
		String error = "无法访问地址:"+request.getRequestURI();
		
		if(StringUtils.isBlank(modelName) || StringUtils.isBlank(subModel) ){
			throw new DynamicReqException( error + ",缺少必要的参数");
		}
		
		DynamicReqContext context = new DynamicReqContext();
		context.setRequest(request);
		context.setResponse(response);
		context.setModelName(modelName);
		context.setSubModel(subModel);
		
		DynamicReqAttr dynamicReqAttr = DynamicReqUtil.findDynamicReqAttr(subModel);
		
		if(dynamicReqAttr == null){
			logger.warn(error+","+subModel+"不支持动态访问");
			throw new DynamicReqException(error);
		}
		
		context.setDynamicReqAttr(dynamicReqAttr);
		
		if(StringUtils.isNotBlank(dynamicReqAttr.getListUrl())){
			context.setListUrl(dynamicReqAttr.getListUrl());
		}else{
			context.setListUrl("modules/"+modelName+"/"+subModel+"List");
		}
		if(StringUtils.isNotBlank(dynamicReqAttr.getFormUrl())){
			context.setFormUrl(dynamicReqAttr.getFormUrl());
		}else{
			context.setFormUrl("modules/"+modelName+"/"+subModel+"Form");
		}
		
		context.setCurdService((CurdService)SpringContextHolder.getBean(dynamicReqAttr.getServiceBeanName()));
		
		return context;
	}
	
	

	public DynamicReqAttr getDynamicReqAttr() {
		return dynamicReqAttr;
	}

	public void setDynamicReqAttr(DynamicReqAttr dynamicReqAttr) {
		this.dynamicReqAttr = dynamicReqAttr;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getSubModel() {
		return subModel;
	}

	public void setSubModel(String subModel) {
		this.subModel = subModel;
	}

	public String getListUrl() {
		return listUrl;
	}

	public void setListUrl(String listUrl) {
		this.listUrl = listUrl;
	}

	public String getFormUrl() {
		return formUrl;
	}

	public void setFormUrl(String formUrl) {
		this.formUrl = formUrl;
	}

	public CurdService getCurdService() {
		return curdService;
	}

	public void setCurdService(CurdService curdService) {
		this.curdService = curdService;
	}


	
	
	
}
