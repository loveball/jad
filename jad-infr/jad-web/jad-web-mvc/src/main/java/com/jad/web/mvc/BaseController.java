/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.mvc;

import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.validation.Validator;

import org.apache.shiro.authc.AuthenticationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.context.Constants;
import com.jad.commons.context.Global;
import com.jad.commons.json.JsonMapper;
import com.jad.commons.service.CurdService;
import com.jad.commons.utils.DateUtils;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.validator.BeanValidators;
import com.jad.commons.vo.BaseVo;
import com.jad.commons.vo.PageQo;
import com.jad.commons.web.api.Result;
import com.jad.commons.web.utils.CookieUtils;
import com.jad.web.mvc.dynamic.exception.DynamicReqException;

/**
 * 控制器支持类
 * @author ThinkGem
 * @version 2013-3-23
 */
public abstract class BaseController implements ApplicationContextAware {

	/**
	 * 日志对象
	 */
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	protected ApplicationContext applicationContext;

	/**
	 * 管理基础路径
	 */
	@Value("${adminPath}")
	protected String adminPath;
	
	/**
	 * 前端基础路径
	 */
	@Value("${frontPath}")
	protected String frontPath;
	
	/**
	 * 前端URL后缀
	 */
	@Value("${urlSuffix}")
	protected String urlSuffix;
	
	/**
	 * 验证Bean实例对象
	 */
	@Autowired
	protected Validator validator;
	
	
	/**
	 * 判断验证码是否正确
	 * @param request
	 * @param validateCode
	 * @return
	 */
	public static boolean validate(HttpServletRequest request, String validateCode){
		String code = (String)request.getSession().getAttribute(Constants.VALIDATE_CODE);
		return validateCode.toUpperCase().equals(code); 
	}

	
	/**
	 * 构造方法
	 * @param request 传递 repage 参数，来记住页码
	 * @param response 用于设置 Cookie，记住页码
	 * @param defaultPageSize 默认分页大小，如果传递 -1 则为不分页，返回所有数据
	 */
	@SuppressWarnings("rawtypes")
	protected PageQo getPage(HttpServletRequest request, HttpServletResponse response){
		int confPageSize = Integer.valueOf(Global.getConfig("page.pageSize"));
		PageQo page=new PageQo();
		page.setPageSize(confPageSize);
		
		// 设置页码参数（传递repage参数，来记住页码）
		String no = request.getParameter("pageNo");
		if (StringUtils.isNumeric(no)){
			CookieUtils.setCookie(response, "pageNo", no);
			page.setPageNo(Integer.parseInt(no));
		}else if (request.getParameter("repage")!=null){
			no = CookieUtils.getCookie(request, "pageNo");
			if (StringUtils.isNumeric(no)){
				page.setPageNo(Integer.parseInt(no));
			}
		}
		// 设置页面大小参数（传递repage参数，来记住页码大小）
		String size = request.getParameter("pageSize");
		if (StringUtils.isNumeric(size)){
			CookieUtils.setCookie(response, "pageSize", size);
			page.setPageSize(Integer.parseInt(size));
		}else if (request.getParameter("repage")!=null){
			no = CookieUtils.getCookie(request, "pageSize");
			if (StringUtils.isNumeric(size)){
				page.setPageSize(Integer.parseInt(size));
			}
		}
//		else if (defaultPageSize != -2){
//			page.setPageSize(defaultPageSize);
//		}
		// 设置排序参数
		String orderBy = request.getParameter("orderBy");
		if (StringUtils.isNotBlank(orderBy)){
			page.setOrderBy(orderBy);
		}
		return page;
	}
	
	
	protected PageQo getPageQo(Integer pageNo,Integer pageSize,String orderBy) {
		if(pageNo==null){
			pageNo = 1;
		}
		if(pageSize == null){
			pageSize = Integer.valueOf(Global.getConfig("page.pageSize"));
		}
		PageQo page = new PageQo();
		page.setPageNo(pageNo);
		page.setPageSize(pageSize);
		if(StringUtils.isNotBlank(orderBy)){
			page.setOrderBy(orderBy);
		}
		return page;
		
	}

	/**
	 * 服务端参数有效性验证
	 * @param object 验证的实体对象
	 * @param groups 验证组
	 * @return 验证成功：返回true；严重失败：将错误信息添加到 message 中
	 */
	protected boolean beanValidator(Model model, Object object, Class<?>... groups) {
		try{
			BeanValidators.validateWithException(validator, object, groups);
		}catch(ConstraintViolationException ex){
			List<String> list = BeanValidators.extractPropertyAndMessageAsList(ex, ": ");
			list.add(0, "数据验证失败：");
			addMessage(model, list.toArray(new String[]{}));
			return false;
		}
		return true;
	}
	
	protected boolean beanValidator(Result<?> res, Object object, Class<?>... groups) {
		try{
			BeanValidators.validateWithException(validator, object, groups);
		}catch(ConstraintViolationException ex){
			List<String> list = BeanValidators.extractPropertyAndMessageAsList(ex, ": ");
//			list.add(0, "数据验证失败：");
//			addMessage(model, list.toArray(new String[]{}));
			StringBuffer sb = new StringBuffer();
			sb.append("数据验证失败：");
			for(int i=0;i<list.size();i++){
				if(i>0){
					sb.append(",");
				}
				sb.append(list.get(i));
			}
			res.setParamFail(sb.toString());
			return false;
		}
		return true;
	}
	
//	
	
	/**
	 * 服务端参数有效性验证
	 * @param object 验证的实体对象
	 * @param groups 验证组
	 * @return 验证成功：返回true；严重失败：将错误信息添加到 flash message 中
	 */
	protected boolean beanValidator(RedirectAttributes redirectAttributes, Object object, Class<?>... groups) {
		try{
			BeanValidators.validateWithException(validator, object, groups);
		}catch(ConstraintViolationException ex){
			List<String> list = BeanValidators.extractPropertyAndMessageAsList(ex, ": ");
			list.add(0, "数据验证失败：");
			addMessage(redirectAttributes, list.toArray(new String[]{}));
			return false;
		}
		return true;
	}
	
	/**
	 * 服务端参数有效性验证
	 * @param object 验证的实体对象
	 * @param groups 验证组，不传入此参数时，同@Valid注解验证
	 * @return 验证成功：继续执行；验证失败：抛出异常跳转400页面。
	 */
	protected void beanValidator(Object object, Class<?>... groups) {
		BeanValidators.validateWithException(validator, object, groups);
	}
	
	/**
	 * 添加Model消息
	 * @param message
	 */
	protected void addMessage(Model model, String... messages) {
		StringBuilder sb = new StringBuilder();
		for (String message : messages){
			sb.append(message).append(messages.length>1?"<br/>":"");
		}
		model.addAttribute("message", sb.toString());
	}
	
	/**
	 * 添加Flash消息
	 * @param message
	 */
	protected void addMessage(RedirectAttributes redirectAttributes, String... messages) {
		StringBuilder sb = new StringBuilder();
		for (String message : messages){
			sb.append(message).append(messages.length>1?"<br/>":"");
		}
		redirectAttributes.addFlashAttribute("message", sb.toString());
	}
	
	/**
	 * 客户端返回(默认返回json)
	 * @param response
	 * @param object
	 * @return
	 */
	protected String renderString(HttpServletResponse response, Object object) {
//		application/json
		return renderString(response, JsonMapper.toJsonString(object), "text/html");
	}
	
	/**
	 * 客户端返回json
	 * @param response
	 * @param object
	 * @return
	 */
	protected String renderJson(HttpServletResponse response, String json) {
		return renderString(response, json, "text/html");
	}
	
	/**
	 * 客户端返回字符串
	 * @param response
	 * @param string
	 * @return
	 */
	protected String renderString(HttpServletResponse response, String string, String type) {
		return renderString(response,string,type,"utf-8");
	}
	
	/**
	 * 客户端返回字符串
	 * @param response
	 * @param string
	 * @return
	 */
	protected String renderString(HttpServletResponse response, String string, String type,String character) {
		try {
	        response.setContentType(type);
	        response.setCharacterEncoding(character);
			response.getWriter().print(string);
			return null;
		} catch (IOException e) {
			return null;
		}
	}



	
	
	protected String redirectTo( String url ) {
		StringBuffer rto = new StringBuffer("redirect:");
		rto.append(url);
		return rto.toString();
	}
	
	
	
	/**
	 * 初始化数据绑定
	 * 将字段中Date类型转换为String类型
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) {
				setValue(StringUtils.isBlank(text) ? null : text.trim());//去掉空格
			}
			@Override
			public String getAsText() {
				Object value = getValue();
				if(value == null){
					return null;
				}
				if(value instanceof String){
					if(StringUtils.isBlankStr(value)){
						return null;
					}
				}
				return value.toString().trim();//去掉空格
			}
		});
		
		// Date 类型转换
		binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) {
				setValue(DateUtils.parseDate(text));
			}
//			@Override
//			public String getAsText() {
//				Object value = getValue();
//				return value != null ? DateUtils.formatDateTime((Date)value) : "";
//			}
		});
	}
	
	/**
	 * 参数绑定异常
	 */
	@ExceptionHandler({BindException.class, ConstraintViolationException.class, ValidationException.class})
    public String bindException() {  
        return "error/400";
    }
	
	
	/**
	 * 授权登录异常
	 */
	@ExceptionHandler({AuthenticationException.class})
    public String authenticationException() {  
        return "error/403";
    }
	
	
	/**
	 * 动态请求地址异常
	 */
	@ExceptionHandler({DynamicReqException.class})
    public String dyncmicUrlException() {
        return "error/404";
    }
	
	protected <VO extends BaseVo> void preInsert(VO vo,String userId) {
		
//		String userId = SecurityHelper.getCurrentUserId();
		
		if(vo.getCreateBy() == null){
			vo.setCreateBy(userId);
		}
		if(vo.getUpdateBy() == null){
			vo.setUpdateBy(userId);
		}
		
		if(vo.getCreateDate() == null ){
			vo.setCreateDate(new Date());	
		}
		if(vo.getUpdateDate() == null ){
			vo.setUpdateDate(new Date());
		}
		
	}
	
	protected <VO extends BaseVo> void preUpdate(VO vo,String userId) {
		
		
		if(vo.getUpdateBy() == null){
//			vo.setUpdateBy(SecurityHelper.getCurrentUserId());
			vo.setUpdateBy(userId);
		}
		
		if(vo.getUpdateDate() == null ){
			vo.setUpdateDate(new Date());
		}
		
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
	
	/**
	 * 默认获得service的方法
	 * @return
	 */
	public CurdService getService(){
		String beanName = this.getClass().getSimpleName();
		String suffix = "Controller";
		if(beanName.endsWith(suffix)){
			beanName = beanName.substring(0, beanName.length()-suffix.length());
		}
		beanName = beanName + "Service";
		if(applicationContext.containsBean(beanName)){
			return (CurdService)applicationContext.getBean(beanName);
		}else{
			throw new RuntimeException("找不到名称为"+beanName+"的bean");
		}
	}
	
	
}
