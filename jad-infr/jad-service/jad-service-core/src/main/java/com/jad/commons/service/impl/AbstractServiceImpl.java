package com.jad.commons.service.impl;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.reflection.ReflectionUtils;
import com.jad.commons.service.AbstractPreService;
import com.jad.commons.service.CurdService;
import com.jad.commons.service.ServiceException;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.BaseVo;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.vo.QueryObject;
import com.jad.commons.vo.ValueObject;
import com.jad.dao.JadEntityDao;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.entity.VoMetaInfo;
import com.jad.dao.utils.EntityUtils;


/**
 * 抽象service
 * 
 * 只做一些依赖检查，泛型处理等操作
 * 
 * @author hechuan
 *
 * @param <EO>
 * @param <ID>
 * @param <VO>
 */
@Transactional(readOnly = true)
public class AbstractServiceImpl <EO extends EntityObject, ID extends Serializable,VO extends BaseVo<ID>>
	extends AbstractPreService<EO,ID> implements InitializingBean,ApplicationContextAware,CurdService<VO,ID>{
	
	private static final Logger logger = LoggerFactory.getLogger(AbstractServiceImpl.class);

	protected ApplicationContext applicationContext;
	
	protected CurdHelper<EO,ID,VO> curdHelper  = new CurdHelper<EO,ID,VO>();

	@Override
	public VO findById(ID id) {
		return curdHelper.findById(id);
	}

	@Override
	public List<VO> findByIdList(List<ID> idList) {
		return curdHelper.findByIdList(idList);
	}

	@Override
	public List<VO> findByIdList(List<ID> idList, String orderBy) {
		return curdHelper.findByIdList(idList, orderBy);
	}
	
	

	@Override
	public <QO extends QueryObject> List<VO> findList(QO qo) {
		return curdHelper.findList(qo);
	}

	@Override
	public <QO extends QueryObject> List<VO> findList(QO qo, String orderBy) {
		return curdHelper.findList(qo,orderBy);
	}
	
	public <QO extends QueryObject> List<VO> findAll(){
		return curdHelper.findAll();
	}
	
	public <QO extends QueryObject> List<VO> findAll(String orderBy){
		return curdHelper.findAll(orderBy);
	}
	

	@Override
	public <QO extends QueryObject> Page<VO> findPage(PageQo page, QO qo) {
		return curdHelper.findPage(page, qo);
	}

	@Override
	public <QO extends QueryObject> Page<VO> findPage(PageQo page, QO qo,String orderBy) {
		return curdHelper.findPage(page, qo,orderBy);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void add(VO vo) {
		curdHelper.add(vo);
	}


	@Override
	@Transactional(readOnly = false)
	public void update(VO vo) {
		curdHelper.update(vo);
	}
	
	@Override
	@Transactional(readOnly = false)
	public int updateId(ID newId,ID oldId){
		return curdHelper.updateId(newId, oldId);
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(VO vo) {
		curdHelper.delete(vo);
	}
	
	/**
	 * 默认自动跟据Eo或者Vo的名称自动加载，子类可覆盖 
	 * @return
	 */
	protected JadEntityDao<EO,ID> getDao(){
		
		JadEntityDao<EO,ID> dao=autoGetDao(curdHelper.getEoMetaInfo().getMetaClass());
		
		if(dao!=null){
			return dao;
		}
		dao=autoGetDao(curdHelper.getVoMetaInfo().getMetaClass());
		
		if(dao==null){
			throw new ServiceException("service中无法自动获取dao,service:"+this.getClass().getName());
		}
		
		return dao;
	}
	
	/**
	 * 通过eo或vo的名称自动从srping中加载dao后缀的bean
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private JadEntityDao<EO,ID>autoGetDao(Class<?>clazz){
		
		String name=clazz.getSimpleName();
		
		String suffix=null;
		if(EntityObject.class.isAssignableFrom(clazz)){
			suffix="Eo";
		}else if(ValueObject.class.isAssignableFrom(clazz)){
			suffix="Vo";
		}else{
			logger.warn("无法自动获得Dao,因为%s类型末知",clazz.getName());
			return null;
		}
		
		String daoName=null;
		if(name.endsWith(suffix)){
			daoName=StringUtils.uncapitalize(name.substring(0,name.length()-suffix.length()));
		}else{
			daoName=StringUtils.uncapitalize(name);
		}
		
		daoName = daoName+"Dao";
		
		JadEntityDao<EO,ID>dao=null;
		
		try {
			if(applicationContext.containsBean(daoName)){
				
				Object bean = applicationContext.getBean(daoName);
				
				if(bean !=null && bean instanceof JadEntityDao){
					dao = (JadEntityDao<EO,ID>)bean;
				}
			}
		} catch (Exception e) {
			logger.warn(String.format("无法跟据%s:%s自动加载dao",suffix, name));
		}
		return dao;
	}
	
	public void afterPropertiesSet() throws Exception{
		curdHelper.setEoMetaInfo(getEoMetaInfo());
		curdHelper.setVoMetaInfo(getVoMetaInfo());
		curdHelper.setDao(getDao());//加载dao
	}
	
	
	@SuppressWarnings("unchecked")
	private EoMetaInfo<EO> getEoMetaInfo(){
		if(curdHelper.getEoMetaInfo()!=null){
			return curdHelper.getEoMetaInfo();
		}
		Class<EO> entityClass=ReflectionUtils.getSuperClassGenricType(getClass(), 0);
		if(EntityObject.class.isAssignableFrom(entityClass)){
			return EntityUtils.getEoInfo(entityClass);
		}
		throw new ServiceException(String.format("service类%s初始化错误,EO必须为RootEo类或子类", this.getClass().getName()));
	}
	
	
	@SuppressWarnings("unchecked")
	private VoMetaInfo<VO> getVoMetaInfo(){
		if(curdHelper.getVoMetaInfo()!=null){
			return curdHelper.getVoMetaInfo();
		}
		Class<VO>voClass=ReflectionUtils.getSuperClassGenricType(getClass(), 2);
		if(ValueObject.class.isAssignableFrom(voClass)){
			return EntityUtils.getVoInfo(voClass);
		}
		throw new ServiceException(String.format("service类%s初始化错误,VO必须为RootVo类或子类", this.getClass().getName()));
	}
	
	
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException{
		this.applicationContext=applicationContext;
	}



	

}



