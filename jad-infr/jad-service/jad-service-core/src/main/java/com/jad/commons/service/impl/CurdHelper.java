package com.jad.commons.service.impl;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.jad.commons.context.Constants;
import com.jad.commons.reflection.ReflectionUtils;
import com.jad.commons.service.AbstractPreService;
import com.jad.commons.service.ServiceException;
import com.jad.commons.util.QoUtil;
import com.jad.commons.vo.BaseVo;
import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.vo.QueryObject;
import com.jad.dao.JadEntityDao;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.entity.VoMetaInfo;
import com.jad.dao.utils.EntityUtils;


/**
 * 抽象service
 * 
 * 只做一些依赖检查，泛型处理等操作
 * 
 * @author hechuan
 *
 * @param <EO>
 * @param <ID>
 * @param <VO>
 */
public class CurdHelper <EO extends EntityObject, ID extends Serializable,VO extends BaseVo<ID>>
	extends AbstractPreService<EO,ID>  {
	
	private static final Logger logger = LoggerFactory.getLogger(CurdHelper.class);

	private EoMetaInfo<EO> eoMetaInfo;	
	
	private VoMetaInfo<VO> voMetaInfo;
	
	private JadEntityDao<EO,ID> dao;
	
	
	
	public VO findById(ID id) {
		Assert.notNull(id);
		EO eo = getDao().findById(id);
		if(eo==null){
			return null;
		}
		return toVo(eo);
	}
	
	public List<VO> findByIdList(List<ID> idList){
		Assert.notEmpty(idList);
		return findByIdList(idList,null);
	}
	
	public List<VO> findByIdList(List<ID> idList,String orderBy){
		Assert.notEmpty(idList);
		return EntityUtils.copyToVoList(getDao().findByIdList(idList,orderBy), voMetaInfo.getMetaClass());
	}

	public <QO extends QueryObject> List<VO> findList(QO qo) {
		return findList(qo,null);
	}
	
	public <QO extends QueryObject> List<VO> findAll(){
		return findList(QoUtil.newBaseNormalDataQo(),null);
	}
	public <QO extends QueryObject> List<VO> findAll(String orderBy){
		return findList(QoUtil.newBaseNormalDataQo(),orderBy);
	}
	
	
	public <QO extends QueryObject> List<VO> findList(QO qo,String orderBy) {
		Assert.notNull(qo);
		List<EO>list=getDao().findByQo(QoUtil.wrapperNormalDataQo(qo),orderBy);
		return EntityUtils.copyToVoList(list, voMetaInfo.getMetaClass());
	}
	
	public <QO extends QueryObject> Page<VO> findPage(PageQo page, QO qo,String orderBy) {
		
		Assert.notNull(page);
		Assert.notNull(qo);
		
		Page<EO>dataPage=getDao().findPageByQo(page, QoUtil.wrapperNormalDataQo(qo),orderBy);
		
		Page<VO>voPage=new Page<VO>(dataPage.getPageNo(),dataPage.getPageSize(),dataPage.getCount());
		
		voPage.setList(EntityUtils.copyToVoList(dataPage.getList(), voMetaInfo.getMetaClass()));
		
		return voPage;
	}

	public <QO extends QueryObject> Page<VO> findPage(PageQo page, QO qo) {
		
		return findPage(page,qo,null);
		
	}
	
	
	/**
	 * 增加 
	 * @param vo
	 */
	public void add(VO vo){
		Assert.notNull(vo);
		preInsert(vo,eoMetaInfo);
		getDao().add(toEo(vo));
	}
	
	
	
	/**
	 * 更新
	 * @param vo
	 */
	public void update(VO vo){
		Assert.notNull(vo);
		preUpdate(vo);
		getDao().updateById(toEo(vo), getIdFromVo(vo));
	}
	
	public int updateId(ID newId,ID oldId){
		return getDao().updateId(newId, oldId);
	}
	
	private ID getIdFromVo(VO vo){
		ID id=(ID)ReflectionUtils.getFieldValue(vo, this.getEoMetaInfo().getKeyFieldInfo().getFieldName());
		return id;
	}
	
	@SuppressWarnings("unchecked")
	public void delete(VO vo) {
		
		Assert.notNull(vo);
		
//		getDao().deleteById(getId(vo));//物理删除
		
		ID id = getIdFromVo(vo);
		
		if(id!=null){
			((BaseVo)vo).setDelFlag(Constants.DEL_FLAG_DELETE);
			this.update(vo);//逻辑删除
		}else{
			VoMetaInfo<VO>voi=EntityUtils.getVoInfo((Class<VO>)vo.getClass());
			logger.warn(String.format("无法删除%s，末知id", voi.getObjName()) );
		}
		
		
		
	}
	

	
	public EO newEo(){
		try {
			
			return eoMetaInfo.getMetaClass().newInstance();
			
		} catch (Exception e) {
			
			throw new ServiceException(e);
			
		} 
		
	}
	
	public VO newVo(){
		
		try {
			
			return voMetaInfo.getMetaClass().newInstance();
			
		} catch (Exception e) {
			
			throw new ServiceException(e);
			
		} 
		
		
	}
	
	public EO toEo(VO vo){
		if(vo==null){
			return null;
		}
		return EntityUtils.copyToEo(vo, eoMetaInfo.getMetaClass());
	}
	
	public VO toVo(EO eo){
		if(eo==null){
			return null;
		}
		return EntityUtils.copyToVo(eo, voMetaInfo.getMetaClass());
	}
	
	
	
	
	
	

	public EoMetaInfo<EO> getEoMetaInfo() {
		return eoMetaInfo;
	}

	public void setEoMetaInfo(EoMetaInfo<EO> eoMetaInfo) {
		this.eoMetaInfo = eoMetaInfo;
	}

	public VoMetaInfo<VO> getVoMetaInfo() {
		return voMetaInfo;
	}

	public void setVoMetaInfo(VoMetaInfo<VO> voMetaInfo) {
		this.voMetaInfo = voMetaInfo;
	}

	public void setDao(JadEntityDao<EO, ID> dao) {
		this.dao = dao;
	}


	public JadEntityDao<EO, ID> getDao() {
		return dao;
	}
	

}



