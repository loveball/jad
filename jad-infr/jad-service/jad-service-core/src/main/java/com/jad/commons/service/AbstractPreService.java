package com.jad.commons.service;

import java.io.Serializable;
import java.util.Date;

import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.utils.IdGen;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.BaseVo;
import com.jad.dao.entity.EoMetaInfo;

@Transactional(readOnly = true)
public abstract class AbstractPreService<EO extends EntityObject,ID extends Serializable> 
	implements BaseService{
	
	

	protected <VO extends BaseVo<ID>> void preInsert(VO vo,EoMetaInfo<EO> eoMetaInfo) {
		if(vo==null)return;
		if(vo.getId() ==null 
				|| !StringUtils.isNotBlank(vo.getId().toString())){
			
//			TODO 目前主键只支持uuid，而且主建类型只能为 String，如果要 支持 其它的类型，需要修改此处代码
			
			vo.setId((ID)IdGen.uuid());//目前主键只支持uuid
		}
		
		if(vo.getCreateDate() == null ){
			vo.setCreateDate(new Date());	
		}
		if(vo.getUpdateDate() == null ){
			vo.setUpdateDate(new Date());
		}
		
		
	}
	

	protected <VO extends BaseVo<ID>> void preUpdate(VO vo) {
		if(vo==null)return;
		if(vo.getUpdateDate() == null ){
			vo.setUpdateDate(new Date());
		}
		
	}
	
	
}
