package com.jad.commons.dao;

import java.io.Serializable;
import java.util.List;

import com.jad.commons.entity.TreeEo;
import com.jad.dao.JadEntityDao;

public interface TreeDao<EO extends TreeEo<EO, ID>, ID extends Serializable>  extends JadEntityDao<EO,ID>{
	
	/**
	 * 更新所有父节点字段
	 * @param entity
	 * @return
	 */
	public int updateParentIds(ID id,ID parentId,String parentIds);
	
//	/**
//	 * 跟据父点模糊查询实体
//	 * @param parentIds
//	 * @return
//	 */
//	public List<EO> findByParentIdsLike(String parentIds);
	
	/**
	 * 查询子节点列表
	 * @param parentId
	 * @return
	 */
	public List<EO>findSubList(ID parentId);
	
	/**
	 * 修改sort属性
	 * @param id
	 * @param sort
	 * @return
	 */
	public int updateSort(ID id,Integer sort);


}



