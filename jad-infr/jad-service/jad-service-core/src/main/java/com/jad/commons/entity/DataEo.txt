package com.jad.commons.entity;

import java.io.Serializable;

import javax.persistence.Column;

public class DataEo<ID extends Serializable> extends BaseEo<ID> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected String delFlag; 	// 删除标记（0：正常；1：删除；2：审核）
	
	public DataEo(){
		
	}
	public DataEo(ID id){
		super(id);
	}
	
	@Column(name="del_flag")
	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	
	

}
