package com.jad.commons.util;

import com.jad.commons.context.Constants;
import com.jad.commons.qo.BaseQo;
import com.jad.commons.service.ServiceException;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.QueryObject;

public class QoUtil {
	
	
	/**
	 * 实例化一下删除标记为“正常”的 qo对像
	 * @param clazz
	 * @return
	 */
	public static <QO extends QueryObject> QO newNormalDataQo(Class<QO> clazz){
		
		try {
			
			return wrapperNormalDataQo(clazz.newInstance());
			
		}catch (Exception e) {
			
			throw new ServiceException("无法实例化qo:"+clazz.getName()+"，请确保此类中存在无参构造函数");
			
		}
	}
	
	
	/**
	 * 尝试包装成删除标记为"正常"的qo
	 * @param qo
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static <QO extends QueryObject> QO wrapperNormalDataQo(QO qo){
		if(qo == null){
			return null;
		}
		if(qo instanceof BaseQo){
			if(!StringUtils.isNotBlank(((BaseQo)qo).getDelFlag())){
				((BaseQo)qo).setDelFlag(Constants.DEL_FLAG_NORMAL);
			}
			return qo;
		}
		return qo;
	}
	
	@SuppressWarnings("rawtypes")
	public static BaseQo newBaseNormalDataQo(){
		BaseQo qo = new BaseQo();
		qo.setDelFlag(Constants.DEL_FLAG_NORMAL);
		return qo;
	}
	
	

}
