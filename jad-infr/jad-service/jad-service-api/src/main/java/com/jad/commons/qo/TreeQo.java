package com.jad.commons.qo;

import java.io.Serializable;

import com.jad.commons.annotation.CURD;
import com.jad.commons.annotation.QueryConf;

public class TreeQo<ID extends Serializable> extends BaseQo<ID>{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@CURD(label="父id",queryConf=@QueryConf(property="parent.id"))
	protected String parentId; 
	
	@CURD(label="父级编号")
	protected String parentIds; // 所有父级编号
	
	@CURD(label="父级编号")
	protected String parentIdsLike; // 所有父级编号 %parentIds%
	
	@CURD(label="父级编号")
	protected String parentIdsLikeRight; // parentIds%
	
	public String getParentIds() {
		return parentIds;
	}
	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}
	public String getParentIdsLike() {
		return parentIdsLike;
	}
	public void setParentIdsLike(String parentIdsLike) {
		this.parentIdsLike = parentIdsLike;
	}
	public String getParentIdsLikeRight() {
		return parentIdsLikeRight;
	}
	public void setParentIdsLikeRight(String parentIdsLikeRight) {
		this.parentIdsLikeRight = parentIdsLikeRight;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	
	
}
