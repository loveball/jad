package com.jad.commons.qo;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.QueryObject;

public class BaseQo<ID extends Serializable>  implements QueryObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@CURD(label="id")
	protected ID id;//
	
	@CURD(label="删除标记")
	protected String delFlag; 	// 删除标记（0：正常；1：删除；2：审核）
	
	@CURD(label="创建者")
	protected String createBy;	// 创建者
	
	@CURD(label="更新者")
	protected String updateBy;	// 更新者
	
	
	public BaseQo(){
	}
	
	public BaseQo(ID id){
		this.id=id;
	}
	

	public ID getId() {
		return id;
	}

	public void setId(ID id) {
		this.id = id;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	
	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this,obj);
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

}
