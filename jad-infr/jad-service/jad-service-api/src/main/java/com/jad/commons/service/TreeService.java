package com.jad.commons.service;

import java.io.Serializable;
import java.util.List;

import com.jad.commons.vo.TreeVo;

public interface TreeService<VO extends TreeVo<VO,ID>, ID extends Serializable> extends CurdService<VO,ID> {

	
	/**
	 * 更新所有父节点字段
	 * @param entity
	 * @return
	 */
	public int updateParentIds(ID id,ID parentId,String parentIds);
	
	/**
	 * 查询子节点列表
	 * @param parentId
	 * @return
	 */
	public List<VO>findSubList(ID parentId);
	
	/**
	 * 修改sort属性
	 * @param id
	 * @param sort
	 * @return
	 */
	public int updateSort(ID id,Integer sort);


}
