package com.jad.commons.vo;

import java.io.Serializable;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jad.commons.annotation.CURD;
import com.jad.commons.annotation.InsertConf;
import com.jad.commons.annotation.UpdateConf;

public abstract class TreeVo<VO extends BaseVo<ID>,ID extends Serializable> extends BaseVo<ID> {

	private static final long serialVersionUID = 1L;
	
	@CURD(label = "所有父级编号",insertConf=@InsertConf(false),updateConf=@UpdateConf(false))
	protected String parentIds; // 所有父级编号
	
	@CURD(label = "名称")
	protected String name; 	// 名称
	
	@CURD(label = "排序")
	protected Integer sort;		// 排序

	
	/**
	 * 默认根节点
	 * @return
	 */
	@JsonIgnore
	public ID getDefRootId(){
//		TODO 目前只支持 ID类型为 String
		return (ID)"0";
	}
	
	
	public TreeVo() {
		super();
	}
	
	public TreeVo(ID id) {
		super(id);
	}
	
	/**
	 * 父对象，只能通过子类实现，父类实现mybatis无法读取
	 * @return
	 */
	@JsonBackReference
	public abstract VO getParent();

	/**
	 * 父对象，只能通过子类实现，父类实现mybatis无法读取
	 * @return
	 */
	public abstract void setParent(VO parent);

	@Length(min=1, max=2000)
	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	@Length(min=1, max=100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
	public ID getParentId() {
		ID id = null;
		if (getParent() != null ){
			id=getParent().getId();
		}
		if(id == null){
			id = getDefRootId();
		}
		return id;
	}

	
}
