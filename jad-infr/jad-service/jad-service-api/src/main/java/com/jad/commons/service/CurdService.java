package com.jad.commons.service;

import java.io.Serializable;
import java.util.List;

import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.vo.QueryObject;
import com.jad.commons.vo.ValueObject;

public interface CurdService<VO extends ValueObject,ID extends Serializable> extends BaseService{

	
	/**
	 * 获取单条数据
	 * @param id
	 * @return
	 */
	public VO findById(ID id);
	public List<VO> findByIdList(List<ID> idList);
	public List<VO> findByIdList(List<ID> idList,String orderBy);
	
	/**
	 * 查询列表数据
	 * @param entity
	 * @return
	 */
	public <QO extends QueryObject> List<VO> findList(QO qo); 
	public <QO extends QueryObject> List<VO> findList(QO qo,String orderBy); 
	public <QO extends QueryObject> List<VO> findAll(); 
	public <QO extends QueryObject> List<VO> findAll(String orderBy); 
	
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param vo
	 * @return
	 */
	public <QO extends QueryObject> Page<VO> findPage(PageQo page, QO qo);
	
	public <QO extends QueryObject> Page<VO> findPage(PageQo page, QO qo,String orderBy);
	
	
	/**
	 * 增加 
	 * @param vo
	 */
	public void add(VO vo);
	
	/**
	 * 更新
	 * @param vo
	 */
	public void update(VO vo);
	public int updateId(ID newId,ID oldId);
	
	/**
	 * 删除数据
	 * @param entity
	 */
	public void delete(VO vo);

}


