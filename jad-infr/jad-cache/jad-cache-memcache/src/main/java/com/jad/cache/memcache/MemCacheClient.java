package com.jad.cache.memcache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.util.StringUtils;

import com.jad.cache.common.AbstractCacheClient;
import com.jad.cache.common.AbstractCacheClientManager;
import com.jad.cache.common.CacheException;
import com.jad.cache.common.JadCacheManager;

/**
 * memcache实现在的CacheClient
 * @author hechuan
 *
 */
public class MemCacheClient extends AbstractCacheClient{

	private static final Logger logger = LoggerFactory.getLogger(MemCacheClient.class);
	
	private static final String MEM_CACHE_MANAGER_NAME="memcacheManager";
	
	private static final String MEM_CACHE_MANAGER_IMPL_NAME="memcacheManagerImpl";
	
	/**
	 * 默认主客户端名称
	 */
	private static final String DEF_MASTER_CACHE_NAME="masterCache";
	
	private static final String MASTER_CACHE_FIELD_NAME="masterCacheName";
	
	private static final String MULTIPLEX_MASTER_CACHE_FIELD_NAME="multiplexMasterCache";
	
	private static final String RESPONSE_STAT_INTERVAL_FIELD_NAME="responseStatInterval";
	
	/**
	 * 主缓存客户端名称
	 */
	private String masterCacheName = DEF_MASTER_CACHE_NAME;
	
	/**
	 * 是否本地缓存
	 */
	private Boolean onlyLocal = false;
	
	/**
	 * 响应统计时间间隔(单位秒,默认为0,0表示不需要做响应统计)
	 * @param seconds
	 */
	private Integer responseStatInterval = 0;
	
	/**
	 * 是否复用主客户端
	 */
	private Boolean multiplexMasterCache = true;
	
	/**
	 * 配置文件路径
	 */
	private String configFile;
	
	
	@Override
	protected void registryCacheManager(BeanDefinitionRegistry registry) {
		String memcacheManagerBeanName=this.getClientName()+"_" + MEM_CACHE_MANAGER_NAME;
		
		if (!registry.containsBeanDefinition(memcacheManagerBeanName)) {
			Class<?> cacheManagerClass = isOnlyLocal() ? LocalCacheManager.class : JadMemcachedCacheManager.class;
			RootBeanDefinition beanDefinition=new RootBeanDefinition(cacheManagerClass);
			beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
			beanDefinition.setLazyInit(false);
			beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			
			beanDefinition.getPropertyValues().add(MASTER_CACHE_FIELD_NAME, this.getMasterCacheName());
			beanDefinition.getPropertyValues().add(MULTIPLEX_MASTER_CACHE_FIELD_NAME, this.getMultiplexMasterCache().booleanValue());
			
			if(!isOnlyLocal()){
				//响应统计
				beanDefinition.getPropertyValues().add(RESPONSE_STAT_INTERVAL_FIELD_NAME, this.getResponseStatInterval());
				//配置文件
				if(StringUtils.hasText(this.getConfigFile())){
					beanDefinition.getPropertyValues().add("configFile", this.getConfigFile().trim());
				}
			}
			registry.registerBeanDefinition(memcacheManagerBeanName, beanDefinition);
		}
		
		String memcacheManagerBeanImplName=this.getClientName()+"_" + MEM_CACHE_MANAGER_IMPL_NAME;
		
		if (!registry.containsBeanDefinition(memcacheManagerBeanImplName)) {
			RootBeanDefinition beanDefinition=new RootBeanDefinition(MemCacheCacheManager.class);
			beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
			beanDefinition.setLazyInit(false);
			beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			beanDefinition.getPropertyValues().add(AbstractCacheClientManager.CACHE_MANAGER_FIELD_NAME, new RuntimeBeanReference(memcacheManagerBeanName));
			beanDefinition.getPropertyValues().add("cacheClient", this);
			registry.registerBeanDefinition(memcacheManagerBeanImplName, beanDefinition);
		}
		
		JadCacheManager manager = (JadCacheManager)applicationContext.getBean(memcacheManagerBeanImplName);
		this.setCacheManager(manager);
		registryToMasterCacheManager(manager);//注册到主管理器
		
	}
	
	public void start() {
		if(this.isStarted()){
			logger.warn("客户端已经启动，不重复启动,clientName:"+this.getClientName());
			return;
		}
		if(getCacheManager()==null){
			throw new CacheException("缓存客户端无法启动，因为没有注入缓存管理器,clientName:"+this.getClientName());
		}
		
		((MemCacheCacheManager)getCacheManager()).start();//把memcache客户端给启动
		
		super.start();
	}
	
	public void stop() {
		if(!this.isStarted()){
			logger.warn("客户端已经停止，不能重复停止,clientName:"+this.getClientName());
			return;
		}
//		this.clearAll();
		((MemCacheCacheManager)getCacheManager()).stop();
		this.started = false;
	}
	



	public boolean isOnlyLocal() {
		return onlyLocal == null ? false : onlyLocal.booleanValue();
	}
	
	public Boolean getOnlyLocal() {
		return onlyLocal;
	}


	public void setOnlyLocal(Boolean onlyLocal) {
		this.onlyLocal = onlyLocal;
	}


	public Integer getResponseStatInterval() {
		return responseStatInterval;
	}


	public void setResponseStatInterval(Integer responseStatInterval) {
		this.responseStatInterval = responseStatInterval;
	}


	public Boolean getMultiplexMasterCache() {
		return multiplexMasterCache;
	}


	public void setMultiplexMasterCache(Boolean multiplexMasterCache) {
		this.multiplexMasterCache = multiplexMasterCache;
	}


	public String getConfigFile() {
		return configFile;
	}


	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}


	public String getMasterCacheName() {
		return masterCacheName;
	}


	public void setMasterCacheName(String masterCacheName) {
		this.masterCacheName = masterCacheName;
	}

	


}
