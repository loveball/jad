package com.jad.cache.common.impl;

import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.util.StringUtils;

import com.jad.cache.common.AbstractCacheClientManager;
import com.jad.cache.common.CacheClient;
import com.jad.cache.common.CacheException;
import com.jad.cache.common.MasterCacheManager;

/**
 * 多客户端管理器
 * 
 * 可同时管理多个诸如 ehcache,memcache等不同的客户端
 * 使多客户端管理器需要指定一个默认的CacheClient(通过defCacheClientName属性指定)
 *	
 */
public class MultiClientManager extends AbstractCacheClientManager{
	
	/**
	 * 缓存客户端默认名称
	 */
	private static final String DEF_CACHE_CLIENT_NAME="defCacheClient";
	
	/**
	 * 客户列表
	 */
	private List<CacheClient>cacheClientList;
	
	
	/**
	 * 默认客户端名称
	 * 如果多客端，需要通过配置自行指定一个默认的客户端
	 * 以管理那些缺省配置的Cache，所有没有通过配置指定CacheCleint的Cache，自动生成后纳入此客户端管理
	 * 当然，需要配置autoCreateCache参数值为true，才能自动创建缺省配置的Cache
	 */
	private String defCacheClientName = DEF_CACHE_CLIENT_NAME;
	
	
	/**
	 * @param registry
	 */
	protected void registerMasterManagerIfNecessary(BeanDefinitionRegistry registry){
		
		//注册主缓存管理器
		if (!registry.containsBeanDefinition(CACHE_MANAGER_BEAN_NAME)) {
			RootBeanDefinition beanDefinition=new RootBeanDefinition(MasterCacheManager.class);
			beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
			beanDefinition.setLazyInit(false);
			beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			
			beanDefinition.getPropertyValues().add("defClientName", this.getDefCacheClientName());
			
			registry.registerBeanDefinition(CACHE_MANAGER_BEAN_NAME, beanDefinition);
		}
	}
	
	
	@Override
	protected void initClients(){
		if(this.getCacheClientList()==null || this.getCacheClientList().isEmpty()){
			return;
		}
		for(CacheClient client:this.getCacheClientList()){
			if(!StringUtils.hasText(client.getClientName())){
				throw new CacheException("缓存客户端配置出错，没有指定客户端名称");
			}
			String clientName = client.getClientName().trim();
			if(this.getCacheClients().containsKey(clientName)){
				throw new CacheException("缓存客户端配置出错，已存在名称为"+clientName+",请不要重复配置");
			}
			
			initClient(client);
			this.getCacheClients().put(clientName, client);
			
		}
	}
	
	
	protected void validateClients(){
		super.validateClients();
		if(!this.getCacheClients().containsKey(this.getDefCacheClientName())){
			throw new CacheException("没有配置名称为["+this.getDefCacheClientName()+"]的默认客户端");
		}
	}
	
	public List<CacheClient> getCacheClientList() {
		return cacheClientList;
	}


	public void setCacheClientList(List<CacheClient> cacheClientList) {
		this.cacheClientList = cacheClientList;
	}
	public String getDefCacheClientName() {
		return defCacheClientName;
	}


	public void setDefCacheClientName(String defCacheClientName) {
		this.defCacheClientName = defCacheClientName;
	}
	
	
}
