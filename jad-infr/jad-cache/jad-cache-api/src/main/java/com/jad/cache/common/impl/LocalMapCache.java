package com.jad.cache.common.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.jad.cache.common.AbstractManageredCache;
import com.jad.cache.common.CacheClient;


/**
 * 本地缓存实现
 * @author hechuan
 *
 */
public class LocalMapCache extends AbstractManageredCache {
	
	private static final Logger logger = LoggerFactory.getLogger(LocalMapCache.class); 
	
	private final String name;

	private final ConcurrentMap<String, Object> store;
	
	
	public LocalMapCache(CacheClient cacheClient,String name, int activityTime, boolean allowNullValues) {
		super(cacheClient,activityTime,allowNullValues);
		Assert.notNull(name, "name不能为null");
		this.name = name;
		this.store = new ConcurrentHashMap<String, Object>(256);
	}
	
	@Override
	public ConcurrentMap<String, Object> getNativeCache() {
		return store;
	}
	
	@Override
	public void put(Object key, Object value) {
		put(key,value,this.getActivityTime());
	}
	
	public void put(Object key, Object value,int activityTime){

		if(key==null){
			logger.warn("key为null,不能缓存对像,"+this.getLoggerInfo(null));
			return ;
		}
		
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法把数据保存到缓存中,"+this.getLoggerInfo(key.toString()));
			return ;
		}
		
		if(!super.isAllowNullValues() && value == null ){
			logger.warn("不能把null对像整到缓存中,"+getLoggerInfo(key.toString()));
			return ;
		}
		
		logger.debug("准备缓存对像,有效秒数:"+activityTime+","+this.getLoggerInfo(key.toString()));
		
		this.getNativeCache().put(key.toString(), toStoreValue(value,activityTime ));
	}

	@Override
	public ValueWrapper putIfAbsent(Object key, Object value) {
		
		if(key==null){
			logger.warn("key为null,不能缓存对像,"+this.getLoggerInfo(null));
			return null;
		}
		
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法把数据保存到缓存中,"+this.getLoggerInfo(key.toString()));
			return null ;
		}
		
		
		if(!super.isAllowNullValues() && value == null ){
			logger.warn("不能把null对像整到缓存中,"+getLoggerInfo(key.toString()));
			return null;
		}
		
		logger.debug("准备缓存对像,有效秒数:"+this.getActivityTime()+","+this.getLoggerInfo(key.toString()));
		
		Object existing = this.getNativeCache().putIfAbsent(key.toString(), toStoreValue(value,this.getActivityTime()));
		return toValueWrapper(existing,key);
	}


	@Override
	public String getName() {
		return name;
	}


	@Override
	public void evict(Object key) {
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法把数据保从缓存中删除,"+this.getLoggerInfo(key==null?null:key.toString()));
			return ;
		}
		logger.debug("准备清除对像,"+this.getLoggerInfo(key.toString()));
		this.store.remove(key);
		
	}


	@Override
	public void clear() {
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法清空数据,"+this.getLoggerInfo(null));
			return ;
		}
		logger.debug("准备清空对像,"+this.getLoggerInfo(null));
		this.store.clear();
		
	}


	@Override
	protected Object lookup(Object key) {
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法从缓存中获得数据,"+this.getLoggerInfo(key==null?null:key.toString()));
			return null;
		}
		
		return this.store.get(key);
	}
	
	@Override
	public Integer size() {
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法统计总数,"+this.getLoggerInfo(null));
			return 0;
		}
		int size = store.size();
		logger.debug("统计出对像总数:"+size+","+this.getLoggerInfo(null));
		return size;
	}
	
	@Override
	public Set<String> keySet() {
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法获得keyset,"+this.getLoggerInfo(null));
			return new HashSet();
		}
		
		return Collections.unmodifiableSet(store.keySet());
	}
	
	
	

}
