package com.jad.cache.common;

import java.io.Serializable;

/**
 * 为null的缓存对像
 * 用于缓存一个null进去以防止缓存穿透
 * 
 * @author Administrator
 *
 */
public class NullCacheData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final Object INSTANCE = new NullCacheData();

	private NullCacheData() {
	}
	

}
