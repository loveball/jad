package com.jad.cache.common;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.StringUtils;

/**
 * jad-cache缓存框架中的Cache配置对像
 * 
 * 用于在配置CacheClient的Bean中直接配置Cache
 * 从而免去在 ehcache.xml或memcache.xml中配置
 * 
 * 在CacheClient初始化时，
 * 为它配置的每个 JadCache实例最终被初始化成ManageredCache实例纳入被这个CacheClient控制的CacheManager中
 *
 */
public class JadCache implements InitializingBean,BeanNameAware{
	
	/**
	 * 缓存名称
	 * 类似于ehcache.xml中的 cache标签中的name
	 * 或类似于memcache.xml中的client标签中的name
	 */
	private String name;
	
	/**
	 */
	private CacheDefinition cacheDefinition;
	
	private int defActivityTime = -1;
	
	private boolean allowNullValues = true;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		if(cacheDefinition!=null){
			this.setDefActivityTime(cacheDefinition.getDefActivityTime());
			this.setAllowNullValues(cacheDefinition.isAllowNullValues());
		}
	}
	
	@Override
	public void setBeanName(String beanName) {
		if (!StringUtils.hasText(this.getName())) {
			this.setName(beanName);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CacheDefinition getCacheDefinition() {
		return cacheDefinition;
	}

	public void setCacheDefinition(CacheDefinition cacheDefinition) {
		this.cacheDefinition = cacheDefinition;
	}

	public int getDefActivityTime() {
		return defActivityTime;
	}

	public void setDefActivityTime(int defActivityTime) {
		this.defActivityTime = defActivityTime;
	}

	public boolean isAllowNullValues() {
		return allowNullValues;
	}

	public void setAllowNullValues(boolean allowNullValues) {
		this.allowNullValues = allowNullValues;
	}

	

}
