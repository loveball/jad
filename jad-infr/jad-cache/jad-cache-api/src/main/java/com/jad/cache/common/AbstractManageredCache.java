package com.jad.cache.common;


import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;


/**
 * ManageredCache 的抽象实现
 * 提供了把对像如果保存到缓存中或从缓存中取出时的一些封装逻辑（存取前可对对像进行包装或拆包）
 * @author hechuan
 *
 */
public abstract class AbstractManageredCache implements ManageredCache{

	
	private static final Logger logger = LoggerFactory.getLogger(AbstractManageredCache.class);
	
	/**
	 * 被管理的客户端
	 */
	private final CacheClient cacheClient;
	
	/**
	 * 存活时间
	 */
	private final int activityTime  ;
	
	/**
	 * 是否缓存null
	 */
	private final boolean allowNullValues;
	
	
	protected AbstractManageredCache(CacheClient cacheClient,int activityTime,boolean allowNullValues) {
		Assert.notNull(cacheClient, "cacheClient不能为null");
		this.cacheClient = cacheClient;
		this.activityTime = activityTime;
		this.allowNullValues = allowNullValues;
	}
	
	public final boolean isAllowNullValues() {
		return this.allowNullValues;
	}
	
	
	@Override
	public final CacheClient getCacheClient() {
		return cacheClient;
	}
	
	public final int getActivityTime(){
		return activityTime;
	}
	
	@Override
	public ValueWrapper get(Object key) {
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法从缓存中获得数据,"+this.getLoggerInfo(key==null?null:key.toString()));
			return null;
		}
		Object value = lookup(key);
		return toValueWrapper(value,key);
	}
	
	@Override
	public CacheValue getVal(String key) {
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法从缓存中获得数据,"+this.getLoggerInfo(key));
			return null;
		}
		Object value = lookup(key);
		
		if(CacheValue.isNull((CacheValue)value)){
			logger.debug("缓存没有命中,"+this.getLoggerInfo(key));
		}else if(CacheValue.isNullData((CacheValue)value)){
			logger.debug("缓存命中，但是数据为null,"+this.getLoggerInfo(key));
		}else{
			logger.debug("缓存命中,"+this.getLoggerInfo(key));
		}
		
		if(value != null && CacheValue.isExpiry((CacheValue)value)){
			logger.debug("缓存的数据已过期,"+this.getLoggerInfo( key));
			this.evict(key);//删掉过期的缓存
			return null;
		}
		
		return (CacheValue)value;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> T get(Object key, Class<T> type) {
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法从缓存中获得数据,"+this.getLoggerInfo(key==null?null:key.toString()));
			return null;
		}
		Object val = lookup(key);
		
		String keyStr = key==null?null:key.toString();
		if(CacheValue.isNull((CacheValue)val)){
			logger.debug("缓存没有命中,"+this.getLoggerInfo(keyStr));
		}else if(CacheValue.isNullData((CacheValue)val)){
			logger.debug("缓存命中，但是数据为null,"+this.getLoggerInfo(keyStr));
		}else{
			logger.debug("缓存命中,"+this.getLoggerInfo(keyStr));
		}
		
		Object value = fromStoreValue(val,key);
		if (value != null && type != null && !type.isInstance(value)) {
			throw new IllegalStateException("缓存的数据类型 不匹配，期望类型:[" + type.getName() + "],实际类型:["+value.getClass().getName()+"],数据值: " + value);
		}
		if(value != null && CacheValue.isExpiry((CacheValue)value)){
			logger.debug("缓存的数据已过期,"+this.getLoggerInfo( keyStr));
			this.evict(key);//删掉过期的缓存
			return null;
		}
		
		return (T) value;
	}
	
	/**
	 * 直接从缓存中查找，不进行包装
	 * @param key
	 * @return
	 */
	protected abstract Object lookup(Object key);
	
	
	
	/**
	 * 从缓存中取出后拆包
	 * 这里主要判断是否过期或是否为null
	 * @param storeValue
	 * @return
	 */
	protected Object fromStoreValue(Object storeValue ,Object key) {
		if(storeValue == null){
			return null;
		}
		CacheValue data = (CacheValue)storeValue;
		if(CacheValue.isExpiry(data)){
			logger.debug("缓存的数据已过期,"+this.getLoggerInfo(null));
			this.evict(key);//删掉过期的缓存
			return null;
		}else{
			if(this.allowNullValues && data.get() == NullCacheData.INSTANCE ){
				return null;
			}else{
				return data.get();
			}
		}
		
	}
	
	
	/**
	 * 缓存对像之前对对像包装
	 * 这里只要包装null值以及设定业务失效时间
	 * @param userValue
	 * @param activityTime
	 * @return
	 */
	protected Object toStoreValue(Object userValue , int activityTime) {
		Object data = userValue;
		if (this.allowNullValues && userValue == null) {
			data = NullCacheData.INSTANCE;
		}
			
		if(activityTime > 0){
			Calendar c = Calendar.getInstance();
			c.add(Calendar.SECOND, activityTime);
			return new CacheValue(data,c.getTime().getTime());
		}else{
			return new CacheValue(data);
		}
		
	}
	
	/**
	 * 返回被拆包后的对像
	 * @param storeValue
	 * @return
	 */
	protected Cache.ValueWrapper toValueWrapper(Object storeValue,Object key) {
		return (storeValue != null ? new SimpleValueWrapper(fromStoreValue(storeValue,key)) : null);
	}
	
	/**
	 * 客户端是否启动
	 * @return
	 */
	protected boolean isClientStarted(){
		return this.getCacheClient().isStarted();
	}
	
	
	protected String getLoggerInfo(String key){
		StringBuffer sb=new StringBuffer();
		sb.append("clientName:").append(cacheClient.getClientName());
		if(StringUtils.hasText(this.getName())){
			sb.append(",cacheName:").append(this.getName());
		}
		
		if(StringUtils.hasText(key)){
			sb.append(",key:").append(key);
		}
		return sb.toString();
	}
	
}










