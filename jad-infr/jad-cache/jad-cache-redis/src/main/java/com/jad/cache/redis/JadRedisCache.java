package com.jad.cache.redis;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.jad.cache.common.AbstractManageredCache;
import com.jad.cache.common.CacheClient;
import com.jad.cache.common.CacheValue;
import com.jad.cache.common.ManageredCache;

/**
 * 这个类参考了 org.springframework.data.redis.cache.RedisCache 类
 * @author hechuan
 *
 */
public class JadRedisCache extends AbstractManageredCache implements ManageredCache {

	private static final Logger logger = LoggerFactory.getLogger(JadRedisCache.class);
	
	
	private static final int PAGE_SIZE = 128;
	private long WAIT_FOR_LOCK = 300;
	
	private final String name;
	@SuppressWarnings("rawtypes") 
	private final RedisTemplate template;
	private final byte[] prefix;
	private final byte[] setName;
	private final byte[] cacheLockName;
	
	public JadRedisCache(CacheClient cacheClient,String name,
			int activityTime,boolean allowNullValues,
			byte[] prefix,RedisTemplate<? extends Object, ? extends Object> template) {
		
		super(cacheClient,activityTime,allowNullValues);
		
		this.name = name;
		this.template = template;
		this.prefix = prefix;
		
		StringRedisSerializer stringSerializer = new StringRedisSerializer();
		// name of the set holding the keys
		this.setName = stringSerializer.serialize(name + "~keys");
		this.cacheLockName = stringSerializer.serialize(name + "~lock");
	}
	
	
	public String getName() {
		return name;
	}

	public Object getNativeCache() {
		return template;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected Object lookup(final Object key) {
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法从缓存中获得数据,"+this.getLoggerInfo(key==null?null:key.toString()));
			return null;
		}
		if(key==null){
			logger.warn("不能获得key为null的缓存对像,"+this.getLoggerInfo(null));
			return null;
		}
		
		Object value = template.execute(new RedisCallback() {
			public Object doInRedis(RedisConnection connection) throws DataAccessException {
				waitForLock(connection);
				byte[] bs = connection.get(computeKey(key));
				if(bs==null) return null;
				Object value = template.getValueSerializer() != null ? template.getValueSerializer().deserialize(bs) : bs;
				return value;
			}
		}, true);
		
		
		// TODO 这里以后会移到父类中去
		if(CacheValue.isNull((CacheValue)value)){
			logger.debug("缓存没有命中,"+this.getLoggerInfo(key.toString()));
		}else if(CacheValue.isNullData((CacheValue)value)){
			logger.debug("缓存命中，但是数据为null,"+this.getLoggerInfo(key.toString()));
		}else{
			logger.debug("缓存命中,"+this.getLoggerInfo(key.toString()));
		}
		
		if(value != null && CacheValue.isExpiry((CacheValue)value)){
			logger.debug("缓存的数据已过期,"+this.getLoggerInfo( key.toString()));
			this.evict(key);//删掉过期的缓存
			return null;
		}
		
		return value;
	}
	
	
	

	
	public void put(final Object key, final Object value) {
		put(key,value,this.getActivityTime());
		
	}
	
	
	@SuppressWarnings({ "unchecked" })
	public ValueWrapper putIfAbsent(Object key, Object value) {
		if(key==null){
			logger.warn("key为null,不能缓存对像,"+this.getLoggerInfo(null));
			return null;
		}
		
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法把数据保存到缓存中,"+this.getLoggerInfo(key.toString()));
			return null;
		}
		
		if(!isAllowNullValues() && value == null ){
			logger.warn("不能把null对像整到缓存中,"+getLoggerInfo(key.toString()));
			return null;
		}
		
		logger.debug("准备缓存对像,有效秒数:"+this.getActivityTime()+","+this.getLoggerInfo(key.toString()));
		
		
		final Object storeVal = toStoreValue(value,this.getActivityTime());
		
		final byte[] keyBytes = computeKey(key);
		final byte[] valueBytes = convertToBytesIfNecessary(template.getValueSerializer(), storeVal);
		final int activityTime = this.getActivityTime();
		
		Object val = template.execute(new RedisCallback<Object>() {
			
			public Object doInRedis(RedisConnection connection) throws DataAccessException {

				waitForLock(connection);

				Object resultValue = storeVal; 
				boolean valueWasSet = connection.setNX(keyBytes, valueBytes);
				if (valueWasSet) {
					connection.zAdd(setName, 0, keyBytes);
					if (activityTime > 0) {
						connection.expire(keyBytes, (long)activityTime);
						// update the expiration of the set of keys as well
						connection.expire(setName, (long)activityTime);
					}
				} else {
					resultValue = deserializeIfNecessary(template.getValueSerializer(), connection.get(keyBytes));
				}

				return resultValue;
			}
		}, true);
		
		return toValueWrapper(val,key);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void put(Object key, Object value, final int activityTime) {
		if(key==null){
			logger.warn("key为null,不能缓存对像,"+this.getLoggerInfo(null));
			return ;
		}
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法把数据保存到缓存中,"+this.getLoggerInfo(key.toString()));
			return ;
		}
		
		if(!isAllowNullValues() && value == null ){
			logger.warn("不能把null对像整到缓存中,"+getLoggerInfo(key.toString()));
			return ;
		}
		
		final Object storeVal = toStoreValue(value,this.getActivityTime());
		
		logger.debug("准备缓存对像,有效秒数:"+this.getActivityTime()+","+this.getLoggerInfo(key.toString()));

		final byte[] keyBytes = computeKey(key);
		final byte[] valueBytes = convertToBytesIfNecessary(template.getValueSerializer(), storeVal);

		template.execute(new RedisCallback<Object>() {
			public Object doInRedis(RedisConnection connection) throws DataAccessException {

				waitForLock(connection);

				connection.multi();

				connection.set(keyBytes, valueBytes);
				connection.zAdd(setName, 0, keyBytes);

				if (activityTime > 0) {
					connection.expire(keyBytes, (long)activityTime);
					// update the expiration of the set of keys as well
					connection.expire(setName, (long)activityTime);
				}
				connection.exec();

				return null;
			}
		}, true);
			
	}

	@SuppressWarnings("unchecked")
	public void evict(Object key) {
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法把数据从缓存中删除,"+this.getLoggerInfo(key==null?null:key.toString()));
			return ;
		}
		
		final byte[] k = computeKey(key);
		logger.debug("准备清除对像,"+this.getLoggerInfo(key.toString()));
		template.execute(new RedisCallback<Object>() {
			public Object doInRedis(RedisConnection connection) throws DataAccessException {
				connection.del(k);
				// remove key from set
				connection.zRem(setName, k);
				return null;
			}
		}, true);
	}

	@SuppressWarnings("unchecked")
	public void clear() {
		
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法清空缓存,"+this.getLoggerInfo(null));
			return ;
		}
		logger.debug("准备清空对像,"+this.getLoggerInfo(null));
		
		// need to del each key individually
		template.execute(new RedisCallback<Object>() {
			public Object doInRedis(RedisConnection connection) throws DataAccessException {
				// another clear is on-going
				if (connection.exists(cacheLockName)) {
					return null;
				}

				try {
					connection.set(cacheLockName, cacheLockName);

					int offset = 0;
					boolean finished = false;

					do {
						// need to paginate the keys
						Set<byte[]> keys = connection.zRange(setName, (offset) * PAGE_SIZE, (offset + 1) * PAGE_SIZE - 1);
						finished = keys.size() < PAGE_SIZE;
						offset++;
						if (!keys.isEmpty()) {
							connection.del(keys.toArray(new byte[keys.size()][]));
						}
					} while (!finished);

					connection.del(setName);
					return null;

				} finally {
					connection.del(cacheLockName);
				}
			}
		}, true);
	}
	
	
	private boolean waitForLock(RedisConnection connection) {

		boolean retry;
		boolean foundLock = false;
		do {
			retry = false;
			if (connection.exists(cacheLockName)) {
				foundLock = true;
				try {
					Thread.sleep(WAIT_FOR_LOCK);
				} catch (InterruptedException ex) {
					Thread.currentThread().interrupt();
				}
				retry = true;
			}
		} while (retry);

		return foundLock;
	}

	
	
	private byte[] computeKey(Object key) {

		byte[] keyBytes = convertToBytesIfNecessary(template.getKeySerializer(), key);

		if (prefix == null || prefix.length == 0) {
			return keyBytes;
		}

		byte[] result = Arrays.copyOf(prefix, prefix.length + keyBytes.length);
		System.arraycopy(keyBytes, 0, result, prefix.length, keyBytes.length);

		return result;
	}
	
	private byte[] convertToBytesIfNecessary(RedisSerializer<Object> serializer, Object value) {

		if (serializer == null && value instanceof byte[]) {
			return (byte[]) value;
		}

		return serializer.serialize(value);
	}
	
	private Object deserializeIfNecessary(RedisSerializer<byte[]> serializer, byte[] value) {

		if (serializer != null) {
			return serializer.deserialize(value);
		}

		return value;
	}
	
	
	@Override
	public Integer size() {
		// TODO Auto-generated method stub
		logger.warn("redis 暂时不支持size");
		return 0;
	}

	@Override
	public Set<String> keySet() {
		// TODO Auto-generated method stub
		logger.warn("redis 暂时不支持keySet");
		return new HashSet<String>();
	}
	
	
	
	
	

}
