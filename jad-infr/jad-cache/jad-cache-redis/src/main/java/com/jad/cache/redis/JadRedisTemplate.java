package com.jad.cache.redis;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.jad.cache.common.CacheValue;

public class JadRedisTemplate extends RedisTemplate<String, CacheValue>{

	public JadRedisTemplate() {
		
		RedisSerializer<String> stringSerializer = new StringRedisSerializer();
		RedisSerializer valueSerializer = new JdkSerializationRedisSerializer();
		
		setKeySerializer(stringSerializer);
		setValueSerializer(valueSerializer);
		
		setHashKeySerializer(stringSerializer);
		setHashValueSerializer(valueSerializer);
	}

}
