package com.jad.core.sys.vo;

import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.BaseVo;

public class LogVo extends BaseVo<String>{
	
	private static final long serialVersionUID = 1L;
		
	@CURD(label="日志类型")
	private String type; 		// 日志类型（1：接入日志；2：错误日志）
	
	@CURD(label="日志标题")
	private String title;		// 日志标题
	
	@CURD(label="ip")
	private String remoteAddr; 	// 操作用户的IP地址
	
	@CURD(label="uri")
	private String requestUri; 	// 操作的URI
	
	@CURD(label="方法")
	private String method; 		// 操作的方式
	
	@CURD(label="参数")
	private String params; 		// 操作提交的数据
	
	@CURD(label="agent")
	private String userAgent;	// 操作用户代理信息
	
	@CURD(label="异常信息")
	private String exception; 	// 异常信息
	
	@CURD(label="操作用户")
	private String createByName;//操作用户
	
	@CURD(label="所在公司")
	private String createByCompany;//所在公司
	
	@CURD(label="所在部门")
	private String createByOffice;//所在部门
	
	public LogVo(){
		super();
	}
	
	public LogVo(String id){
		super(id);
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRemoteAddr() {
		return remoteAddr;
	}

	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getRequestUri() {
		return requestUri;
	}

	public void setRequestUri(String requestUri) {
		this.requestUri = requestUri;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}
	
	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getCreateByName() {
		return createByName;
	}

	public void setCreateByName(String createByName) {
		this.createByName = createByName;
	}

	public String getCreateByCompany() {
		return createByCompany;
	}

	public void setCreateByCompany(String createByCompany) {
		this.createByCompany = createByCompany;
	}

	public String getCreateByOffice() {
		return createByOffice;
	}

	public void setCreateByOffice(String createByOffice) {
		this.createByOffice = createByOffice;
	}

	


}
