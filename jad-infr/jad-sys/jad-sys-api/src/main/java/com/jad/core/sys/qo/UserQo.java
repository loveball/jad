package com.jad.core.sys.qo;

import com.jad.commons.annotation.CURD;
import com.jad.commons.annotation.QueryConf;
import com.jad.commons.qo.BaseQo;

public class UserQo extends BaseQo<String>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@CURD(label="登录名")
	private String loginName;
	
	@CURD(label="登录名")
	private String loginNameLike;// 登录名
	
	@CURD(label="姓名")
	private String nameLike;// 姓名
	
	@CURD(label="归属部门",queryConf=@QueryConf(property="office.id"))
	private String officeId;	// 归属部门
	
	@CURD(label="归属公司",queryConf=@QueryConf(property="company.id"))
	private String companyId;	// 归属公司
	

	public UserQo() {
		
	}
	
	public UserQo(String id) {
		super(id);
	}
	


	public String getOfficeId() {
		return officeId;
	}

	public void setOfficeId(String officeId) {
		this.officeId = officeId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getLoginNameLike() {
		return loginNameLike;
	}

	public void setLoginNameLike(String loginNameLike) {
		this.loginNameLike = loginNameLike;
	}

	public String getNameLike() {
		return nameLike;
	}

	public void setNameLike(String nameLike) {
		this.nameLike = nameLike;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	
	
	
}
