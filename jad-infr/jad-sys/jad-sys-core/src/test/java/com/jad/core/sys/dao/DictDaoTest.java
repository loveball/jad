package com.jad.core.sys.dao;

import java.util.List;

import org.testng.annotations.Test;

import com.jad.commons.json.JsonMapper;
import com.jad.core.sys.entity.DictEo;
import com.jad.core.sys.qo.DictQo;
import com.jad.core.sys.vo.DictVo;
import com.jad.dao.utils.EntityUtils;
import com.jad.test.dao.BaseDaoTestCase;

public class DictDaoTest extends BaseDaoTestCase{
	
	@Test
	public void findTypeList(){
		
		DictDao dictDao=getBean("dictDao");
		List<String> list = dictDao.findTypeList();
		
		System.out.println("查询结果:"+JsonMapper.toJsonString(list));
		
		List<DictEo> eoList = dictDao.findByQo(new DictQo());
		List<DictVo> voList = EntityUtils.copyToVoList(eoList, DictVo.class); 
		System.out.println(eoList.size());
		
		
	}


}
