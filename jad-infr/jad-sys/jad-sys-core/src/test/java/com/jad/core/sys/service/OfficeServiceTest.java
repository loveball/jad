package com.jad.core.sys.service;

import java.util.List;

import org.testng.annotations.Test;

import com.jad.core.sys.dao.MenuDao;
import com.jad.core.sys.entity.MenuEo;
import com.jad.core.sys.qo.MenuQo;
import com.jad.core.sys.vo.OfficeVo;
import com.jad.test.common.BaseTestCase;

public class OfficeServiceTest extends BaseTestCase{

	@Test
    public void findList() throws Exception {
		
//		super.setNeedInitContext(false);
//		String sql=SqlHelper.getSelectSql(OfficeEo.class, null, new ArrayList());
//		System.out.println(sql);
		
//		MenuDao menuDao=getBean("menuDao");
//		List<MenuEo>eoList=menuDao.findByQo(new MenuQo());
		
		OfficeService service=getBean("officeService");
		List<OfficeVo>voList=service.findList(true, null);
		System.out.println(voList.size());
	}
}
