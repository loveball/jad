package com.jad.core.sys.dao;

import com.jad.commons.dao.TreeDao;
import com.jad.core.sys.entity.AreaEo;
import com.jad.dao.annotation.JadDao;

/**
 * 区域DAO接口
 */
@JadDao
public interface AreaDao extends TreeDao<AreaEo,String> {


}
