/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.sys.dao;

import java.util.List;

import com.jad.core.sys.entity.UserEo;
import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;

/**
 * 用户DAO接口
 */
@JadDao
public interface UserDao extends JadEntityDao<UserEo,String> {
	
	/**
	 * 查询全部用户数目
	 * @return
	 */
	public long findAllCount();
	
	/**
	 * 更新用户密码
	 * @param user
	 * @return
	 */
	public int updatePasswordById(String id,String password);
	
	/**
	 * 删除用户角色关联数据
	 * @param user
	 * @return
	 */
	public int deleteUserRoleByUserId(String userId);
	
	/**
	 * 删除用户角色关联数据
	 * @param user
	 * @return
	 */
	public int deleteUserRoleByRoleId(String roleId);
	
	public int deleteUserRole(String userId,String roleId);
	
	public int insertUserRole(String userId,String roleId) ;
	
	/**
	 * 查询拥有指定角色的用户
	 * @param roleId
	 * @return
	 */
	public List<UserEo>findByRoleId(String roleId);
	

}



