package com.jad.core.sys.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.jad.commons.context.Constants;
import com.jad.commons.utils.StringUtils;
import com.jad.core.sys.dao.RoleDao;
import com.jad.core.sys.entity.RoleEo;
import com.jad.core.sys.vo.MenuVo;
import com.jad.core.sys.vo.OfficeVo;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;
import com.jad.dao.utils.SqlHelper;

@JadDao("roleDao")
public class RoleDaoImpl extends AbstractJadEntityDao<RoleEo,String>implements RoleDao{

	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<RoleEo> findByUserId(String userId){
		
		StringBuffer sb = new StringBuffer();
		
		String alias=StringUtils.uncapitalize(RoleEo.class.getSimpleName());
		
		sb.append(SqlHelper.getSelectSql(RoleEo.class, alias, new ArrayList<String>()));
		sb.append(" join sys_user_role ur on ur.role_id="+alias+".id where ur.user_id=? ");
		
//		sb.append(" where ").append(alias).append(".id in (select role_id from sys_user_role ur where ur.user_id=? )");
		
		sb.append(" and "+alias+".del_flag=? ");
		
		List params = new ArrayList();
		params.add(userId);
		params.add(Constants.DEL_FLAG_NORMAL);
		
		return super.findBySql(sb.toString(), params);
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public int deleteRoleMenu(String roleId) {
		
		List params= new ArrayList();
		params.add(roleId);
		
		String sql="DELETE FROM sys_role_menu WHERE role_id = ?";
		
		return super.executeSql(sql,params);
		
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public int insertRoleMenu(String roleId,List<MenuVo> menuList) {
		if(CollectionUtils.isEmpty(menuList)){
			return 0;
		}
		int v=0;
		String sql="INSERT INTO sys_role_menu(role_id, menu_id) values(?,?) ";
		List params=null;
		for(MenuVo menuVo:menuList){
			params= new ArrayList();
			params.add(roleId);
			params.add(menuVo.getId());
			v=v+super.executeSql(sql,params);
		}
		
		return v;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public int deleteRoleOffice(String roleId) {
		String sql="DELETE FROM sys_role_office WHERE role_id = ?";
		List params=new ArrayList();
		params.add(roleId);
		return super.executeSql(sql,params);
	}

	@Override
	public int insertRoleOffice(String roleId,List<OfficeVo> officeList) {
		
		if(CollectionUtils.isEmpty(officeList)){
			return 0;
		}
		
		int v=0;
			
		String sql="INSERT INTO sys_role_office(role_id, office_id) values(?,?) ";
		List params=null;
		for(OfficeVo officeVo:officeList){
			params=new ArrayList();
			params.add(roleId);
			params.add(officeVo.getId());
			v=v+super.executeSql(sql,params);
		}
		
		return v;
	}


}
