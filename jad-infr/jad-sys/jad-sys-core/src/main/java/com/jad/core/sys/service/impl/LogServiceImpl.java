/**
 * Copyright &copy; 2012-2013 <a href="httparamMap://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.sys.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.context.RequestLogContext;
import com.jad.commons.json.JsonMapper;
import com.jad.commons.service.impl.AbstractServiceImpl;
import com.jad.commons.utils.Exceptions;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.core.sys.constant.LogConstant;
import com.jad.core.sys.dao.UserDao;
import com.jad.core.sys.entity.LogEo;
import com.jad.core.sys.entity.UserEo;
import com.jad.core.sys.qo.LogQo;
import com.jad.core.sys.service.LogService;
import com.jad.core.sys.vo.LogVo;


/**
 * 日志Service
 */
@Service("logService")
@Transactional(readOnly = true)
public class LogServiceImpl extends AbstractServiceImpl<LogEo,String, LogVo> implements LogService {

	private static final Logger logger = LoggerFactory.getLogger(LogServiceImpl.class);
	
	@Autowired
	public UserDao userDao;
	
	public Page<LogVo> findLogPage(PageQo page, LogQo log) {
		
		// 设置默认时间范围，默认当前月
		if (log.getCreateDateBegin() == null){
//			log.setCreateDateBegin(DateUtils.setDays(DateUtils.parseDate(DateUtils.getDate()), 1));
		}
		if (log.getCreateDateEnd() == null){
//			log.setCreateDateEnd(DateUtils.addMonths(log.getCreateDateBegin(), 1));
		}
		log.setCreateDateBegin(null);
		log.setCreateDateEnd(null);
		
		Page<LogVo> dataPage= super.findPage(page, log);
		if(Page.isEmpty(dataPage)){
			return dataPage;
		}
		Map<String,UserEo>userMap=new HashMap<String,UserEo>();
		for(LogVo vo:dataPage.getList()){
			String createBy = vo.getCreateBy();
			if(createBy==null){
				continue;
			}
			if(userMap.containsKey(createBy)){
				UserEo user = userMap.get(createBy);
				setCreateBy(vo,user);
				continue;
			}else{
				UserEo user = userDao.findById(createBy); 
				if(user==null){
					logger.warn("找不到id为"+createBy+"的用户信息");
					continue;
				}
				userMap.put(createBy, user);
				setCreateBy(vo,user);
			}
			
		}
		return dataPage;
	}
	
	private void setCreateBy(LogVo vo,UserEo user){
		vo.setCreateByName(user.getName());
		if(user.getCompany()!=null){
			vo.setCreateByCompany(user.getCompany().getName());
		}
		if(user.getOffice()!=null){
			vo.setCreateByOffice(user.getOffice().getName());
		}
	}
	
	@Transactional(readOnly = false)
	public void saveLog(RequestLogContext request, String title,boolean isAsync){
		saveLog(request,null,title,isAsync); 
	}
	
	@Transactional(readOnly = false)
	public void saveLog(RequestLogContext request , Exception ex, String title,boolean isAsync){
//		UserVo user = UserHelper.getUser();
//		if (user != null && user.getId() != null){
			LogVo log = new LogVo();
			log.setTitle(title);
			log.setType(ex == null ? LogConstant.TYPE_ACCESS : LogConstant.TYPE_EXCEPTION);
			
			log.setRemoteAddr(request.getRemoteAddr());
			log.setUserAgent(request.getUserAgent());
			log.setRequestUri(request.getRequestUri());
			
			log.setParams(JsonMapper.toJsonString(request.getParams()));
			
			log.setMethod(request.getMethod());
			
			log.setException(Exceptions.getStackTraceAsString(ex));
			
			if(isAsync){
				new SaveLogThread(log).start();
			}else{
				add(log);
			}
			
			
//		}
		
	}
	@Transactional(readOnly = false)
	public void saveLog(RequestLogContext request , Exception ex, String title){
		saveLog(request,ex,title,false);
	}
	
	/**
	 * 异步保存日志
	 * @param request
	 * @param ex
	 * @param title
	 */
	@Transactional(readOnly = false)
	public void saveLogWidthAsynchronous(RequestLogContext request , Exception ex, String title){
		saveLog(request,ex,title,true);
	}
	
	
	/**
	 * 保存日志线程
	 */
	public class SaveLogThread extends Thread{
		private LogVo log;
		public SaveLogThread(LogVo log){
			this.log = log;
		}
		@Override
		public void run() {
			add(log);
		}
	}
}
