package com.jad.core.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.service.impl.JadTreeServiceImpl;
import com.jad.commons.utils.Validate;
import com.jad.core.sys.dao.MenuDao;
import com.jad.core.sys.entity.MenuEo;
import com.jad.core.sys.service.MenuService;
import com.jad.core.sys.vo.MenuVo;
import com.jad.core.sys.vo.RoleVo;
import com.jad.dao.utils.EntityUtils;

@Service("menuService")
@Transactional(readOnly = true)
public class MenuServiceImpl extends JadTreeServiceImpl<MenuEo,String, MenuVo> implements MenuService {

	
	@Autowired
	private MenuDao menuDao;
	
	public List<MenuVo> findByUserId(String userId){
		Validate.notBlank(userId);
		return EntityUtils.copyToVoList(menuDao.findByUserId(userId), MenuVo.class);
	}
	
	/**
	 * 跟据角色id查找有权限的菜单
	 * @param roleId
	 * @return
	 */
	public List<MenuVo> findByRoleId(String roleId){
		Validate.notBlank(roleId);
		return EntityUtils.copyToVoList(menuDao.findByRoleId(roleId), MenuVo.class);
	}

}






