package com.jad.core.sys.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;

import com.jad.commons.context.Constants;
import com.jad.commons.utils.StringUtils;
import com.jad.core.sys.dao.UserDao;
import com.jad.core.sys.entity.UserEo;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;
import com.jad.dao.utils.SqlHelper;

@JadDao("userDao")
public class UserDaoImpl extends AbstractJadEntityDao<UserEo,String>implements UserDao{

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public long findAllCount() {
		String sql="SELECT COUNT(1) C FROM sys_user a WHERE a.del_flag = ? ";
		List params=new ArrayList();
		params.add(Constants.DEL_FLAG_NORMAL);
		
		List<Map>mapList=super.findBySql(sql, params, Map.class);
		if(mapList!=null && !mapList.isEmpty()){
			return MapUtils.getLongValue(mapList.get(0), "C");
		}
		
		return 0;
	}
	
	
	/**
	 * 查询拥有指定角色的用户
	 * @param roleId
	 * @return
	 */
	public List<UserEo>findByRoleId(String roleId){
		StringBuffer sb = new StringBuffer();
		
		String alias=StringUtils.uncapitalize(UserEo.class.getSimpleName());
		
		sb.append(SqlHelper.getSelectSql(UserEo.class, alias, new ArrayList<String>()));
		
		sb.append(" join sys_user_role ur on ur.user_id="+alias+".id where ur.role_id=? ");
		
		sb.append(" and "+alias+".del_flag=? ");
		
		List params = new ArrayList();
		params.add(roleId);
		params.add(Constants.DEL_FLAG_NORMAL);
		
		return super.findBySql(sb.toString(), params);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public int updatePasswordById(String id,String password) {
		
		List params=new ArrayList();
		params.add(password);
		params.add(id);
		
		String sql="UPDATE sys_user SET password = ?  WHERE id = ? ";
	
		return super.executeSql(sql,params);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public int deleteUserRoleByUserId(String userId) {
		
		List params=new ArrayList();
		params.add(userId);
		
		String sql="DELETE FROM sys_user_role WHERE user_id = ? ";
		
		return super.executeSql(sql,params);
	}
	
	/**
	 * 删除用户角色关联数据
	 * @param user
	 * @return
	 */
	public int deleteUserRoleByRoleId(String roleId){
		List params=new ArrayList();
		params.add(roleId);
		
		String sql="DELETE FROM sys_user_role WHERE role_id = ? ";
		
		return super.executeSql(sql,params);
	}
	
	
	public int deleteUserRole(String userId,String roleId) {
		
		List params=new ArrayList();
		params.add(userId);
		params.add(roleId);
		String sql="DELETE FROM sys_user_role WHERE user_id = ? and role_id =?";
		
		return super.executeSql(sql,params);
		
	}
	
	public int insertUserRole(String userId,String roleId) {
		
		List params = new ArrayList();
		String sql="INSERT INTO sys_user_role(user_id, role_id) values(?,?) ";
		params.add(userId);
		params.add(roleId);
		
		return super.executeSql(sql,params);
		
	}


}
