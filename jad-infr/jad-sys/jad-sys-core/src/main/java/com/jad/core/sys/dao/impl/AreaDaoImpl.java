package com.jad.core.sys.dao.impl;

import com.jad.commons.dao.AbstractTreeDao;
import com.jad.core.sys.dao.AreaDao;
import com.jad.core.sys.entity.AreaEo;
import com.jad.dao.annotation.JadDao;


@JadDao("areaDao")
public class AreaDaoImpl extends AbstractTreeDao<AreaEo,String>implements AreaDao{
}
