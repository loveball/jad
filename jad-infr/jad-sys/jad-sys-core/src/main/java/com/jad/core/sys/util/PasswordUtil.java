package com.jad.core.sys.util;

import com.jad.commons.context.Constants;
import com.jad.commons.encrypt.Digests;
import com.jad.commons.encrypt.Encodes;

public class PasswordUtil {
	
//	private static Logger logger=LoggerFactory.getLogger(PasswordUtil.class);
//	private static final String CHARSET_NAME="UTF-8";
	
	/**
	 * 生成安全的密码，生成随机的16位salt并经过1024次 sha-1 hash
	 */
	public static String entryptPassword(String plainPassword) {
		String plain = Encodes.unescapeHtml(plainPassword);
		byte[] salt = Digests.generateSalt(Constants.SALT_SIZE);
		byte[] hashPassword = Digests.sha1(plain.getBytes(), salt, Constants.HASH_INTERATIONS);
		return Encodes.encodeHex(salt)+Encodes.encodeHex(hashPassword);
	}
	
	/**
	 * 验证密码
	 * @param plainPassword 明文密码
	 * @param password 密文密码
	 * @return 验证成功返回true
	 */
	public static boolean validatePassword(String plainPassword, String password) {
		String plain = Encodes.unescapeHtml(plainPassword);
		byte[] salt = Encodes.decodeHex(password.substring(0,Constants.SALT_SIZE * 2));
		byte[] hashPassword = Digests.sha1(plain.getBytes(), salt, Constants.HASH_INTERATIONS);
		return password.equals(Encodes.encodeHex(salt)+Encodes.encodeHex(hashPassword));
	}
	

}
