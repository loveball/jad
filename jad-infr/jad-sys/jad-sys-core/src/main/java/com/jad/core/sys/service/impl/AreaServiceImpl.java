/**
 */
package com.jad.core.sys.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.service.impl.JadTreeServiceImpl;
import com.jad.core.sys.entity.AreaEo;
import com.jad.core.sys.service.AreaService;
import com.jad.core.sys.vo.AreaVo;

/**
 * 区域Service
 * @author hechuan
 * @version 2017-01-30
 */
@Service("areaService")
@Transactional(readOnly = true)
public class AreaServiceImpl extends JadTreeServiceImpl< AreaEo, String,AreaVo >  implements AreaService{
	
}
