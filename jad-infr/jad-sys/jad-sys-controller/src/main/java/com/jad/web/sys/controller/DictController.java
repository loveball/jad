/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.sys.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.annotation.ApiCurdType;
import com.jad.commons.context.Global;
import com.jad.commons.enums.CurdType;
import com.jad.commons.security.shiro.SecurityHelper;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.web.api.Result;
import com.jad.core.sys.qo.DictQo;
import com.jad.core.sys.service.DictService;
import com.jad.core.sys.vo.DictVo;
import com.jad.web.mvc.BaseController;

/**
 * 字典Controller
 * @author ThinkGem
 * @version 2014-05-16
 */
@Controller
@RequestMapping(value = "${adminPath}/sys/dict")
@Api(description = "字典管理")
public class DictController extends BaseController {

	@Autowired
	private DictService dictService;
	
//	@ModelAttribute
//	public DictVo get(@RequestParam(required=false) String id) {
////		if (StringUtils.isNotBlank(id)){
////			return dictService.findById(id);
////		}else{
////			return new DictVo();
////		}
//		return new DictVo();
//	}
	
	/**
	 * 查看单条记录
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:dict:view")
	@RequestMapping(value = {"find"})
	@ResponseBody
	@ApiOperation(value = "查看字典信息",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_ONE)
	public Result<DictVo> find(@RequestParam(required=true) String id){
		return Result.newSuccResult(dictService.findById(id));
	}
	
	/**
	 * 查询列表
	 * @param userQo
	 * @param pageQo
	 * @return
	 */
	@RequiresPermissions("sys:dict:view")
	@ResponseBody
	@RequestMapping(value = {"findPage"})
	@ApiOperation(value = "查询字典列表",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_LIST)
	public Result<Page<DictVo>> findPage(DictQo dictQo,PageQo pageQo){
		Page<DictVo> page = dictService.findPage(pageQo, dictQo);
		return Result.newSuccResult(page);
	}
	
	/**
	 * 修改
	 * @param dictVo
	 * @return
	 */
	@RequiresPermissions("sys:dict:edit")
	@RequestMapping(value = "update")
	@ResponseBody
	@ApiOperation(value = "修改字典信息",httpMethod = "POST")
	@ApiCurdType(CurdType.UPDATE)
	public Result<?> update(DictVo dictVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, dictVo)){
			return result;
		}
		if(StringUtils.isBlank(dictVo.getId())){
			result.setParamFail("修改失败,末知的id");
			return result;
		}
		preUpdate(dictVo,SecurityHelper.getCurrentUserId());
		dictService.update(dictVo);
		return result;
	}
	
	/**
	 * 新增
	 * @param userVo
	 * @return
	 */
	@RequiresPermissions("sys:dict:edit")
	@RequestMapping(value = "add")
	@ResponseBody
	@ApiOperation(value = "新增会员信息",httpMethod = "POST")
	@ApiCurdType(CurdType.ADD)
	public Result<?> add(DictVo dictVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, dictVo)){
			return result;
		}
		preInsert(dictVo,SecurityHelper.getCurrentUserId());
		dictService.add(dictVo);
		return result;
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:dict:edit")
	@RequestMapping(value = "del")
	@ResponseBody
	@ApiOperation(value = "删除字典信息",httpMethod = "POST")
	@ApiCurdType(CurdType.DELETE)
	public Result<?> delete(@RequestParam(required=true)String id){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		DictVo delVo = new DictVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		dictService.delete(delVo);
		return result;
	}
	
	/**
	 * 查询类型列表
	 * @return
	 */
	@RequiresPermissions("sys:dict:view")
	@ResponseBody
	@RequestMapping(value = {"findTypeList"})
	@ApiOperation(value = "查询字典类型列表",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_LIST)
	public Result<List<String>> findTypeList(){
		List<String> typeList = dictService.findTypeList();
		return Result.newSuccResult(typeList);
	}
	
	
	@RequiresPermissions("sys:dict:view")
	@RequestMapping(value = {"list", ""})
	public String list(DictQo dictQo, HttpServletRequest request, HttpServletResponse response, Model model) {
		List<String> typeList = dictService.findTypeList();
		model.addAttribute("typeList", typeList);
		
//		DictQo qo = new DictQo();
//		if(StringUtils.isNotBlank(dictVo.getType())){
//			qo.setType(dictVo.getType());
//		}
//		if(StringUtils.isNotBlank(dictVo.getDescription())){
//			qo.setDescription(dictVo.getDescription());
//		}
//		Page<DictVo> page = dictService.findPage(getPage(request, response), qo); 
		
		Page<DictVo> page = dictService.findPage(getPage(request, response), dictQo); 
		
        model.addAttribute("page", page);
        
		return "modules/sys/dictList";
	}

	@RequiresPermissions("sys:dict:view")
	@RequestMapping(value = "form")
	public String form(DictQo dictQo, Model model) {
//		if(dictVo!=null && StringUtils.isNotBlank(dictVo.getId())){
//			dictVo = dictService.findById(dictVo.getId());
//		}
		DictVo dictVo = new DictVo();
		if(StringUtils.isNotBlank(dictQo.getId())){
			dictVo = dictService.findById(dictQo.getId());
		}
		model.addAttribute("dictVo", dictVo);
		
		return "modules/sys/dictForm";
	}
	
	/**
	 * 添加键值
	 * @param dictVo
	 * @param model
	 * @return
	 */
	@RequiresPermissions("sys:dict:view")
	@RequestMapping(value = "addVal")
	public String addVal(String dictId, Model model) {
		DictVo dbVo = dictService.findById(dictId);
		DictVo addVo = new DictVo();
		addVo.setDescription(dbVo.getDescription());
		addVo.setType(dbVo.getType());
		addVo.setSort(dbVo.getSort()==null?10:(dbVo.getSort()+10));
		model.addAttribute("dictVo", addVo);
		return "modules/sys/dictForm";
	}

	@RequiresPermissions("sys:dict:edit")
	@RequestMapping(value = "save")//@Valid 
	public String save(DictVo dictVo, Model model, RedirectAttributes redirectAttributes) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/sys/dict/?repage&type="+dictVo.getType();
		}
		if (!beanValidator(model, dictVo)){
//			return form(dictVo, model);
			model.addAttribute("dictVo", dictVo);
			return "modules/sys/dictForm";
		}
		if(StringUtils.isNotBlank(dictVo.getId())){
			dictVo.setUpdateBy(SecurityHelper.getCurrentUserId());
			preUpdate(dictVo,SecurityHelper.getCurrentUserId());
			dictService.update(dictVo);
		}else{
			preInsert(dictVo,SecurityHelper.getCurrentUserId());
			dictService.add(dictVo);	
		}
		
		
		addMessage(redirectAttributes, "保存字典'" + dictVo.getLabel() + "'成功");
		System.out.println("dictVo.type:"+dictVo.getType());
		return "redirect:" + adminPath + "/sys/dict/?repage&type="+dictVo.getType();
	}
	
	@RequiresPermissions("sys:dict:edit")
	@RequestMapping(value = "delete")
	public String delete(@RequestParam(required=true)String id, RedirectAttributes redirectAttributes) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/sys/dict/?repage";
		}
		
		DictVo oldVo = dictService.findById(id);
		if(oldVo==null){
			addMessage(redirectAttributes, "删除失败,可能已经被删除，请不要重复删除");
			return "redirect:" + adminPath + "/sys/dict/?repage";
		}
		
		DictVo delVo = new DictVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		
		dictService.delete(delVo);
		
		addMessage(redirectAttributes, "删除字典成功");
		return "redirect:" + adminPath + "/sys/dict/?repage&type="+oldVo.getType();
	}
	
	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "treeData")
	public List<Map<String, Object>> treeData(@RequestParam(required=false) String type, HttpServletResponse response) {
		List<Map<String, Object>> mapList =  new ArrayList();
		DictQo dict = new DictQo();
		dict.setType(type);
		List<DictVo> list = dictService.findList(dict);
		for (int i=0; i<list.size(); i++){
			DictVo e = list.get(i);
			Map<String, Object> map =  new HashMap();
			map.put("id", e.getId());
			map.put("pId", e.getParentId());
			map.put("name", StringUtils.replace(e.getLabel(), " ", ""));
			mapList.add(map);
		}
		return mapList;
	}
	
	@ResponseBody
	@RequestMapping(value = "listData")
	public List<DictVo> listData(@RequestParam(required=false) String type) {
		DictQo dict = new DictQo();
		dict.setType(type);
		return dictService.findList(dict);
	}

}
