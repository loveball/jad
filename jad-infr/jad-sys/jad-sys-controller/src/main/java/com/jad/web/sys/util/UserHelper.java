/**
 */
package com.jad.web.sys.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jad.commons.context.SpringContextHolder;
import com.jad.commons.security.shiro.Principal;
import com.jad.commons.security.shiro.SecurityHelper;
import com.jad.core.sys.qo.AreaQo;
import com.jad.core.sys.qo.MenuQo;
import com.jad.core.sys.qo.OfficeQo;
import com.jad.core.sys.qo.RoleQo;
import com.jad.core.sys.service.AreaService;
import com.jad.core.sys.service.MenuService;
import com.jad.core.sys.service.OfficeService;
import com.jad.core.sys.service.RoleService;
import com.jad.core.sys.service.UserService;
import com.jad.core.sys.vo.AreaVo;
import com.jad.core.sys.vo.MenuVo;
import com.jad.core.sys.vo.OfficeVo;
import com.jad.core.sys.vo.RoleVo;
import com.jad.core.sys.vo.UserVo;

/**
 * 用户工具类
 */
public class UserHelper {
	
	private static UserService userService ;
	private static RoleService roleService ;
	private static MenuService menuService ;
	private static AreaService areaService ;
	private static OfficeService officeService ;
	
	public static void init(){
		userService = SpringContextHolder.getBean("userService");
		roleService = SpringContextHolder.getBean("roleService");
		menuService = SpringContextHolder.getBean("menuService");
		areaService = SpringContextHolder.getBean("areaService");
		officeService = SpringContextHolder.getBean("officeService");
		
	}
	
//	public static final String USER_CACHE = "userCache";
//	public static final String USER_CACHE_ID_ = "id_";
//	public static final String USER_CACHE_LOGIN_NAME_ = "ln";
//	public static final String USER_CACHE_LIST_BY_OFFICE_ID_ = "oid_";
//
//	public static final String CACHE_ROLE_LIST = "roleList";
//	public static final String CACHE_MENU_LIST = "menuList";
//	public static final String CACHE_AREA_LIST = "areaList";
//	public static final String CACHE_OFFICE_LIST = "officeList";
//	public static final String CACHE_OFFICE_ALL_LIST = "officeAllList";
	
	/**
	 * 根据ID获取用户
	 * @param id
	 * @return 取不到返回null
	 */
	public static UserVo get(String id){
		
		UserVo user = userService.findById(id);
		
		user.setRoleList(getRoleList(user));
		
		return user;
	}
	
	
	public static Principal getPrincipal(){
		return SecurityHelper.getPrincipal();
	}
	
	/**
	 * 获取当前用户
	 * @return 取不到返回 new User()
	 */
	public static UserVo getUser(){
		Principal principal = getPrincipal();
		if (principal!=null){
			UserVo user = get(principal.getId());
			if (user != null){
				return user;
			}
			

		}
//		TODO 待完成		
		return new UserVo();
		
	}

	private static List<RoleVo> getRoleList(UserVo user){

		if( user == null ){
			return new ArrayList<RoleVo>();
		}
		
		if (user.isAdmin()){
			
			return roleService.findList(new RoleQo());
			
		}
		
		if(StringUtils.isBlank(user.getId())){
			
			return new ArrayList<RoleVo>();
			
		}
		
		return roleService.findByUserId(user.getId());
	}
	
	/**
	 * 获取当前用户角色列表
	 * @return
	 */
	public static List<RoleVo> getRoleList(){
		
		UserVo user = getUser(); //当前用户
		
		return getRoleList(user);
			
	}
	
	/**
	 * 获取当前用户授权菜单
	 * @return
	 */
	public static List<MenuVo> getMenuList(){
		
		UserVo user = getUser();
		
		if( user == null ){
			
			return new ArrayList<MenuVo>();
			
		}
		if (user.isAdmin()){
			
			return menuService.findList(new MenuQo()); 
			
		}
		
		if(StringUtils.isBlank(user.getId())){
			
			return new ArrayList<MenuVo>();
			
		}
		
		return menuService.findByUserId(user.getId());
		
		
	}
	
	/**
	 * 获取当前用户授权的区域
	 * @return
	 */
	public static List<AreaVo> getAreaList(){
		
		
//		TODO 待完成
		return areaService.findList(new AreaQo());
		
//		@SuppressWarnings("unchecked")
//		List<AreaVo> areaList = (List<AreaVo>)getCache(CACHE_AREA_LIST);
//		List<AreaVo> areaList = null;
//		if (areaList == null){
////			areaList = areaDao.findAllList(new AreaEo());
//			areaList=EntityUtils.copyToVoList(areaDao.findByQo(new AreaQo()), AreaVo.class);
//					
////			putCache(CACHE_AREA_LIST, areaList);
//		}
//		return areaList;
	}
	
	/**
	 * 获取当前用户有权限访问的部门
	 * @return
	 */
	public static List<OfficeVo> getOfficeList(){
		
//		TODO 待完成
		return officeService.findList(new OfficeQo());
		
//		@SuppressWarnings("unchecked")
////		List<OfficeVo> officeList = (List<OfficeVo>)getCache(CACHE_OFFICE_LIST);
//		List<OfficeVo> officeList = null;
//		if (officeList == null){
//			UserVo user = getUser();
//			if (user.isAdmin()){
////				officeList = officeDao.findAllList(new OfficeVo());
//				officeList = EntityUtils.copyToVoList(officeDao.findByQo(new OfficeQo()), OfficeVo.class);
//			}else{
//				OfficeVo office = new OfficeVo();
//				// TODO Auto-generated method stub
////				office.getSqlMap().put("dsf", DataFilters.dataScopeFilter(UserUtils.getUser(), "a", ""));
////				officeList = officeDao.findList(office);
//				officeList = EntityUtils.copyToVoList(officeDao.findByQo(new OfficeQo()), OfficeVo.class);
//			}
////			putCache(CACHE_OFFICE_LIST, officeList);
//		}
//		return officeList;
		
	}

	
	
	
	

	
}
