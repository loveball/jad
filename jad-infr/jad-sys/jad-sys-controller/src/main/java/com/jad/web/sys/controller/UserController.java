/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.sys.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.annotation.ApiCurdType;
import com.jad.commons.context.Constants;
import com.jad.commons.context.Global;
import com.jad.commons.enums.CurdType;
import com.jad.commons.security.shiro.SecurityHelper;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.web.api.RespCode;
import com.jad.commons.web.api.Result;
import com.jad.core.sys.qo.RoleQo;
import com.jad.core.sys.qo.UserQo;
import com.jad.core.sys.service.RoleService;
import com.jad.core.sys.service.UserService;
import com.jad.core.sys.util.UserUtil;
import com.jad.core.sys.vo.OfficeVo;
import com.jad.core.sys.vo.RoleVo;
import com.jad.core.sys.vo.UserVo;
import com.jad.web.mvc.BaseController;

/**
 * 用户Controller
 * @author ThinkGem
 * @version 2013-8-29
 */
@Controller
@RequestMapping(value = "${adminPath}/sys/user")
@Api(description = "用户api")
public class UserController extends BaseController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;
	
//	@ModelAttribute
//	public UserVo get(@RequestParam(required=false) String id) {
////		if (StringUtils.isNotBlank(id)){
////			return userService.findById(id);
////		}else{
////			return new UserVo();
////		}
//		return new UserVo();
//	}
	
	
	
	/**
	 * 获得会员信息
	 * @param id 会员id,如果id为空，则获得当前会员信息
	 * @return
	 */
	@RequiresPermissions("sys:user:view")
	@RequestMapping(value = {"find"})
	@ResponseBody
	@ApiOperation(value = "查看会员信息",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_ONE)
	public Result<UserVo> find(@RequestParam(required=false) String id){
		if(StringUtils.isBlankStr(id)){
			id=SecurityHelper.getCurrentUserId();
		}
		if(StringUtils.isBlankStr(id)){
			return Result.newResult(RespCode.NO_LOGIN,null);
		}
		return Result.newSuccResult(userService.findById(id));
	}

	/**
	 * 查询会员列表
	 * @param userQo
	 * @param pageQo
	 * @return
	 */
	@RequiresPermissions("sys:user:view")
	@ResponseBody
	@RequestMapping(value = {"findPage"})
	@ApiOperation(value = "查询会员列表",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_LIST)
	public Result<Page<UserVo>> findPage(UserQo userQo,PageQo pageQo){
		Page<UserVo> page = userService.findPage(pageQo, userQo);
		return Result.newSuccResult(page);
	}
	
	@RequiresPermissions("sys:user:view")
	@RequestMapping(value = {"index"})
	public String index(Model model) {
		return "modules/sys/userIndex";
	}
	@RequiresPermissions("sys:user:view")
	@RequestMapping(value = {"list", ""})
	public String list(UserQo userQo, HttpServletRequest request, HttpServletResponse response, Model model) {
//		UserQo userQo = getQo(userVo);
		Page<UserVo> page = userService.findPage(getPage(request, response), userQo);
        model.addAttribute("page", page);
		return "modules/sys/userList";
	}
	
//	private UserQo getQo(UserVo userVo){
//		Assert.notNull(userVo);
//		UserQo userQo = new UserQo(); 
//		
////		TODO 收集查询参数
//		if(StringUtils.isNotBlank(userVo.getId())){
//			userQo.setId(userVo.getId().trim());
//		}
//		if(userVo.getOffice()!=null && StringUtils.isNotBlank(userVo.getOffice().getId())){
//			userQo.setOfficeId(userVo.getOffice().getId().trim());
//		}
//		if(userVo.getCompany()!=null && StringUtils.isNotBlank(userVo.getCompany().getId())){
//			userQo.setCompanyId(userVo.getCompany().getId().trim());
//		}
//		if(StringUtils.isNotBlankStr(userVo.getLoginName())){
//			userQo.setLoginNameLike(userVo.getLoginName().trim());
//		}
//		
//		if(StringUtils.isNotBlankStr(userVo.getName())){
//			userQo.setNameLike(userVo.getName());
//		}
//		
//		
//		return userQo;
//	}
	
	@ResponseBody
	@RequiresPermissions("sys:user:view")
	@RequestMapping(value = {"listData"})
	public Page<UserVo> listData(UserQo userQo, HttpServletRequest request, HttpServletResponse response, Model model) {
//		UserQo userQo = getQo(userVo);
		Page<UserVo> page = userService.findPage(getPage(request, response), userQo);
		return page;
	}

	@RequiresPermissions("sys:user:view")
	@RequestMapping(value = "form")
	public String form(UserQo userQo, Model model) {
		
		UserVo userVo = new UserVo();
		if(StringUtils.isNotBlank(userQo.getId())){
			userVo = userService.findById(userQo.getId());
			userVo.setRoleList(roleService.findByUserId(userQo.getId()));
		}else{
			UserVo currentUserVo = null;
			if (StringUtils.isNotBlank(userQo.getCompanyId())){
				if(currentUserVo==null){
					currentUserVo=SecurityHelper.getCurrentUser();
				}
				userVo.setCompany(currentUserVo.getCompany());
			}else{
				OfficeVo company = new OfficeVo();
				company.setId(userQo.getCompanyId());
				userVo.setCompany(company);
			}
			if (StringUtils.isNotBlank(userQo.getOfficeId())){
				if(currentUserVo==null){
					currentUserVo=SecurityHelper.getCurrentUser();
				}
				userVo.setOffice(currentUserVo.getOffice());
			}else{
				OfficeVo office = new OfficeVo();
				office.setId(userQo.getOfficeId());
				userVo.setOffice(office);
			}
		}
		
		
		model.addAttribute("userVo", userVo);
		model.addAttribute("allRoles", roleService.findList(new RoleQo()) );
		return "modules/sys/userForm";
	}
	
	/**
	 * 修改
	 * @param userVo
	 * @return
	 */
	@RequiresPermissions("sys:user:edit")
	@RequestMapping(value = "update")
	@ResponseBody
	@ApiOperation(value = "修改会员信息",httpMethod = "POST")
	@ApiCurdType(CurdType.UPDATE)
	public Result<?> update(UserVo userVo){
		// 如果新密码为空，则不更换密码
		if (StringUtils.isNotBlank(userVo.getNewPassword())) {
			userVo.setPassword(userService.entryptPassword(userVo.getNewPassword()));
		}
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, userVo)){
			return result;
		}
		if(StringUtils.isBlank(userVo.getId())){
			result.setParamFail("修改失败,末知的id");
			return result;
		}
		if (!"true".equals(checkLoginName(userVo.getOldLoginName(), userVo.getLoginName()))){
			result.setCode(RespCode.BUSI_ERROR.getCode());
			result.setMsg("保存用户'" + userVo.getLoginName() + "'失败，登录名已存在");
			return result;
		}
		
		// 角色数据有效性验证，过滤不在授权内的角色
		List<RoleVo> roleList =  new ArrayList<RoleVo>();
		List<String> roleIdList = userVo.getRoleIdList();
		for (RoleVo r : roleService.findList(new RoleQo())){
			if (roleIdList.contains(r.getId())){
				roleList.add(r);
			}
		}
		userVo.setRoleList(roleList);
		
		preUpdate(userVo,SecurityHelper.getCurrentUserId());
		userService.update(userVo);
		
		return result;
		
	}
	
	/**
	 * 修改个人用户密码
	 * @param oldPassword
	 * @param newPassword
	 * @param model
	 * @return
	 */
	@RequiresPermissions("user")
	@RequestMapping(value = "updatePwd")
	@ApiOperation(value = "修改个人用户密码",httpMethod = "POST")
	public Result<UserVo> updatePwd(@RequestParam(required=true)String oldPassword, @RequestParam(required=true)String newPassword) {
		Result<UserVo> result = Result.newSuccResult();
		UserVo user = SecurityHelper.getCurrentUser();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (userService.validatePassword(oldPassword, user.getPassword())){
			userService.updatePasswordById(user.getId(), newPassword);
			result.setSuccess("密码修改成功");
			
		}else{
			result.setBusiFail("修改密码失败，旧密码错误");
		}
		return result;
	}
	
	
	
	/**
	 * 新增
	 * @param userVo
	 * @return
	 */
	@RequiresPermissions("sys:user:edit")
	@RequestMapping(value = "add")
	@ResponseBody
	@ApiOperation(value = "新增会员信息",httpMethod = "POST")
	@ApiCurdType(CurdType.ADD)
	public Result<?> add(UserVo userVo){
		
		Result<?> result = Result.newSuccResult();
		
		if (StringUtils.isBlank(userVo.getNewPassword())) {
			userVo.setPassword(userService.entryptPassword(userVo.getNewPassword()));
			result.setParamFail("密码不能为空");
			return result;
		}
		
		if (!beanValidator(result, userVo)){
			return result;
		}
		if (!"true".equals(checkLoginName(userVo.getOldLoginName(), userVo.getLoginName()))){
			result.setCode(RespCode.BUSI_ERROR.getCode());
			result.setMsg("保存用户'" + userVo.getLoginName() + "'失败，登录名已存在");
			return result;
		}
		
		// 角色数据有效性验证，过滤不在授权内的角色
		List<RoleVo> roleList =  new ArrayList();
		List<String> roleIdList = userVo.getRoleIdList();
		for (RoleVo r : roleService.findList(new RoleQo())){
			if (roleIdList.contains(r.getId())){
				roleList.add(r);
			}
		}
		userVo.setRoleList(roleList);
		preInsert(userVo,SecurityHelper.getCurrentUserId());
		userService.add(userVo);
		return result;
	}

	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:user:edit")
	@RequestMapping(value = "del")
	@ResponseBody
	@ApiOperation(value = "删除会员信息",httpMethod = "POST")
	@ApiCurdType(CurdType.DELETE)
	public Result<UserVo> delete(@RequestParam(required=true)String id){
		Result<UserVo> result = Result.newSuccResult();
		
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		
		if (id.equals(SecurityHelper.getCurrentUserId())){
			result.setBusiFail("删除用户失败, 不允许删除当前用户");
			return result;
			
		}else if (UserUtil.isAdmin(id)){
			result.setBusiFail("删除用户失败, 不允许删除超级管理员用户");
			return result;
		}else{
			
			UserVo delVo = new UserVo();
			delVo.setId(id);
			
			preUpdate(delVo,SecurityHelper.getCurrentUserId());
			userService.delete(delVo);
			return result;
		}
		
	}

	@RequiresPermissions("sys:user:edit")
	@RequestMapping(value = "save")
	public String save(UserVo userVo, HttpServletRequest request, Model model, RedirectAttributes redirectAttributes) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/sys/user/list?repage";
		}
		// 如果新密码为空，则不更换密码
		if (StringUtils.isNotBlank(userVo.getNewPassword())) {
			userVo.setPassword(userService.entryptPassword(userVo.getNewPassword()));
		}
		if (!beanValidator(model, userVo)){
//			return form(userVo, model);
			model.addAttribute("allRoles", roleService.findList(new RoleQo()) );
			return "modules/sys/userForm";
		}
		if (!"true".equals(checkLoginName(userVo.getOldLoginName(), userVo.getLoginName()))){
			addMessage(model, "保存用户'" + userVo.getLoginName() + "'失败，登录名已存在");
//			return form(userVo, model);
			model.addAttribute("allRoles", roleService.findList(new RoleQo()) );
			return "modules/sys/userForm";
		}
		// 角色数据有效性验证，过滤不在授权内的角色
		List<RoleVo> roleList =  new ArrayList();
		List<String> roleIdList = userVo.getRoleIdList();
		for (RoleVo r : roleService.findList(new RoleQo())){
			if (roleIdList.contains(r.getId())){
				roleList.add(r);
			}
		}
		userVo.setRoleList(roleList);
		
		// 保存用户信息
		if(StringUtils.isNotBlank(userVo.getId())){
			preUpdate(userVo,SecurityHelper.getCurrentUserId());
			userService.update(userVo);
		}else{
			preInsert(userVo,SecurityHelper.getCurrentUserId());
			userService.add(userVo);
		}
		// 清除当前用户缓存
//		if (user.getLoginName().equals(systemService.getUser().getLoginName())){
//			UserUtils.clearCache();
//			//UserUtils.getCacheMap().clear();
//		}
		addMessage(redirectAttributes, "保存用户'" + userVo.getLoginName() + "'成功");
		return "redirect:" + adminPath + "/sys/user/list?repage";
	}
	
	@RequiresPermissions("sys:user:edit")
	@RequestMapping(value = "delete")
	public String delete(@RequestParam(required=true)String id, RedirectAttributes redirectAttributes) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/sys/user/list?repage";
		}
		if (id.equals(SecurityHelper.getCurrentUserId())){
			addMessage(redirectAttributes, "删除用户失败, 不允许删除当前用户");
		}else 
		if (UserUtil.isAdmin(id)){
			addMessage(redirectAttributes, "删除用户失败, 不允许删除超级管理员用户");
		}else{
			
			UserVo delVo = new UserVo();
			delVo.setId(id);
			delVo.setDelFlag(Constants.DEL_FLAG_DELETE);//标记删除
			
			preUpdate(delVo,SecurityHelper.getCurrentUserId());
			userService.delete(delVo);
			addMessage(redirectAttributes, "删除用户成功");
		}
		return "redirect:" + adminPath + "/sys/user/list?repage";
	}
	
	/**
	 * 导出用户数据
	 * @param user
	 * @param request
	 * @param response
	 * @param redirectAttributes
	 * @return
	 */
//	@RequiresPermissions("sys:user:view")
//    @RequestMapping(value = "export", method=RequestMethod.POST)
//    public String exportFile(User user, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
//		try {
//            String fileName = "用户数据"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
//            JadPage<User> page = systemService.findUser(new Page<User>(request, response, -1), user);
//    		new ExportExcel("用户数据", User.class).setDataList(page.getList()).write(response, fileName).dispose();
//    		return null;
//		} catch (Exception e) {
//			addMessage(redirectAttributes, "导出用户失败！失败信息："+e.getMessage());
//		}
//		return "redirect:" + adminPath + "/sys/user/list?repage";
//    }

	/**
	 * 导入用户数据
	 * @param file
	 * @param redirectAttributes
	 * @return
	 */
//	@RequiresPermissions("sys:user:edit")
//    @RequestMapping(value = "import", method=RequestMethod.POST)
//    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
//		if(Global.isDemoMode()){
//			addMessage(redirectAttributes, "演示模式，不允许操作！");
//			return "redirect:" + adminPath + "/sys/user/list?repage";
//		}
//		try {
//			int successNum = 0;
//			int failureNum = 0;
//			StringBuilder failureMsg = new StringBuilder();
//			ImportExcel ei = new ImportExcel(file, 1, 0);
//			List<User> list = ei.getDataList(User.class);
//			for (User user : list){
//				try{
//					if ("true".equals(checkLoginName("", user.getLoginName()))){
//						user.setPassword(SystemService.entryptPassword("123456"));
//						BeanValidators.validateWithException(validator, user);
//						systemService.saveUser(user);
//						successNum++;
//					}else{
//						failureMsg.append("<br/>登录名 "+user.getLoginName()+" 已存在; ");
//						failureNum++;
//					}
//				}catch(ConstraintViolationException ex){
//					failureMsg.append("<br/>登录名 "+user.getLoginName()+" 导入失败：");
//					List<String> messageList = BeanValidators.extractPropertyAndMessageAsList(ex, ": ");
//					for (String message : messageList){
//						failureMsg.append(message+"; ");
//						failureNum++;
//					}
//				}catch (Exception ex) {
//					failureMsg.append("<br/>登录名 "+user.getLoginName()+" 导入失败："+ex.getMessage());
//				}
//			}
//			if (failureNum>0){
//				failureMsg.insert(0, "，失败 "+failureNum+" 条用户，导入信息如下：");
//			}
//			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条用户"+failureMsg);
//		} catch (Exception e) {
//			addMessage(redirectAttributes, "导入用户失败！失败信息："+e.getMessage());
//		}
//		return "redirect:" + adminPath + "/sys/user/list?repage";
//    }
	
	/**
	 * 下载导入用户数据模板
	 * @param response
	 * @param redirectAttributes
	 * @return
	 */
//	@RequiresPermissions("sys:user:view")
//    @RequestMapping(value = "import/template")
//    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
//		try {
//            String fileName = "用户数据导入模板.xlsx";
//    		List<User> list = Lists.newArrayList(); list.add(UserUtils.getUser());
//    		new ExportExcel("用户数据", User.class, 2).setDataList(list).write(response, fileName).dispose();
//    		return null;
//		} catch (Exception e) {
//			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
//		}
//		return "redirect:" + adminPath + "/sys/user/list?repage";
//    }

	/**
	 * 验证登录名是否有效
	 * @param oldLoginName
	 * @param loginName
	 * @return
	 */
	@ResponseBody
	@RequiresPermissions("sys:user:edit")
	@RequestMapping(value = "checkLoginName")
	public String checkLoginName(String oldLoginName, String loginName) {
		if (loginName !=null && loginName.equals(oldLoginName)) {
			return "true";
		} else if (loginName !=null && userService.findByLoginName(loginName) == null) {
			return "true";
		}
		return "false";
	}

	/**
	 * 用户信息显示
	 * @param userVo
	 * @param model
	 * @return
	 */
	@RequiresPermissions("user")
	@RequestMapping(value = "info")
	public String info(HttpServletResponse response, Model model) {
		UserVo userVo = SecurityHelper.getCurrentUser();
		userVo.setRoleList(roleService.findByUserId(userVo.getId()));
		model.addAttribute("userVo", userVo);
		model.addAttribute("Global", new Global());
		return "modules/sys/userInfo";
	}
	
	/**
	 * 用户信息保存
	 * @param userVo
	 * @param response
	 * @param model
	 * @return
	 */
	@RequiresPermissions("user")
	@RequestMapping(value = "modifyInfo")
	public String modifyInfo(UserVo userVo, HttpServletResponse response, Model model){
		UserVo currentUser = SecurityHelper.getCurrentUser();
		if (StringUtils.isNotBlank(userVo.getName())){
			if(Global.isDemoMode()){
				model.addAttribute("message", "演示模式，不允许操作！");
				return "modules/sys/userInfo";
			}
			currentUser.setRoleList(roleService.findByUserId(currentUser.getId()));
			currentUser.setEmail(userVo.getEmail());
			currentUser.setPhone(userVo.getPhone());
			currentUser.setMobile(userVo.getMobile());
			currentUser.setRemarks(userVo.getRemarks());
			currentUser.setPhoto(userVo.getPhoto());
			userService.update(currentUser);
			model.addAttribute("message", "保存用户信息成功");
		}
		model.addAttribute("userVo", currentUser);
		model.addAttribute("Global", new Global());
		return "modules/sys/userInfo";
	}

	/**
	 * 返回用户信息
	 * @return
	 */
	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "infoData")
	public UserVo infoData() {
		return SecurityHelper.getCurrentUser();
	}
	
	/**
	 * 修改个人用户密码
	 * @param oldPassword
	 * @param newPassword
	 * @param model
	 * @return
	 */
	@RequiresPermissions("user")
	@RequestMapping(value = "modifyPwd")
	public String modifyPwd(String oldPassword, String newPassword, Model model) {
		UserVo user = SecurityHelper.getCurrentUser();
		if (StringUtils.isNotBlank(oldPassword) && StringUtils.isNotBlank(newPassword)){
			if(Global.isDemoMode()){
				model.addAttribute("message", "演示模式，不允许操作！");
				return "modules/sys/userModifyPwd";
			}
			if (userService.validatePassword(oldPassword, user.getPassword())){
				userService.updatePasswordById(user.getId(), newPassword);
				model.addAttribute("message", "修改密码成功");
			}else{
				model.addAttribute("message", "修改密码失败，旧密码错误");
			}
		}
		model.addAttribute("userVo", user);
		return "modules/sys/userModifyPwd";
	}
	
	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "treeData")
	public List<Map<String, Object>> treeData(@RequestParam(required=false) String officeId, HttpServletResponse response) {
		List<Map<String, Object>> mapList =  new ArrayList();
		UserQo qo=new UserQo();
		qo.setOfficeId(officeId);
		List<UserVo> list = userService.findList(qo);
		for (int i=0; i<list.size(); i++){
			UserVo e = list.get(i);
			Map<String, Object> map =  new HashMap();
			map.put("id", "u_"+e.getId());
			map.put("pId", officeId);
			map.put("name", StringUtils.replace(e.getName(), " ", ""));
			mapList.add(map);
		}
		return mapList;
	}
    
//	@InitBinder
//	public void initBinder(WebDataBinder b) {
//		b.registerCustomEditor(List.class, "roleList", new PropertyEditorSupport(){
//			@Autowired
//			private SystemService systemService;
//			@Override
//			public void setAsText(String text) throws IllegalArgumentException {
//				String[] ids = StringUtils.split(text, ",");
//				List<Role> roles = new ArrayList<Role>();
//				for (String id : ids) {
//					Role role = systemService.getRole(Long.valueOf(id));
//					roles.add(role);
//				}
//				setValue(roles);
//			}
//			@Override
//			public String getAsText() {
//				return Collections3.extractToString((List) getValue(), "id", ",");
//			}
//		});
//	}
}
