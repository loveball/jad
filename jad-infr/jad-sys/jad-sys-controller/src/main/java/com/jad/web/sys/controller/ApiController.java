package com.jad.web.sys.controller;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jad.commons.security.shiro.SecurityHelper;
import com.jad.commons.security.shiro.SessionManager;
import com.jad.web.mvc.BaseController;
import com.jad.web.mvc.swagger.conf.ApiSwaggerConfig;


/**
 * api测试
 */
@Controller
@RequestMapping(value = "${adminPath}/api")
public class ApiController extends BaseController{
	
	
	@RequestMapping(value = {"index",""})
	public String index(Model model,HttpServletRequest request) {
		System.out.println("进入api列表");
//		swaggerApiSessionId
//		request.getSession().setAttribute(ApiSwaggerConfig.API_SESSION_ID, SecurityHelper.getSessionId());
//		System.out.println("进去的id:"+request.getSession().getAttribute(ApiSwaggerConfig.API_SESSION_ID)+",原id:"+request.getSession().getId());
//		return "redirect:../../swagger-ui.jsp?__apisid="+SecurityHelper.getSessionId();
		return "redirect:../../swagger-ui.html";
	}
	
}




