package com.jad.rpc.provider.cms;

import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.jad.rpc.service.RpcMain;



/**
 * cms模块启动入口
 * @author Administrator
 *
 */
public class RpcMainCms extends RpcMain{
	
	public static void main(String[] args) {
		PathMatchingResourcePatternResolver d=new PathMatchingResourcePatternResolver();
		start(args);

	}

}
