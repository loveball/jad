package com.jad.commons.enums;

/**
 * @author Administrator
 *
 */
public enum CurdType {
	
	ADD("add", "增加"), 
	UPDATE("update", "修改"),
	DELETE("delete", "删除"),
	FIND_ONE("findOne", "查看单条记录"),
	FIND_LIST("findList", "查看列表");
	
	private final String type;

	private final String desc;

	CurdType(final String type, final String desc) {
		this.type = type;
		this.desc = desc;
	}

	public String getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}
	
}



