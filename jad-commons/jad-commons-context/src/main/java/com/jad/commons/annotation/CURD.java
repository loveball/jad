package com.jad.commons.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jad.commons.enums.DataType;
import com.jad.commons.enums.WidgetType;

/**
 * 
 * 主要用于标识VO或QO中的属性
 * 以便在数据进行CURD操作时，给出一些默认的操作建议
 * 
 * 主要有以下几个用途
 * 1、为CURD页面提供建议性属性，比如建议某个字段能否在列表页、新增页面或修改页面上显示，能否为空，最大长度限制等等
 * 2、可作为后台数据校验的依据
 * 3、为Swagger自产api组件提拱依据
 * 4、为自动生成的页面提供ui依据
 * 
 * 注意：此注解不是必须的，仅仅为一些默认的建议性操作。
 * 除了服务器数据验证逻辑之外，开发人员不应该将过多的业务逻辑依懒于此注解
 * 而且这里的属性仅仅为建议性的或默认操作，具体以业务代码为准。比如通过此注解配置了某个字段在修改时不能为null值，
 * 但实体上后台逻辑可以把它的值设为null值
 * 
 * @author Administrator
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface CURD {
	
	String label() default "";
	
	boolean showInList() default true;
	
	boolean showInView() default true;
	
	InsertConf insertConf() default @InsertConf(true);
	
	UpdateConf updateConf() default @UpdateConf(true);
	
	QueryConf queryConf() default @QueryConf(true);
	
	DataType dataType() default DataType.STR;
	
	WidgetType widgetType() default WidgetType.STR_INPUT;
	
	boolean required() default false;
	
}








