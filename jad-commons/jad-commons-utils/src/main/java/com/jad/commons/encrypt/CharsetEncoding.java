package com.jad.commons.encrypt;

import java.nio.charset.Charset;

public class CharsetEncoding {
	
	public static final String DEFAULT_ENCODING="UTF-8";
	
	public static final Charset DEFAULT_CHARSET_ENCODING = Charset.forName(DEFAULT_ENCODING);
	

}
