package com.jad.commons.http;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.encrypt.Encodes;


public class HttpUtil {


	
	private static Logger logger = LoggerFactory.getLogger(HttpUtil.class);
 
	
	public static final String DEFAULT_CHARCHTER = "UTF-8";
	
	public static final int timeout = 20;//连接超时(秒)
	
	
	public static void main(String[] args)throws Exception {
//		String url="https://api.weixin.qq.com/customservice/msgrecord/getmsglist?access_token=ACCESS_TOKEN";
		
		String url="gid=3 or 1=1 &gname=【新疆】时时彩";
//		String url="http://555.tp008.com/index/user/betrecord?gname=【新疆】时时彩";
		
		url=Encodes.urlEncode(url);
		
		url="http://555.tp008.com/index/user/betrecord?"+url;
		
		System.out.println(url.contains("?"));
		
//		System.out.println("url:"+url);
//		
//		String cookie="PHPSESSID=606ff85fbdbcd92d7ef62786185023af; EZApi=B30CIFI4WzFWcFJsBCcENg16VSxUKglnADADalU9VyJTaFV3VBZbFlYOVSRUYAAwBjBVZAICAHUCZlAxAzdRMgcrAjJSYFt3Vn5SdARsBHANelU6VCoJOABjAzdVdlcyU2dVe1RlW2BWZlV%2FVGQAMQZgVSICcgAiAiNQOwN8UXIHJAI4UnNbbVZiUmYENgQiDSU%3D; EZCookie=AXsAIlI4CmBTdQM9BiUBMVExUDdQPg0vAH0EIlAoBCBSNlUnVzpaNVA5UDFVdgE%2FVSIHM1JsCTwAZwo3DTlUIQEsACJSNAppUzYDbgZrASdRPlAnUC4NIQBzBHFQLARxUmlVd1d2WnhQdlA2VT0Bd1V0BzpSagloACgKLA06VG0BdQBsUj0KKFN1A3EGbgF1USZQP1AuDTwAcwQsUH8EJVI6VSVXIFo9UCBQOFUxASdVOgdwUlIJfABnCjgNNlRmAVwAdVJoCjRTZgNmBlsBcFEwUGNQPQ1sAA0EdVBoBGdSZlVtV3ZaeFB2UDlVNQF9VXYHO1J%2BCSsAawosDTFUIQEsACJSPAplUy8DcQZuAXVRcFBsUHgNYQA0BCJQZwRxUg9VIFdiWmJQYlAxVQgBcFU5B2JSPwloAA0Kew00VGUBMQBhUg0KcVNiAzMGMgE9USZQKVAuDXkAKARwUDgEcVJpVXdXNVozUDFQOlUgASdVLAdwUnwJbAAzCm8NdFRmASIAOlJzCjVTZQMpBjQBJ1EoUCdQZQ1jACUEZVA6BCFSMlU5V3ZablB2UGRVdgEpVSIHMFJvCWUAMApgDWNUZgEiADpScwo2U24DNQYxATFRKlA1UDwNPQBhBCJQcQRxUjFVNFc4WjVQOlA3VTEBWlV1ByFSawkrAGsKLA0yVDoBMgA2UmUKKlNnAzcGNwE1USZQKVAuDW4AIwRlUDwEJ1I2VQpXIFo9UDlQMVV2AT9VIgdjUjoJPgBmCjkNNFQzATEAMlJlCiZTewMlBmMBZFFwUGBQeA1kADwEZVB%2FBGlSYlVhV2xaZ1BgUGBVYwE9VTcHY1IiCSsAPQphDWdUXAF0AGlSPAphU3UDPQYlATRRMFA9UD8NOQBlBDZQagRnUmdVd1d4WnZQO1A6VTgBbFVuBzdSLAkzAHMKPg0iVC8BIgBwUjAKdlMyA2kGcwFaUW1QYVAuDTcAcwQyUGQEYFJlVXdXeFp2UDhQO1UzAVpVeQc8UiwJMwBgCnM%3D";
//		List<String>cookeList=new ArrayList<String>();
//		cookeList.add(cookie);
//		
//		HttpReq req=new HttpReq();
//		req.setAddr(url);
//		req.setCookieList(cookeList);
////		req.setHeadMap(headMap);
//		
//		Map params=new HashMap();
//		params.put("gid", "3");
//		
//		req.setParams(params);
//		
//		HttpResp resp= HttpUtil.get(req);
//		System.out.println(resp.getRespMsg());
		
	}
	
	
	
	
//	public static final RequestConfig requestconfig = RequestConfig.custom()
//			.setSocketTimeout(timeout).setConnectTimeout(timeout)
//			.setConnectionRequestTimeout(timeout * 2).build();
	
	public static HttpResp get(String url) throws MyHttpException {
		return get(url,null,null,null,timeout,timeout*2);
	}
	public static HttpResp get(String url,Map<String,String>params ) throws MyHttpException {
		return get(url,params,null,null,timeout,timeout*2);
	}
	public static HttpResp get(String url,Map<String,String>params,Map<String,String>headMap) throws MyHttpException {
		return get(url,params,headMap,null,timeout,timeout*2);
	}
	
	public static HttpResp get(String url,int connectTimeout ,int readTimeout) throws MyHttpException {
		return get(url,null,null,null,connectTimeout,readTimeout);
	}
	public static HttpResp get(String url,Map<String,String>params,int connectTimeout ,int readTimeout) throws MyHttpException {
		return get(url,params,null,null,connectTimeout,readTimeout);
	}
	
	public static HttpResp get(String url,Map<String,String>params,Map<String,String>headMap,int connectTimeout ,int readTimeout) throws MyHttpException {
		return get(url,params,headMap,null,connectTimeout,readTimeout);
	}
	
	public static File getToFile(String addr,String absoluteFile)throws MyHttpException{
		return getToFile(addr,timeout,timeout*2,absoluteFile);
	}
	
	public static File getToFile(String addr, int connectTimeout,int readTimeout,String absoluteFile)throws MyHttpException{

		byte []data=getByte(addr,connectTimeout,readTimeout);
		
		File file=new File(absoluteFile);
		if(file.exists() && file.isDirectory()){
			throw new MyHttpException("写入文件异常，文件("+absoluteFile+")是一个目录");
		}
		
		FileOutputStream output=null;
		try {
			
			output=new FileOutputStream(file);
			
			output.write(data);
			
		} catch (Exception e) {
			throw new MyHttpException("写入文件异常,io错误,"+e.getMessage(),e);
			
		}finally {
			try {
				if(output!=null){
					output.close();
				}
			} catch (IOException e) {
				logger.error(e.getMessage(),e);
			}
		}
		
		return file;
		
			
	}
	/**
	 * 发送get请求 并获得二进制数据
	 * 
	 * @param addr
	 * @param msg
	 * @param timeout
	 *            超时时单,单位:秒
	 * @return
	 * @throws MyHttpException
	 */
	public static byte[] getByte(String addr) throws MyHttpException {
		return getByte(addr,timeout,timeout*2);
	}
	
	
	/**
	 * 发送get请求 
	 * 
	 * @param addr
	 * @param msg
	 * @param timeout
	 *            超时时单,单位:秒
	 * @return
	 * @throws MyHttpException
	 */
	public static byte[] getByte(String addr, int connectTimeout,int readTimeout) throws MyHttpException {


		CloseableHttpClient client = null;
		try {
			HttpGet get = new HttpGet(addr);
			if(connectTimeout<=0){
				connectTimeout=timeout;
			}
			if(readTimeout<=0){
				readTimeout=timeout*2;
			}
			
			RequestConfig requestconfig = RequestConfig.custom()
					.setSocketTimeout(connectTimeout*1000).setConnectTimeout(connectTimeout*1000)
					.setConnectionRequestTimeout(readTimeout*1000).build();
			
			
			get.setConfig(requestconfig);
			client = HttpClients.createDefault();
			HttpResponse response = client.execute(get);
			return getByteData(response);
			
		} catch (Exception e) {
			throw new MyHttpException("get异常,url:" + addr + "," + e.getMessage(),e);
		} finally {
			if (client != null) {
				try {
					client.close();
				} catch (IOException e) {
				}
			}
		}
	
	}
	
	
	/**
	 * 发送get请求 
	 * 
	 * @param addr
	 * @param msg
	 * @param timeout
	 *            超时时单,单位:秒
	 * @return
	 * @throws MyHttpException
	 */
	public static HttpResp get(String addr, 
			Map<String, String> params,
			Map<String,String>headMap,
			List<String>cookieList,int connectTimeout,int readTimeout) throws MyHttpException {

		
		
		CloseableHttpClient client = null;
		try {
			StringBuffer sb=new StringBuffer();
			sb.append(addr);
			
			if(params!=null && !params.isEmpty()){
				if(addr.contains("?")){
					sb.append("&");
				}else{
					sb.append("?");
				}
				
				for(Map.Entry<String, String>ent:params.entrySet()){
					sb.append(URLEncoder.encode(ent.getKey(),DEFAULT_CHARCHTER));
					sb.append("=");
					sb.append(URLEncoder.encode(ent.getValue(),DEFAULT_CHARCHTER));
				}
				
				addr=sb.toString();
			}
			
			
			HttpGet get = new HttpGet(addr);
			
			if(headMap!=null){
				for(Map.Entry<String, String>ent:headMap.entrySet()){
					get.addHeader(ent.getKey(), ent.getValue());
				}
			}
			if(cookieList!=null){
				for(String cookie:cookieList){
					if(StringUtils.isNotBlank(cookie)){
						get.addHeader("Cookie", cookie);
					}
				}
			}
			
			if(connectTimeout<=0){
				connectTimeout=timeout;
			}
			if(readTimeout<=0){
				readTimeout=timeout*2;
			}
			
			RequestConfig requestconfig = RequestConfig.custom()
					.setSocketTimeout(connectTimeout*1000).setConnectTimeout(connectTimeout*1000)
					.setConnectionRequestTimeout(readTimeout*1000).build();
			
			
			get.setConfig(requestconfig);
			client = HttpClients.createDefault();
			
			HttpResponse response = client.execute(get);
			
			return getHttpResp(response);
			
		} catch (Exception e) {
			throw new MyHttpException("get异常,url:" + addr + "," + e.getMessage(),e);
		} finally {
			if (client != null) {
				try {
					client.close();
				} catch (IOException e) {
				}
			}
		}
	
	}

	
	/**
	 * 发送post请求 
	 * 
	 * @param addr
	 * @param msg
	 * @param timeout
	 *            超时时单,单位:秒
	 * @return
	 * @throws MyHttpException
	 */
	public static HttpResp post(String addr, 
			Map<String, String> params) throws MyHttpException {
		return post(addr,params,null,null,timeout,timeout*2);
	}
	
	public static HttpResp postMsg(String addr,String msg)throws MyHttpException {
		return postMsg(addr,msg,null,null,timeout,timeout*2);
	}
	
	public static HttpResp post(String addr, 
			Map<String, String> params,int connectTimeout,int readTimeout) throws MyHttpException {
		return post(addr,params,null,null,connectTimeout,readTimeout);
	}
	
	public static HttpResp post(String addr, 
			Map<String, String> params,
			Map<String,String>headMap,
			List<String>cookieList) throws MyHttpException {
		return post(addr,params,headMap,cookieList,timeout,timeout*2);
		
		
	}
	
	
	public static HttpResp postMsg(String addr, 
			String msg,
			Map<String,String>headMap,
			List<String>cookieList,int connectTimeout,int readTimeout) throws MyHttpException {
		HttpPost httpost = new HttpPost(addr);
		if(headMap!=null){
			for(Map.Entry<String, String>ent:headMap.entrySet()){
				httpost.addHeader(ent.getKey(), ent.getValue());
			}
		}
		if(cookieList!=null){
			for(String cookie:cookieList){
				if(StringUtils.isNotBlank(cookie)){
					httpost.addHeader("Cookie", cookie);
				}
			}
		}
//		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
//		if (params != null && !params.isEmpty()) {
//			for (String key : params.keySet()) {
//				nvps.add(new BasicNameValuePair(key, params.get(key)));
//			}
//		}
		StringEntity reqEntity = new StringEntity(msg, HTTP.UTF_8);
		CloseableHttpClient client = null;
		try {
			client = HttpClients.createDefault();
//			httpost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
			httpost.setEntity(reqEntity);
			
			if(connectTimeout<=0){
				connectTimeout=timeout;
			}
			if(readTimeout<=0){
				readTimeout=timeout*2;
			}
			
			RequestConfig requestconfig = RequestConfig.custom()
					.setSocketTimeout(connectTimeout*1000).setConnectTimeout(connectTimeout*1000)
					.setConnectionRequestTimeout(readTimeout*1000).build();
			
			httpost.setConfig(requestconfig);

			HttpResponse response = client.execute(httpost);
			return getHttpResp(response);
			
		} catch (Exception e) {
			throw new MyHttpException("post异常,url:" + addr + "," + e.getMessage(),
					e);
		} finally {
			if (client != null) {
				try {
					client.close();
				} catch (IOException e) {
				}
			}
		}
		
	}
	
	public static HttpResp post(String addr, 
			Map<String, String> params,
			Map<String,String>headMap,
			List<String>cookieList,int connectTimeout,int readTimeout) throws MyHttpException {
		HttpPost httpost = new HttpPost(addr);
		if(headMap!=null){
			for(Map.Entry<String, String>ent:headMap.entrySet()){
				httpost.addHeader(ent.getKey(), ent.getValue());
			}
		}
		if(cookieList!=null){
			for(String cookie:cookieList){
				if(StringUtils.isNotBlank(cookie)){
					httpost.addHeader("Cookie", cookie);
				}
			}
		}
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		if (params != null && !params.isEmpty()) {
			for (String key : params.keySet()) {
				nvps.add(new BasicNameValuePair(key, params.get(key)));
			}
		}
		CloseableHttpClient client = null;
		try {
			client = HttpClients.createDefault();
			httpost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
			
			if(connectTimeout<=0){
				connectTimeout=timeout;
			}
			if(readTimeout<=0){
				readTimeout=timeout*2;
			}
			
			RequestConfig requestconfig = RequestConfig.custom()
					.setSocketTimeout(connectTimeout*1000).setConnectTimeout(connectTimeout*1000)
					.setConnectionRequestTimeout(readTimeout*1000).build();
			
			httpost.setConfig(requestconfig);

			HttpResponse response = client.execute(httpost);
			return getHttpResp(response);
			
		} catch (Exception e) {
			throw new MyHttpException("post异常,url:" + addr + "," + e.getMessage(),
					e);
		} finally {
			if (client != null) {
				try {
					client.close();
				} catch (IOException e) {
				}
			}
		}
		
	}
	
	private static byte[] getByteData(HttpResponse response)throws MyHttpException {
		
		String state=response.getStatusLine().getStatusCode()+"";
		if(!"200".equals(state)){
			throw new MyHttpException("通讯异常，响应状态码("+state+")不是200");
		}
		HttpEntity entity = response.getEntity();
		
		
		try {
			return EntityUtils.toByteArray(entity);
			
			
		} catch (IOException e) {
			throw new MyHttpException("通讯异常，"+e.getMessage(),e);
		}
		
		
		
		
	}
	private static HttpResp getHttpResp(HttpResponse response) throws ParseException, IOException{
		HttpResp resp=new HttpResp();
		Header[]heads=response.getAllHeaders();//响应头
		
		Map<String,String>respHeadMap=new HashMap();
		List<String>respCookieList=new ArrayList();
		
		resp.setHeadMap(respHeadMap);
		resp.setCookieList(respCookieList);
		
		if(heads!=null && heads.length>0){
			for(Header h:heads){
				respHeadMap.put(h.getName(), h.getValue());
				if("Set-Cookie".equals(h.getName())){
					String cookie=h.getValue();
					respCookieList.add(cookie);
				}
			}
		}
		
		HttpEntity entity = response.getEntity();
		String res = EntityUtils.toString(entity,DEFAULT_CHARCHTER);
		resp.setRespMsg(res);
		
		resp.setRespStatusCode(response.getStatusLine().getStatusCode()+"");//响应状态码
		
//		System.out.println("结果:"+JsonMapper.toJsonString(resp));
		
		return resp;
	}
	
	public static HttpResp post(HttpReq vo) throws MyHttpException {
		return post(vo.getAddr(),vo.getParams(),vo.getHeadMap(),vo.getCookieList(),vo.getConnectTimeout(),vo.getReadTimeout());
	}
	
	public static HttpResp get(HttpReq vo) throws MyHttpException {
		int conntectTimeout=vo.getConnectTimeout();
		if(conntectTimeout<=0)conntectTimeout=timeout;
		
		int readTimeout=vo.getReadTimeout();
		if(readTimeout<=0)readTimeout=timeout*2;
		
		return get(vo.getAddr(),vo.getParams(),vo.getHeadMap(),vo.getCookieList(),conntectTimeout,readTimeout);
	}
	
	
	


	
	
}
