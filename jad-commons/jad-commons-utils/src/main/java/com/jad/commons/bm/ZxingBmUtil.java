package com.jad.commons.bm;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Binarizer;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Result;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class ZxingBmUtil {

	public static int DEF_SIZE=500;
	public static int DEF_MARGIN=10;
	
	public static void main(String[] args) throws Exception {
		
		String filePath = "D:/temp/qr/qr2.jpg";
//		File file = new File(filePath);
//		
		OutputStream img=new FileOutputStream(filePath);
////		
//////		genBm("测试",200,10,img,"G://testImage//bole.png");
//////		String str="http://www.boole28.com/m/mobile.jsp";
//		String str="http://weixin.edubaba.cn/";
		String str="http://98.126.200.138/qrj/";
//		String str="http://www.boole28.com/AnonymousServlet?method=toTelBm";
//		String logo="G://testImage//and.jpg";
		String logo=null;
		genBm(str, 400, 10, img,logo);
//		genBm(str, 82, 2, img,logo);
		
//		String filePath = "E://tempfile//t5.jpg";
		 parseBm(filePath);
	}

	/**
	 * 生成二维码
	 * @param content
	 * @param output
	 * @throws Exception
	 */
	public static void genBm(String content ,OutputStream output)throws Exception {
		genBm(content,DEF_SIZE,DEF_MARGIN,output,null);
	}
	/**
	 * 生成二维码
	 * @param content
	 * @param output
	 * @throws Exception
	 */
	public static void genBm(String content ,OutputStream output,String logoPath)throws Exception {
		genBm(content,DEF_SIZE,DEF_MARGIN,output,logoPath);
	}
	
	/**
	 * 生成二维码
	 * @param content
	 * @param size
	 * @param margin
	 * @param output
	 * 
	 * 1、如果要采用在二维码中添加头像，那么生成的二维码最好采用最高等级H级别的纠错能力，目的有两个：一是增加二维码的正确识别能力；二是扩大二维码数据内容的大小。
	 * 
	 * @throws Exception
	 */
	public static void genBm(String content,int size,int margin ,OutputStream output,String logoPath)throws Exception {
		
		System.out.println("开始生成二维码,content:"+content);
		
		MultiFormatWriter multiFormatWriter = new MultiFormatWriter();

		Map hints = new HashMap();
		hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		
		ErrorCorrectionLevel level = ErrorCorrectionLevel.H;
		hints.put(EncodeHintType.ERROR_CORRECTION, level); //容错率
		
		BitMatrix bitMatrix = multiFormatWriter.encode(content,
				BarcodeFormat.QR_CODE, size, size, hints);
		bitMatrix = updateBit(bitMatrix, margin);  //生成新的bitMatrix
		BufferedImage bi =  MatrixToImageWriter.toBufferedImage(bitMatrix);
		
		bi = zoomInImage(bi,size,size);//根据size放大、缩小生成的二维码
		
		String format="jpg";
        
		if(StringUtils.isNotBlank(logoPath)){//载入logo
			Graphics2D gs = bi.createGraphics();  
			
			int wLogo =size*20/100;
			int hLogo =size*20/100;
			
			int x = (size - wLogo) / 2; 
	        int y = (size - hLogo) / 2;
			
	        Image img = ImageIO.read(new File(logoPath));  
	        gs.drawImage(img, x, y,wLogo,hLogo, null);  
	        gs.dispose();  
	        img.flush();  
		}
		
        
        if(!ImageIO.write(bi, format, output)){  
            throw new IOException("Could not write an image of format: " + format);    
        }  
		
	}

	
	/** 
     *  
     * @param matrix 二维码矩阵相关 
     * @param format 二维码图片格式 
     * @param file 二维码图片文件 
     * @param logoPath logo路径 
     * @throws IOException 
     */  
//    public static void writeToFile(BitMatrix matrix,String format,File file,String logoPath) throws IOException {  
//        BufferedImage image = MatrixToImageWriter.toBufferedImage(matrix);  
//        Graphics2D gs = image.createGraphics();  
//          
//        //载入logo  
//        Image img = ImageIO.read(new File(logoPath));  
//        gs.drawImage(img, 125, 125, null);  
//        gs.dispose();  
//        img.flush();  
//        if(!ImageIO.write(image, format, file)){  
//            throw new IOException("Could not write an image of format " + format + " to " + file);    
//        }  
//    }  
    
	public static void parseBm(String filePath) {
		try {
			MultiFormatReader formatReader = new MultiFormatReader();
			
			File file = new File(filePath);
			BufferedImage image = ImageIO.read(file);
			LuminanceSource source = new BufferedImageLuminanceSource(image);
			Binarizer binarizer = new HybridBinarizer(source);
			BinaryBitmap binaryBitmap = new BinaryBitmap(binarizer);
			Map hints = new HashMap();
			hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
			Result result = formatReader.decode(binaryBitmap, hints);

			System.out.println("result = " + result.toString());
			System.out.println("resultFormat = " + result.getBarcodeFormat());
			System.out.println("resultText = " + result.getText());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
    

	private static BitMatrix updateBit(BitMatrix matrix, int margin) {

		int tempM = margin * 2;

		int[] rec = matrix.getEnclosingRectangle(); // 获取二维码图案的属性

		int resWidth = rec[2] + tempM;

		int resHeight = rec[3] + tempM;

		BitMatrix resMatrix = new BitMatrix(resWidth, resHeight); // 按照自定义边框生成新的BitMatrix

		resMatrix.clear();

		for (int i = margin; i < resWidth - margin; i++) { // 循环，将二维码图案绘制到新的bitMatrix中

			for (int j = margin; j < resHeight - margin; j++) {

				if (matrix.get(i - margin + rec[0], j - margin + rec[1])) {

					resMatrix.set(i, j);

				}

			}

		}

		return resMatrix;

	}

	/**
	 * 
	 * 图片放大缩小
	 */

	public static BufferedImage zoomInImage(BufferedImage originalImage,
			int width, int height) {

		BufferedImage newImage = new BufferedImage(width, height,
				originalImage.getType());

		Graphics g = newImage.getGraphics();

		g.drawImage(originalImage, 0, 0, width, height, null);

		g.dispose();

		return newImage;

	}

}
