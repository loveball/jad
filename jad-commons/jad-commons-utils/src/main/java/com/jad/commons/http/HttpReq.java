package com.jad.commons.http;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

public class HttpReq  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 目标地址
	 */
	private String addr;
	
	/**
	 * 请求方法 
	 */
	private String method;
	
	/**
	 * 请求参数
	 */
	private Map<String, String> params;
	
	/**
	 * 请求字符串
	 */
	private String reqMsg;
	
	/**
	 * 请求头
	 */
	private Map<String,String>headMap;
	
	/**
	 * cookie
	 */
	private List<String>cookieList;
	
	
	/**
	 * 连接超时(秒)
	 */
	private int connectTimeout;
	
	/**
	 * 读取超时(秒)
	 */
	private int readTimeout;
	
	
	public static void main(String[] args)   {

	}


	public int getConnectTimeout() {
		return connectTimeout;
	}



	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}


	public int getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(int readTimeout) {
		this.readTimeout = readTimeout;
	}



	@Override
	public String toString() {
		return new Gson().toJson(this);
	}


	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public Map<String, String> getParams() {
		return params;
	}
	public void setParams(Map<String, String> params) {
		this.params = params;
	}
	public Map<String, String> getHeadMap() {
		return headMap;
	}
	public void setHeadMap(Map<String, String> headMap) {
		this.headMap = headMap;
	}
	public List<String> getCookieList() {
		return cookieList;
	}
	public void setCookieList(List<String> cookieList) {
		this.cookieList = cookieList;
	}


	public String getReqMsg() {
		return reqMsg;
	}


	public void setReqMsg(String reqMsg) {
		this.reqMsg = reqMsg;
	}
	
	
	
}
