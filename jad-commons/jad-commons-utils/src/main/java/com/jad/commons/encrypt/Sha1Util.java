/**
 * Copyright (c) 2011-2014, hubin (jobob@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jad.commons.encrypt;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.Arrays;

import org.apache.commons.codec.binary.Hex;

import com.jad.commons.utils.Exceptions;

/**
 * <p>
 * SHA1 签名算法
 * </p>
 * 
 * @author hubin
 * @Date 2015-01-09
 */
public class Sha1Util {
	
	public static String getSha1(String msg) {
		
		return getSha1(msg,CharsetEncoding.DEFAULT_ENCODING);
		
	}
	
	public static String getSha1(String msg,String charsetEncoding) {
		try {
			
			byte[] digest = Digests.sha1(msg.getBytes(charsetEncoding));
			
			return Hex.encodeHexString(digest);
			
		} catch (UnsupportedEncodingException e) {
			throw Exceptions.unchecked(e);
		}
		
	}

	
	
}