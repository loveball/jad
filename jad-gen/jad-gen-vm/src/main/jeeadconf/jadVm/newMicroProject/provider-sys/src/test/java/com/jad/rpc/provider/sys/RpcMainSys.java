package com.jad.rpc.provider.sys;

import com.jad.rpc.service.RpcMain;



/**
 * sys模块启动入口
 * @author Administrator
 *
 */
public class RpcMainSys extends RpcMain{
	
	public static void main(String[] args) {
		
		start(args);

	}

}
