package com.jad.test.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.BeforeTest;

public class BaseTestCase {

	private static Logger logger=LoggerFactory.getLogger(BaseTestCase.class); 
	
	protected ApplicationContext context ;
	
	private String contextConfigFile="spring-context.xml";
	
	private boolean needInitContext=true;
	
	
	@SuppressWarnings("unchecked")
	protected<T>T getBean(String beanName){
		if(!isNeedInitContext()){
			throw new TestException("没有初始化context");
		}
		try {
			return (T)context.getBean(beanName);
		} catch (Exception e) {
			throw new TestException("初始化context失败,"+e.getMessage(),e);
		}
	}
	
	
	public boolean isNeedInitContext() {
		return needInitContext;
	}


	public void setNeedInitContext(boolean needInitContext) {
		this.needInitContext = needInitContext;
	}


	public String getContextConfigFile() {
		return contextConfigFile;
	}
	

	public void setContextConfigFile(String contextConfigFile) {
		this.contextConfigFile = contextConfigFile;
	}


	@BeforeTest
    public void baseBeforeTest() throws Exception {
		logger.debug("context 正在初始化...");
		context = new ClassPathXmlApplicationContext(getContextConfigFile());
		logger.debug("context 初始化完成...");
	}
}
