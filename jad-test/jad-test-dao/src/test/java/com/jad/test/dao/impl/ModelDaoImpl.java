package com.jad.test.dao.impl;

import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;
import com.jad.test.dao.ModelDao;
import com.jad.test.entity.ModelEo;


@JadDao("modelDao")
public class ModelDaoImpl extends AbstractJadEntityDao<ModelEo,String>implements ModelDao {


}
